//
//  YesOrNoAlert.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 19/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class YesOrNoAlert: UIView {

    static let sharedInstance = Bundle.main.loadNibNamed("YesOrNoAlert", owner: nil, options: nil)!.first as! YesOrNoAlert
    
    class func alertView() -> YesOrNoAlert {
        return Bundle.main.loadNibNamed("YesOrNoAlert", owner: nil, options: nil)!.first as! YesOrNoAlert
    }
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var button1: PDButton!
    @IBOutlet weak var button2: PDButton!
    
    var completion:((Int)->Void)?
    var textFormat : TextFormat!
    var count : Int!
    
    func show(_ completion :  @escaping (_ buttonIndex : Int) -> Void) {
        self.showWithTitle(labelTitle.text, button1Title : "YES", button2Title: "NO", completion: completion)
    }
    
    func showWithTitle(_ title: String?, button1Title : String, button2Title : String, completion : @escaping (_ buttonIndex : Int) -> Void) {
        labelTitle.text = title
        button1.setTitle(button1Title, for: UIControlState())
        button2.setTitle(button2Title, for: UIControlState())
        self.completion = completion
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func button1Action(_ sender: AnyObject) {
        self.removeFromSuperview()
        completion?(1)
    }
    @IBAction func button2Action(_ sender: AnyObject) {
        self.removeFromSuperview()
        completion?(2)
    }
}
