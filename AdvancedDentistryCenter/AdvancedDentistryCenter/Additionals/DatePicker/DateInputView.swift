//
//  DateInputView.swift
//  Secure Dental
//
//  Created by SRS Web Solutions on 27/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateInputView: UIView {
    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!
      var dateFormat : String!
    
    var arrayStates: [String]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), for: UIControlEvents.valueChanged)
        
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(datePicker)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .time ? "hh:mm a" : "MMM dd, yyyy"
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .time ? "hh:mm a" : "MMM dd, yyyy"
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
        textField.resignFirstResponder()
    }
    class func addDatePickerForTextField(_ textField: UITextField) {
        
        self.addDatePickerForTextField(textField, minimumDate: nil, maximumDate: Date())
    }
    
    
    
    class func addDatePickerForTextField(_ textField: UITextField, minimumDate: Date?, maximumDate: Date?, dateFormat : String?) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        if let format = dateFormat {
            dateListView.dateFormat = format
        } else {
            dateListView.dateFormat = "MMM dd, yyyy"
        }
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = minimumDate
        dateListView.datePicker.maximumDate = maximumDate
    }

    
    class func addDatePickerForTextField(_ textField: UITextField, minimumDate: Date?, maximumDate: Date?) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = minimumDate
        dateListView.datePicker.maximumDate = maximumDate
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        let dateString = "1 Jan 1980"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }

    }
    
    class func addDatePickerForTextField(_ textField: UITextField, selectedDate : String) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = nil
        dateListView.datePicker.maximumDate = nil
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        let dateString = selectedDate
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
        
    }

    
    class func addExpiryDatePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = nil
        dateListView.datePicker.maximumDate = nil
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        
    }

//    class func addDatePickerForTextField(textField: UITextField, minimumDate: NSDate?, maximumDate: NSDate?, mode: UIDatePickerMode, interval: NSInteger) {
//        let dateListView = DateInputView(frame: CGRectMake(0, 0, screenSize.width, 260))
//        textField.inputView = dateListView
//        textField.inputAccessoryView = dateListView.toolbar
//        dateListView.textField = textField
//        dateListView.datePicker.minimumDate = minimumDate
//        dateListView.datePicker.maximumDate = maximumDate
//        dateListView.datePicker.datePickerMode = mode
//        dateListView.datePicker.minuteInterval = interval
//        
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "hh:mm a"
//        
//        dateListView.datePicker.setDate(dateFormatter.dateFromString("12:00 am")!, animated: true)
//    }
    
    
    
}
