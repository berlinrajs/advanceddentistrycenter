//
//  PopupButton.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/28/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

enum SelectionMode: Int {
    case single = 0
    case multiple
}

class PopupButton: UIView {

    static let sharedInstance = Bundle.main.loadNibNamed("PopupButton", owner: nil, options: nil)!.first as! PopupButton
    var completion:((PopupButton, Bool)->Void)?
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintLabelHeight: NSLayoutConstraint!
    
    var question: PDQuestion!
    var selectionMode: SelectionMode!
    
    
    override func awakeFromNib() {
        
    }
    
    func show(_ inViewController : UIViewController?, title : String?, selectionMode: SelectionMode!, question : PDQuestion, completion : @escaping (_ popupView: PopupButton, _ selected : Bool) -> Void) {
        self.question = question
        self.completion = completion
        self.selectionMode = selectionMode
        self.tableView.reloadData()
        if let controller = inViewController {
            controller.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        
        func setHeight(_ height : CGFloat) {
            if let options = question.options {
                let viewHeight = height + CGFloat(34 * options.count) + 78
                constraintViewHeight.constant = viewHeight
            }
        }
        
        if let t = title {
            labelTitle.text = t
            let labelHeight = t.heightWithConstrainedWidth(404, font: labelTitle.font) + 10
            constraintLabelHeight.constant = labelHeight
            setHeight(labelHeight)
        } else {
            setHeight(labelTitle.frame.height)
        }
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if inViewController != nil {
            inViewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func close() {
        self.removeFromSuperview()
    }
    
    func findSelected() -> PDOption? {
        for option in question.options! {
            if option.isSelected == true {
                return option
            }
        }
        return nil
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        if let _ = findSelected() {
            completion?(self, true)
        } else {
            completion?(self, false)
        }
    }
}


extension PopupButton: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = self.question.options {
            return self.question.options!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = Bundle.main.loadNibNamed("PopupButton", owner: nil, options: nil)![1] as? UITableViewCell
        }
        cell?.backgroundColor = UIColor.clear
        cell?.contentView.backgroundColor = UIColor.clear
        let obj = self.question.options![indexPath.row]
        let button = cell?.contentView.viewWithTag(101) as! UIButton
        if selectionMode == .single {
            button.setImage(UIImage(named: "RadioButtonUnChecked"), for: UIControlState())
            button.setImage(UIImage(named: "RadioButtonChecked"), for: .selected)
        } else {
            button.setImage(UIImage(named: "CheckBoxWhite"), for: UIControlState())
            button.setImage(UIImage(named: "CheckBoxWhiteTick"), for: .selected)
        }
        button.setTitle(" " + obj.question, for: UIControlState())
        button.isSelected = obj.isSelected!
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.question.options![indexPath.row]
        if selectionMode == .single {
            for option in question.options! {
                option.isSelected = false
            }
            obj.isSelected = true
            tableView.reloadData()
        } else {
            obj.isSelected = !obj.isSelected!
            tableView.reloadData()
        }

    }
}
