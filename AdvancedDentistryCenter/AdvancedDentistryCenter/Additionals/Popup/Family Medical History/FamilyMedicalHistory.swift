//
//  OralSurgeryPopup.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FamilyMedicalHistory: UIView {
    static let sharedInstance = Bundle.main.loadNibNamed("FamilyMedicalHistory", owner: nil, options: nil)!.first as! FamilyMedicalHistory
    
    
    class func popUpView() -> FamilyMedicalHistory {
        return Bundle.main.loadNibNamed("FamilyMedicalHistory", owner: nil, options: nil)!.first as! FamilyMedicalHistory
    }
    @IBOutlet weak var textView: PDTextView!
    @IBOutlet weak var textView2: PDTextView!
    @IBOutlet weak var textView3: PDTextView!
    var placeHolder : String = "PLEASE TYPE HERE"
    var completion:((String, String, String)->Void)?
    
    func show(_ inView: UIView, completion : @escaping (_ string1 : String, _ string2: String, _ string3: String) -> Void) {
        self.showinview(inView, placeHolder: "PLEASE TYPE HERE", completion: completion)
    }
    
    func showinview(_ inView: UIView, placeHolder : String, completion : @escaping (_ string1 : String, _ string2: String, _ string3: String) -> Void) {
        self.placeHolder = placeHolder
        self.completion = completion
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        inView.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textView.text = placeHolder
        textView.textColor = UIColor.lightGray
        textView2.text = placeHolder
        textView2.textColor = UIColor.lightGray
        textView3.text = placeHolder
        textView3.textColor = UIColor.lightGray

        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
       
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        self.removeFromSuperview()
        let string1 = textView.isEmpty || textView.text == "PLEASE TYPE HERE" ? "" : textView.text
        let string2 = textView2.isEmpty || textView2.text == "PLEASE TYPE HERE" ? "" : textView2.text
          let string3 = textView3.isEmpty || textView3.text == "PLEASE TYPE HERE" ? "" : textView3.text
        
        completion?(string1!, string2!, string3!)
    }
}


extension FamilyMedicalHistory: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolder {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
