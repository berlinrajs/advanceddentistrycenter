//
//  BoneSurgey1ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/2/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BoneSurgey1ViewController: PDViewController {

    @IBOutlet weak var patientSignature : SignatureView!
    @IBOutlet weak var witnessSignature : SignatureView!
    @IBOutlet weak var doctorSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !patientSignature.isSigned() || !witnessSignature.isSigned() || !doctorSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "BoneSurgeryFormVC") as! BoneSurgeryFormViewController
            new1VC.dictDetails = ["Patient" : patientSignature.signatureImage() , "Doctor" : doctorSignature.signatureImage(), "Witness" : witnessSignature.signatureImage()]
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
    }

}
