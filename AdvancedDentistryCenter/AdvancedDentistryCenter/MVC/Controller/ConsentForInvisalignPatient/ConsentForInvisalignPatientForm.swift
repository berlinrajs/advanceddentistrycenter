//
//  ConsentForInvisalignPatientForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 02/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentForInvisalignPatientForm: PDViewController {

    @IBOutlet weak var patientsign: UIImageView!
    
    @IBOutlet weak var patientName: UILabel!
    
    @IBOutlet weak var patientAddress: UILabel!
    
    @IBOutlet weak var patientCityStateZip: UILabel!
    
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    @IBOutlet weak var witnessSign: UIImageView!
    
    @IBOutlet weak var witnessName: UILabel!
    
    
    @IBOutlet weak var parentSign: UIImageView!
    
   
    @IBOutlet weak var doctorSign: UIImageView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        patientsign.image = patient.ConsentSign1
        witnessSign.image = patient.ConsentSign2
        parentSign.image = patient.ConsentSign3
         doctorSign.image = patient.ConsentSign4
        
        dateLabel.text = patient.dateToday
        
        patientName.text = patient.fullName
        patientAddress.text = patient.ConsentAddress
        patientCityStateZip.text = patient.ConsentCity + " , " + patient.ConsentState + " , "+patient.Consentzip
        witnessName.text = patient.ConsentWitnessname == "" ? "N/A" : patient.ConsentWitnessname
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
