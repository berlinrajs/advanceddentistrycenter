//
//  ConsentForInvisalignPatientVC.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 02/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentForInvisalignPatientVC1:PDViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func nextBtnPressed(_ sender: AnyObject) {
        
        
        let step2 = self.storyboard?.instantiateViewController(withIdentifier: "ConsentInvisalignVC2") as! ConsentForInvisalignPatientVC2
        step2.patient = patient
        self.navigationController?.pushViewController(step2, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
