//
//  ConsentForInvisalignPatientVC2.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 02/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentForInvisalignPatientVC2: PDViewController {

    @IBOutlet weak var witnessName: PDTextField!
    
    @IBOutlet weak var patientSignature: SignatureView!
    
    @IBOutlet weak var witnessSignature: SignatureView!
    
    @IBOutlet weak var doctorSign: SignatureView!
    
    @IBOutlet weak var parentSignature: SignatureView!
    
    @IBOutlet weak var labelDate1: DateLabel!
    
    @IBOutlet weak var labelDate2: DateLabel!
    
    @IBOutlet weak var labelDate3: DateLabel!
    
    @IBOutlet weak var labelDate4: DateLabel!
    @IBOutlet weak var textFieldAddressLine: PDTextField!
    
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
         labelDate4.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func doneBtnPressed(_ sender: AnyObject) {
        
        if !patientSignature.isSigned() || !witnessSignature.isSigned() || !parentSignature.isSigned() || !doctorSign.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped || !labelDate4.dateTapped{
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        }else if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            patient.ConsentSign1 = patientSignature.signatureImage()
             patient.ConsentSign2 = witnessSignature.signatureImage()
             patient.ConsentSign3 = parentSignature.signatureImage()
            
              patient.ConsentSign4 = doctorSign.signatureImage()
            
            patient.ConsentAddress = textFieldAddressLine.text
             patient.ConsentState = textFieldState.text
             patient.ConsentCity = textFieldCity.text
             patient.Consentzip = textFieldZipCode.text
            
            patient.ConsentWitnessname = witnessName.text
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kConsentInvisalignForm") as! ConsentForInvisalignPatientForm
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func findEmptyTextField() -> UITextField? {
        let textFields =  [textFieldAddressLine,  textFieldCity, textFieldState, textFieldZipCode]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }


}

extension ConsentForInvisalignPatientVC2 : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
       
      
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

