//
//  CrownLengtheningForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 03/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CrownLengtheningForm: PDViewController {
    
    @IBOutlet var patientSignature: UIImageView!
    @IBOutlet var dentistSignature: UIImageView!
    @IBOutlet var patientName: FormLabel!
    @IBOutlet var doctorName: FormLabel!
    @IBOutlet var labelDate1: FormLabel!
    @IBOutlet var labelDate2: FormLabel!
    @IBOutlet var labelDate3 : UILabel!
    @IBOutlet var witnessSignature : UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        patientSignature.image = patient.signature
        dentistSignature.image = patient.signature2
        patientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        doctorName.text = "Dr. Mati"
        labelDate3.text = patient.dateToday
        witnessSignature.image = patient.signature3

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
