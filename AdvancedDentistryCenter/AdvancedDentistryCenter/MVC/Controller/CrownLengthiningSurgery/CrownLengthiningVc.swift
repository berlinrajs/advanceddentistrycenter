//
//  CrownLengthiningVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 03/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CrownLengthiningVc: PDViewController {
    
    @IBOutlet var imageViewSignature: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    @IBOutlet var imageViewSignature3: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func buttonActionNext(_ sender: AnyObject) {
    if !imageViewSignature.isSigned() || !imageViewSignature2.isSigned() || !imageViewSignature3.isSigned(){
    let alert = Extention.alert("PLEASE SIGN THE FORM")
    self.present(alert, animated: true, completion: nil)
    }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
    let alert = Extention.alert("PLEASE SELECT THE DATE")
    self.present(alert, animated: true, completion: nil)
    }else{
    let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "CrownLengtheningForm") as! CrownLengtheningForm
        patient.signature = imageViewSignature.image
        patient.signature2 = imageViewSignature2.image
        patient.signature3 = imageViewSignature3.image
    new1VC.patient = self.patient
    self.navigationController?.pushViewController(new1VC, animated: true)
    
    }
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
