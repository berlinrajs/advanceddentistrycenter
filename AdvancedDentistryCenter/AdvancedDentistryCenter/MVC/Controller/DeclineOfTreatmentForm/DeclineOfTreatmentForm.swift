//
//  DeclineOfTreatmentForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 05/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DeclineOfTreatmentForm: PDViewController {
    
    
    @IBOutlet var labelPatientName: UILabel!
    
    @IBOutlet var labelDate1: UILabel!
    @IBOutlet var labelDate2: UILabel!
    @IBOutlet var labelDate3: UILabel!
    @IBOutlet var labelDate4: UILabel!
    @IBOutlet var labelDate5: UILabel!
    @IBOutlet var labeDate6: UILabel!
    
    @IBOutlet var imageViewPatientSignature: UIImageView!
    
    @IBOutlet var imageViewDentistSignature: UIImageView!
    
    @IBOutlet var imageViewWitnessSignature: UIImageView!
    
    @IBOutlet var imageViewPatient2: UIImageView!
    
    @IBOutlet var imageViewWitness2: UIImageView!
    
    @IBOutlet var labelRelationshipName: UILabel!

    @IBOutlet var labelRelationshipType: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        labelDate4.text = patient .dateToday
        labelDate5.text = patient.dateToday
        labeDate6.text = patient.dateToday
        labelRelationshipName.text = patient.relationshipName
        labelRelationshipType.text = patient.relationshipType
        imageViewPatientSignature.image = patient.signature
        imageViewDentistSignature.image = patient.signature2
        imageViewWitnessSignature.image  = patient.signature3
        imageViewPatient2.image = patient.signature
        imageViewWitness2.image = patient.signature3
       

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
