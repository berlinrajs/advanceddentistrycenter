//
//  EndodonticTheraphyVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 05/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticTheraphyVc: PDViewController {
    
    @IBOutlet var labelPatientNameChange: UILabel!
    
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3: DateLabel!
    var arrayEndodonticTheraphy : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        labelPatientNameChange.text = labelPatientNameChange.text!.replacingOccurrences(of: "KpatientName", with: "\(patient.fullName)")
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == KEndodonticTherapy
        }

          labelPatientNameChange.text = labelPatientNameChange.text!.replacingOccurrences(of: "KthNo", with: "\(form[0].toothNumbers)")
        let string : NSString = labelPatientNameChange.text! as NSString
        let range = string.range(of: patient.fullName )
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        //labelPatientNameChange.attributedText = attributedString
        
        let range2 = string.range(of: form[0].toothNumbers)
        //let attributedString1 = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        labelPatientNameChange.attributedText = attributedString

    }

    @IBAction func buttonMultipleSelection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if arrayEndodonticTheraphy.contains(sender.tag){
            arrayEndodonticTheraphy.remove(sender.tag)
        } else {
                arrayEndodonticTheraphy.add(sender.tag)
            }
            
        }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionDone(_ sender: AnyObject) {
        if arrayEndodonticTheraphy.count == 0{
            let alert = Extention.alert("PLEASE SELECT ANY ONE OF THE ABOVE PROCEDURES")
            self.present(alert, animated: true, completion: nil)
        }else if !imageViewPatientSignature.isSigned() || !imageViewDoctorSignature.isSigned() || !imageViewWitnessSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
           patient.arrayEndodonticQuestions = self.arrayEndodonticTheraphy
            patient.signature = imageViewPatientSignature.image
            patient.signature2 = imageViewWitnessSignature.image
            patient.signature3 = imageViewDoctorSignature.image
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "EndodonticTherapyForm") as!EndodonticTherapyForm
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }

}
