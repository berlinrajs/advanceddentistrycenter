//
//  EndodonticTherapyForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 05/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticTherapyForm: PDViewController {
    @IBOutlet var imageViewPatientSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: UIImageView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var labelDate1: FormLabel!
    @IBOutlet var labelDate2: FormLabel!
    @IBOutlet var labelDate3: FormLabel!

    @IBOutlet var labelPatientName: FormLabel!
   
    @IBOutlet var labelToothNo: FormLabel!
    
    @IBOutlet var btnCollection: [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        for btn in btnCollection {
            btn.isSelected = patient.arrayEndodonticQuestions.contains(btn.tag)
        }
        labelPatientName.text = patient.fullName
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == KEndodonticTherapy
        }
        labelToothNo.text =  form[0].toothNumbers
        
        imageViewPatientSignature.image = patient.signature
        imageViewDoctorSignature.image = patient.signature2
        imageViewWitnessSignature.image = patient.signature3
        
    }
        

        // Do any additional setup after loading the view.
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
