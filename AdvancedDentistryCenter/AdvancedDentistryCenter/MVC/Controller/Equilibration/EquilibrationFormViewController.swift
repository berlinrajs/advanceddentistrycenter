//
//  EquilibrationFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EquilibrationFormViewController: PDViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelPrintName : UILabel!
    @IBOutlet weak var labelRelationship : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var signPatient : UIImageView!
    @IBOutlet weak var signDentist : UIImageView!
    @IBOutlet weak var signWitness : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelPrintName.text = patient.equilibration.printName
        labelRelationship.text = patient.equilibration.relationship
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        signPatient.image = patient.equilibration.patientSignature
        signDentist.image = patient.equilibration.dentistSignature
        signWitness.image = patient.equilibration.witnessSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
