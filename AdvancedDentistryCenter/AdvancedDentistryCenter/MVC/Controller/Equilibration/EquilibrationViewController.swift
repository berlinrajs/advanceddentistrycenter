//
//  EquilibrationViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EquilibrationViewController: PDViewController {

    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var patientSignature : SignatureView!
    @IBOutlet weak var dentistSignature : SignatureView!
    @IBOutlet weak var witnessSignature : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var labelDate3 : DateLabel!
    @IBOutlet weak var radioSign : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onSignedButtonAction (_ sender : UIButton){
        if sender.tag == 1{
            viewContainer.isUserInteractionEnabled = false
            viewContainer.alpha = 0.5
            textfieldRelationship.text = ""
            textfieldName.text = ""
        }else{
            viewContainer.isUserInteractionEnabled = true
            viewContainer.alpha = 1.0
            
        }
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){

        if radioSign.selected.tag == 2 && (textfieldName.isEmpty || textfieldRelationship.isEmpty){
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
            
        }else if !patientSignature.isSigned() || !dentistSignature.isSigned() || !witnessSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            patient.equilibration.patientSignature = patientSignature.signatureImage()
            patient.equilibration.printName = radioSign.selected.tag == 1 ? "N/A" : textfieldName.text!
            patient.equilibration.relationship = radioSign.selected.tag == 1 ? "N/A" : textfieldRelationship.text!
            patient.equilibration.dentistSignature = dentistSignature.signatureImage()
            patient.equilibration.witnessSignature = witnessSignature.signatureImage()
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "EquilibrationFormVC") as! EquilibrationFormViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }


}

extension EquilibrationViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true

}
}
