//
//  FamilyMedicalHistoryForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 08/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FamilyMedicalHistoryForm: PDViewController {
    
    @IBOutlet var radioButton1: RadioButton!
    @IBOutlet var radioButton2: RadioButton!
    @IBOutlet var radioButton3: RadioButton!
    @IBOutlet var radioButton4: RadioButton!
    @IBOutlet var radioButton5: RadioButton!
    @IBOutlet var radioButton6: RadioButton!
    @IBOutlet var radioButton7: RadioButton!
    @IBOutlet var radioButton8: RadioButton!
    @IBOutlet var radioButton9: RadioButton!
    @IBOutlet var radioButton10: RadioButton!
    @IBOutlet var radioButton11: RadioButton!
    @IBOutlet var labelCancer: FormLabel!
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet var labelDate1: FormLabel!
    @IBOutlet var labelCollection1: [FormLabel]!
    @IBOutlet var labelCollection2: [FormLabel]!
    @IBOutlet var labelCollection3: [FormLabel]!
    @IBOutlet var imageViewSignature2: UIImageView!
    @IBOutlet var labelDate2: FormLabel!
    @IBOutlet var labelDate3: FormLabel!
//    @IBOutlet var labelDate4: FormLabel!
//    @IBOutlet var labelDate5: FormLabel!
    @IBOutlet var labelComments1: FormLabel!
//    @IBOutlet var labelComments2: FormLabel!
//    @IBOutlet var labelComments3: FormLabel!
    @IBOutlet var imageViewSignature3: UIImageView!
//    @IBOutlet var imageViewSignature4: UIImageView!
//    @IBOutlet var imageViewSignature5: UIImageView!
    @IBOutlet var imageViewSignature6: UIImageView!
//    @IBOutlet var imageViewSignature7: UIImageView!
//    @IBOutlet var imageViewSignature8: UIImageView!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelCancer.text = patient.buttontag10 == 0 ? "" : patient.popUpText
        imageViewSignature.image = patient.signature
        imageViewSignature2.image = patient.signature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
//        labelDate4.text = patient.dateToday
//        labelDate5.text = patient.dateToday
        radioButton1.setSelectedWithTag(patient.buttontag1!)
        radioButton2.setSelectedWithTag(patient.buttontag2!)
        radioButton3.setSelectedWithTag(patient.buttontag3!)
        radioButton4.setSelectedWithTag(patient.buttontag4!)
        radioButton5.setSelectedWithTag(patient.buttontag5!)
        radioButton6.setSelectedWithTag(patient.buttontag6!)
        radioButton7.setSelectedWithTag(patient.buttontag7!)
        radioButton8.setSelectedWithTag(patient.buttontag8!)
        radioButton9.setSelectedWithTag(patient.buttontag9!)
        radioButton10.setSelectedWithTag(patient.buttontag10!)
        radioButton11.setSelectedWithTag(patient.buttontag11!)
    patient.textViewOthers1.setTextForArrayOfLabels(labelCollection1)
    patient.textViewOthers2.setTextForArrayOfLabels(labelCollection2)
    patient.textViewOthers3.setTextForArrayOfLabels(labelCollection3)
        labelComments1.text =  patient.textViewCommments
//        labelComments2.text = patient.textViewCommments
//        labelComments3.text = patient.textViewCommments
        imageViewSignature3.image = patient.signature
//         imageViewSignature4.image = patient.signature
//         imageViewSignature5.image = patient.signature
       imageViewSignature6.image = patient.signature2
//        imageViewSignature7.image = patient.signature2
//        imageViewSignature8.image = patient.signature2
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
