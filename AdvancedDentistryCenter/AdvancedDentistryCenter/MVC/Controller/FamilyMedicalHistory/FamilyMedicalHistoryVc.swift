//
//  FamilyMedicalHistoryVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 08/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FamilyMedicalHistoryVc: PDViewController {
    
    @IBOutlet var imageViewSignature: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var textViewComments: PDTextView!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet var radiobutton1: RadioButton!
    @IBOutlet var radiobutton2: RadioButton!
    @IBOutlet var radiobutton3: RadioButton!
    @IBOutlet var radiobutton4: RadioButton!
    @IBOutlet var radiobutton5: RadioButton!
    @IBOutlet var radiobutton6: RadioButton!
    @IBOutlet var radiobutton7: RadioButton!
    @IBOutlet var radiobutton8: RadioButton!
    @IBOutlet var radiobutton9: RadioButton!
    @IBOutlet var radiobutton10: RadioButton!
    @IBOutlet var radiobutton11: RadioButton!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        sender.setSelectedWithTag(sender.tag)

          if sender.tag == 10
        {
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE TYPE HERE") { (textView, isEdited) in
                if isEdited {
                   self.patient.popUpText = textView.text
                } else {
                    sender.isSelected = false
                }
            }
            
        }
        
    }


@IBAction func buttonActionNext(_ sender: AnyObject) {
    if !buttonVerified.isSelected {
        let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        self.present(alert, animated: true, completion: nil)
    } else if !imageViewSignature.isSigned() || !imageViewSignature2.isSigned() {
        let alert = Extention.alert("PLEASE SIGN THE FORM")
        self.present(alert, animated: true, completion: nil)
    } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
        let alert = Extention.alert("PLEASE SELECT THE DATE")
        self.present(alert, animated: true, completion: nil)
    } else{
        let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "FamilyMedicalHistoryForm") as! FamilyMedicalHistoryForm
        patient.signature = imageViewSignature.image
        patient.signature2 = imageViewSignature2.image
        patient.buttontag1 = radiobutton1.selected.tag
        patient.buttontag2 = radiobutton2.selected.tag
         patient.buttontag3 = radiobutton3.selected.tag
         patient.buttontag4 = radiobutton4.selected.tag
         patient.buttontag5 = radiobutton5.selected.tag
         patient.buttontag6 = radiobutton6.selected.tag
         patient.buttontag7 = radiobutton7.selected.tag
         patient.buttontag8 = radiobutton8.selected.tag
         patient.buttontag9 = radiobutton9.selected.tag
         patient.buttontag10 = radiobutton10.selected.tag
        patient.buttontag11 = radiobutton11.selected.tag
        patient.textViewCommments = textViewComments.text == "COMMENTS" ? " " : textViewComments.text
        new1VC.patient = self.patient
        self.navigationController?.pushViewController(new1VC, animated: true)
        
    }
}
}


extension FamilyMedicalHistoryVc : UITextViewDelegate {
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.text == "COMMENTS" {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
//                textView.text = textViewComments.text
                textView.textColor = UIColor.lightGray
            }
        }
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
            }
            return true
        }
    }





