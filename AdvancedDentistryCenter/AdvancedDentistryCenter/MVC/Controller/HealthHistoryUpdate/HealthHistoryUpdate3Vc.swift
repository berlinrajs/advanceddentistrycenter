//
//  HealthHistoryUpdate3Vc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 10/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HealthHistoryUpdate3Vc: PDViewController {
    
    @IBOutlet weak var textFieldHealthChange: PDTextField!
    
    
    @IBOutlet weak var textViewComments1: PDTextView!
    
    @IBOutlet weak var textFieldPhysiciansName: PDTextField!
    
    @IBOutlet weak var textFieldPhysicianPhone: PDTextField!
    
    @IBOutlet weak var textViewComments2: PDTextView!
    
    @IBOutlet weak var textFieldLastExam: PDTextField!
    
    @IBOutlet weak var textFieldAllergies: PDTextField!
    
    @IBOutlet weak var imageViewInitials: SignatureView!
    
    @IBOutlet weak var imageViewSignature: SignatureView!
    
    @IBOutlet weak var labelDate: DateLabel!
    
    
    @IBOutlet weak var labelPatientName : UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        DateInputView.addDatePickerForTextField(textFieldHealthChange, selectedDate: "1 Jan 2016")
        DateInputView.addDatePickerForTextField(textFieldLastExam, selectedDate: "1 Jan 2016")
        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !imageViewSignature.isSigned() || !imageViewInitials.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            
            self.view.endEditing(true)
            
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "HealthHistoryUpdateForm") as! HealthHistoryUpdateForm
            patient.healthDateChange2 = textFieldHealthChange.text
            patient.healthComments3 =  textViewComments1.text == "COMMENTS" ? " " : textViewComments1.text
            patient.healthPhysicianName2 = textFieldPhysiciansName.text
            patient.healthPhysiciansPhone2 = textFieldPhysicianPhone.text
            patient.healthComments4 =  textViewComments2.text == "COMMENTS" ? " " : textViewComments2.text
            patient.healthLastPhysicialExam2 =  textFieldLastExam.text
            patient.healthAllergies2 = textFieldAllergies.text
            patient.signature3 = imageViewInitials.image
            patient.signature4 = imageViewSignature.image
            new1VC.patient = patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
}

    
    extension HealthHistoryUpdate3Vc: UITextFieldDelegate {
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if textField == textFieldPhysicianPhone {
                return textField.formatPhoneNumber(range, string: string)
            }
            return true
        }
        
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    }
    
    extension HealthHistoryUpdate3Vc: UITextViewDelegate {
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.text == "COMMENTS" {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                //             textView.text = textViewComments.text
                textView.textColor = UIColor.lightGray
            }
        }
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
            }
            return true
        }
}



