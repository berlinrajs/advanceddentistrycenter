//
//  HealthHistoryUpdateForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 10/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HealthHistoryUpdateForm: PDViewController {
    
    
    @IBOutlet weak var labelTodaysDate: FormLabel!
    @IBOutlet weak var labelPatientNumber: FormLabel!
    @IBOutlet weak var labelFirstName: FormLabel!
    @IBOutlet weak var labelMiddleName: FormLabel!
    @IBOutlet weak var labelLastName: FormLabel!
    @IBOutlet weak var labelAddress: FormLabel!
    @IBOutlet weak var labelCity: FormLabel!
    @IBOutlet weak var labelState: FormLabel!
    @IBOutlet weak var labelzip: FormLabel!
    @IBOutlet weak var labelHomePhone: FormLabel!
    @IBOutlet weak var labelWorkPhone: FormLabel!
    @IBOutlet weak var labelCellPhone: FormLabel!
     @IBOutlet weak var labelEmail: FormLabel!
     @IBOutlet weak var labelFax: FormLabel!
    @IBOutlet var labelAnythingElse: [FormLabel]!
    @IBOutlet weak var labelHealthChange: FormLabel!
    @IBOutlet var labelComments1: [FormLabel]!
    @IBOutlet weak var labelPhysiciansName: FormLabel!
    @IBOutlet weak var labelPhysiciansPhone: FormLabel!
    @IBOutlet var labelCurrentMedications: [FormLabel]!
    @IBOutlet weak var labelLastPhysicalExam: FormLabel!
    @IBOutlet weak var labelAnyAllergies: FormLabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelDate: FormLabel!
    // second section
    @IBOutlet weak var labelHealthChange2: FormLabel!
    @IBOutlet var labelComments2: [FormLabel]!
    @IBOutlet weak var labelPhysiciansName2: FormLabel!
    @IBOutlet weak var labelPhysiciansPhone2: FormLabel!
    @IBOutlet var labelCurrentMedications2: [FormLabel]!
    @IBOutlet weak var labelLastPhysicalExam2: FormLabel!
    @IBOutlet weak var labelAnyAllergies2: FormLabel!
    @IBOutlet weak var imageViewSignature3: UIImageView!
    @IBOutlet weak var imageViewSignature4: UIImageView!
    @IBOutlet weak var labelDate2: FormLabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        if patient.buttontag1 == 1 {
            labelTodaysDate.text = patient.dateToday
            
            labelFirstName.text = patient.firstName
            labelMiddleName.text = patient.initial
            labelLastName.text = patient.lastName
            labelAddress.text = patient.addressLine
            labelCity.text = patient.city
            labelState.text = patient.state
            labelzip.text = patient.zipCode
            labelHomePhone.text = patient.homePhone
            labelWorkPhone.text = patient.workPhone
            labelCellPhone.text = patient.phoneNumber
            labelEmail.text = patient.email
            labelFax.text = patient.fax
            patient.textViewOthers1.setTextForArrayOfLabels(labelAnythingElse)
            
            labelHealthChange.text = patient.healthDateChange
            patient.healthComments1.setTextForArrayOfLabels(labelComments1)
            labelPhysiciansName.text = patient.healthPhysicianName
            labelPhysiciansPhone.text = patient.healthPhysiciansPhone
            patient.healthComments2.setTextForArrayOfLabels(labelCurrentMedications)
            labelLastPhysicalExam.text = patient.healthLastPhysicialExam
            labelAnyAllergies.text = patient.healthAllergies
            imageViewSignature.image = patient.signature
            imageViewSignature2.image = patient.signature2
            
            let form = patient.selectedForms.filter { (formObj) -> Bool in
                return formObj.formTitle == kHealthHistoryUpdate
            }
        labelPatientNumber.text =  form[0].toothNumbers
        labelHealthChange2.text = patient.healthDateChange2
        patient.healthComments3.setTextForArrayOfLabels(labelComments2)
        labelPhysiciansName2.text = patient.healthPhysicianName2
        labelPhysiciansPhone2.text = patient.healthPhysiciansPhone2
        patient.healthComments4.setTextForArrayOfLabels(labelCurrentMedications2)
        labelLastPhysicalExam2.text = patient.healthLastPhysicialExam2
        labelAnyAllergies2.text = patient.healthAllergies2
        imageViewSignature3.image = patient.signature4
        imageViewSignature4.image = patient.signature3
            labelDate.text = patient.dateToday
            labelDate2.text = patient.dateToday
      
        }
        else {
                   let form = patient.selectedForms.filter { (formObj) -> Bool in
                return formObj.formTitle == kHealthHistoryUpdate
            }
          labelPatientNumber.text =  form[0].toothNumbers
            labelTodaysDate.text = patient.dateToday
            labelFirstName.text = patient.firstName
            labelMiddleName.text = patient.initial
            labelLastName.text = patient.lastName
            labelAddress.text = patient.addressLine
            labelCity.text = patient.city
            labelState.text = patient.state
            labelzip.text = patient.zipCode
            labelHomePhone.text = patient.homePhone
            labelWorkPhone.text = patient.workPhone
            labelCellPhone.text = patient.phoneNumber
            labelEmail.text = patient.email
            labelFax.text = patient.fax
            patient.textViewOthers1.setTextForArrayOfLabels(labelAnythingElse)
            labelHealthChange.text = patient.healthDateChange
            patient.healthComments1.setTextForArrayOfLabels(labelComments1)
            labelPhysiciansName.text = patient.healthPhysicianName
            labelPhysiciansPhone.text = patient.healthPhysiciansPhone
            patient.healthComments2.setTextForArrayOfLabels(labelCurrentMedications)
            labelLastPhysicalExam.text = patient.healthLastPhysicialExam
            labelAnyAllergies.text = patient.healthAllergies
            imageViewSignature.image = patient.signature2
            imageViewSignature2.image = patient.signature
            labelDate.text = patient.dateToday
  
        }
        
        
        // Do any additional setup after loading the view.
    }

   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
