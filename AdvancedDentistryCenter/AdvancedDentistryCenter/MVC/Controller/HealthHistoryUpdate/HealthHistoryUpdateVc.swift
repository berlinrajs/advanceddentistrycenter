//
//  HealthHistoryUpdateVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 10/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HealthHistoryUpdateVc: PDViewController {
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldHomePhone: PDTextField!
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    @IBOutlet weak var textFieldCellPhone: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldFax: PDTextField!
    @IBOutlet weak var textViewOther: PDTextView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textFieldState)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }
        else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldHomePhone.isEmpty && !textFieldHomePhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE")
            self.present(alert, animated: true, completion: nil)
        }
        else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID CELL PHONE")
            self.present(alert, animated: true, completion: nil)
            
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER VALID EMAIL")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldFax.isEmpty && !textFieldFax.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID FAX NUMBER")
            self.present(alert, animated: true, completion: nil)

        } else {
            patient.addressLine = textFieldAddress.text
            patient.city = textFieldCity.text
            patient.phoneNumber = textFieldHomePhone.isEmpty ? "N/A" : textFieldHomePhone.text
            patient.state = textFieldState.text
            patient.zipCode = textFieldZipCode.text
            patient.homePhone = textFieldHomePhone.isEmpty ? "N/A" : textFieldHomePhone.text
            patient.workPhone = textFieldWorkPhone.isEmpty ? "N/A" : textFieldWorkPhone.text
            patient.phoneNumber = textFieldCellPhone.isEmpty ? "N/A" : textFieldCellPhone.text
            patient.email = textFieldEmail.isEmpty ? "N/A" : textFieldEmail.text
            patient.fax = textFieldFax.isEmpty ? "N/A" : textFieldFax.text
            patient.textViewOthers1 = textViewOther.text == "TYPE HERE" ? "N/A" : textViewOther.text
            
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "HealthHistoryUpdateVc2") as! HealthHistoryUpdateVc2
            new1VC.patient = patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldAddress, textFieldAddress, textFieldCity, textFieldState, textFieldZipCode]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
    
}

extension HealthHistoryUpdateVc: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldHomePhone || textField == textFieldCellPhone || textField == textFieldWorkPhone || textField == textFieldFax {
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension HealthHistoryUpdateVc: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" || textView.text == "" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

