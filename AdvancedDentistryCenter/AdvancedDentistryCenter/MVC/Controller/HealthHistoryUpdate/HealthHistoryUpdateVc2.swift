//
//  HealthHistoryUpdateVc2.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 10/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HealthHistoryUpdateVc2: PDViewController {
    
    
    @IBOutlet weak var textFieldHealthChange: PDTextField!
    
   
    @IBOutlet weak var textViewComments1: PDTextView!

    @IBOutlet weak var textFieldPhysiciansName: PDTextField!
    
    @IBOutlet weak var textFieldPhysicianPhone: PDTextField!
    
    @IBOutlet weak var textViewComments2: PDTextView!
    
    @IBOutlet weak var textFieldLastExam: PDTextField!
    
    @IBOutlet weak var textFieldAllergies: PDTextField!
    
    @IBOutlet weak var imageViewInitials: SignatureView!
    
    @IBOutlet weak var imageViewSignature: SignatureView!
    
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var radiobutton: RadioButton!
    
    @IBOutlet weak var labelPatientName : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        DateInputView.addDatePickerForTextField(textFieldHealthChange, selectedDate: "1 Jan 2016")
        DateInputView.addDatePickerForTextField(textFieldLastExam, selectedDate: "1 Jan 2016")
        labelPatientName.text = patient.fullName

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        sender.setSelectedWithTag(sender.tag)
      
    }
 
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if radiobutton.selected == nil {
            let alert = Extention.alert("PLEASE SELECT WHETHER YOU HAVE ANY ADDITIONAL CHANGES")
            self.present(alert, animated: true, completion: nil)
        }else if !imageViewSignature.isSigned() || !imageViewInitials.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)

        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)

        }
       else  if radiobutton.selected.tag == 1 {
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "HealthHistoryUpdate3Vc") as! HealthHistoryUpdate3Vc
            patient.buttontag1 = radiobutton.selected.tag
           patient.healthDateChange = textFieldHealthChange.text
           patient.healthComments1 = textViewComments1.text == "COMMENTS" ? " " : textViewComments1.text
            patient.healthPhysicianName = textFieldPhysiciansName.text
            patient.healthPhysiciansPhone = textFieldPhysicianPhone.text
           patient.healthComments2 =  textViewComments2.text == "COMMENTS" ? " " : textViewComments2.text
           patient.healthLastPhysicialExam =  textFieldLastExam.text
          patient.healthAllergies = textFieldAllergies.text
            patient.signature = imageViewSignature.image
           patient.signature2 = imageViewInitials.image
            new1VC.patient = patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else {
      
        let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "HealthHistoryUpdateForm") as! HealthHistoryUpdateForm
            patient.buttontag1 = radiobutton.selected.tag
            patient.healthDateChange = textFieldHealthChange.text
            patient.healthComments1 =  textViewComments1.text == "COMMENTS" ? " " : textViewComments1.text
            patient.healthPhysicianName = textFieldPhysiciansName.text
            patient.healthPhysiciansPhone = textFieldPhysicianPhone.text
            patient.healthComments2 =  textViewComments2.text == "COMMENTS" ? " " : textViewComments2.text
            patient.healthLastPhysicialExam =  textFieldLastExam.text
            patient.healthAllergies = textFieldAllergies.text
            patient.signature = imageViewInitials.image
            patient.signature2 = imageViewSignature.image
        new1VC.patient = patient
        self.navigationController?.pushViewController(new1VC, animated: true)
    }
}


}


extension HealthHistoryUpdateVc2: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhysicianPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension HealthHistoryUpdateVc2: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "COMMENTS" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            //             textView.text = textViewComments.text
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

