//
//  HomeViewController.swift
//  Advanced Dentistry Center
//
//  Created by Leojin Bose on 17/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class HomeViewController: UIViewController {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet var labelAlertFooter: UILabel!
    @IBOutlet var labelAlertHeader: UILabel!
    @IBOutlet var viewToothNumbers: PDView!
    var textViewOthers1 : String!
    var textViewOthers2 : String!
    var textViewOthers3 : String!

    var privacyPracticeTag : Int = 0
    var isPrivacySkipPressed : Bool = false
    var privacyOthers : String = ""
    var privacySignatureOffice : UIImage = UIImage()
    var patientNumber : String = "N/A"

    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    var consentIndex  = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showAlert), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.string(from: NSDate() as Date).uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "Advanced Dentistry Center", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
//                let credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
//                if credentials.canAuthorize {
//                    buttonLogout.hidden = false
//                } else {
//                    buttonLogout.hidden = true
//                }
//        
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        selectedForms.removeAll()
        for (idx, form) in formList.enumerated() {
            if form.isSelected == true {
                if idx == consentIndex  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sort(by: { (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
            
            let patient1 = PDPatient(forms: selectedForms)
            patient1.dateToday = labelDate.text
            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
            patient1.textViewOthers1 = textViewOthers1
            patient1.textViewOthers2 = textViewOthers2
            patient1.textViewOthers3 = textViewOthers3
            patient1.privacyOthers = self.privacyOthers
            patient1.privacyPracticeTag = self.privacyPracticeTag
            patient1.isPrivacySkipPressed = self.isPrivacySkipPressed
            patient1.privacySignatureOffice = self.privacySignatureOffice
            patient1.patientNumber = self.patientNumber
            patientInfoVC.patient = patient1
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func buttonActionLogout(_ sender: AnyObject) {
        //        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
        //        buttonLogout.hidden = true
    }
    @IBAction func buttonActionDone(_ sender: AnyObject) {
            textFieldToothNumbers.resignFirstResponder()
            self.viewToothNumbers.removeFromSuperview()
            self.viewShadow.isHidden = true
            let form = textFieldToothNumbers.tag <= consentIndex ? formList[textFieldToothNumbers.tag] : formList[consentIndex].subForms[textFieldToothNumbers.tag - (consentIndex + 1)]
            if !textFieldToothNumbers.isEmpty {
                form.isSelected = true
                form.toothNumbers = textFieldToothNumbers.text
            } else {
                form.isSelected = false
            }
            self.tableViewForms.reloadData()
            
        }

    func showAlert() {
        viewAlert.isHidden = false
        viewShadow.isHidden = false
    }
    
    @IBAction func buttonActionOk(_ sender: AnyObject) {
        
        viewAlert.isHidden = true
        viewShadow.isHidden = true
    }
//    @IBAction func popUpDonePressed(sender: AnyObject) {
//        
//        textFieldToothNumbers.resignFirstResponder()
//        textFieldNPO.resignFirstResponder()
//        self.viewToothNumbers.removeFromSuperview()
//        self.viewShadow.hidden = true
//        let form = textFieldToothNumbers.tag <= consentIndex ? formList[textFieldToothNumbers.tag] : formList[consentIndex].subForms[textFieldToothNumbers.tag - (consentIndex + 1)]
//        if !textFieldToothNumbers.isEmpty {
//            form.isSelected = true
//            form.toothNumbers = textFieldToothNumbers.text
//            form.toothNPO = textFieldNPO.text
//        } else {
//            form.isSelected = false
//        }
//        self.tableViewForms.reloadData()
//        
//    }
    
    
}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890,").inverted) != nil {
            return false
        }
        
        let newRange = textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location)..<textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = textField.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                textField.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
                if lastString == "," {
                    textField.text = "0" + textFieldString
                    return false
                }
                
            }
            if textFieldString == "," {
                return false
            }
        }
        
        
        
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms
            for subFrom in subForms! {
                subFrom.isSelected = false
            }
            let form = self.formList[consentIndex]
            form.isSelected = !form.isSelected
            var indexPaths : [IndexPath] = [IndexPath]()
            for (idx, _) in form.subForms.enumerated() {
                let indexPath = IndexPath(row: consentIndex + 1 + idx, section: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.beginUpdates()
                tableView.insertRows(at: indexPaths, with: .bottom)
                tableView.endUpdates()
                let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    if form.isSelected == true {
                        tableView.scrollToRow(at: indexPaths.last!, at: .bottom, animated: true)
                    }
                }
            } else {
                tableView.beginUpdates()
                tableView.deleteRows(at: indexPaths, with: .bottom)
                tableView.endUpdates()
            }
            tableView.reloadRows(at: [indexPath], with: .none)
            return
        }
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
        } else {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        }
            if form.isToothNumberRequired == true && !form.isSelected {
                textFieldToothNumbers.tag = indexPath.row
                if indexPath.row == 2 {
                    textFieldToothNumbers.placeholder = "PLEASE TYPE"
                    labelAlertHeader.text = "PATIENT NUMBER"
                    labelAlertFooter.text = "If Any"
                    textFieldToothNumbers.delegate = nil
                    textFieldToothNumbers.keyboardType = UIKeyboardType.numberPad
                } else {
                    textFieldToothNumbers.placeholder = "01, 02, 15, 18"
                    labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
                    labelAlertFooter.text = "Note: Separate with commas"
                    textFieldToothNumbers.delegate = self
                    textFieldToothNumbers.keyboardType = UIKeyboardType.numberPad
                }
        self.showPopup()
        }
        if !form.isSelected && indexPath.row == 1 {
            
            FamilyMedicalHistory.sharedInstance.showinview(self.view, placeHolder: "PLEASE TYPE HERE", completion: { (string1, string2, string3) in
                self.textViewOthers1 = string1
                self.textViewOthers2 = string2
                self.textViewOthers3 = string3
                form.isSelected = true
                self.tableViewForms.reloadData()
            })
        }
        else {
            form.isSelected = !form.isSelected
            tableView.reloadData()
        }
        if form.isSelected && indexPath.row ==  0{
            PopupTextField.sharedInstance.showWithPlaceHolder("PATIENT NUMBER *", keyboardType: UIKeyboardType.numbersAndPunctuation, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patientNumber = textField.text!
                    PrivacyAlert.sharedInstance.showPopUp(self,date: self.labelDate.text!, completion: { (acknowledgementTag, acknowledgementReason, signature, efforts, isOkSelected) in
                        self.privacyPracticeTag = acknowledgementTag
                        self.privacyOthers = acknowledgementReason
                        self.isPrivacySkipPressed = !isOkSelected
                        self.privacySignatureOffice = signature
                        }, showAlert: { (alertMessage) in
                            let alert = Extention.alert(alertMessage)
                            self.present(alert, animated: true, completion: nil)
                    })

                }else{
                    self.patientNumber = "N/A"
                    form.isSelected = false
                    self.tableViewForms.reloadData()
                }
            })
        }
        
//        if form.isSelected && indexPath.row ==  0 { //consentIndex + 19
//            PrivacyAlert.sharedInstance.showPopUp(self.view,date: labelDate.text!, completion: { (acknowledgementTag, acknowledgementReason, signature, efforts, isOkSelected) in
//                self.privacyPracticeTag = acknowledgementTag
//                self.privacyOthers = acknowledgementReason
//                self.isPrivacySkipPressed = !isOkSelected
//                self.privacySignatureOffice = signature
//                }, showAlert: { (alertMessage) in
//                    let alert = Extention.alert(alertMessage)
//                    self.presentViewController(alert, animated: true, completion: nil)
//            })
//        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: (indexPath.row < (consentIndex + 1)) ? "cellMainForm" : "cellSubForm") as! FormsTableViewCell
        let form = indexPath.row < (consentIndex + 1) ? formList[indexPath.row] : formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        let height = form.formTitle.heightWithConstrainedWidth(cell.labelFormName.frame.width, font: cell.labelFormName.font) + 24
        return height
    }

}
extension HomeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0 {
            let subForms = formList[consentIndex].subForms
            return formList[consentIndex].isSelected == true ? formList.count + subForms!.count  : formList.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row < (consentIndex + 1)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
            let form = formList[indexPath.row]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }
}


