//
//  ImplantPatientInformationForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 03/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantPatientInformationForm: PDViewController {
    @IBOutlet var imageViewSignature1: UIImageView!
    @IBOutlet var imageViewSignature2: UIImageView!
    @IBOutlet var imageViewSignature3: UIImageView!
    @IBOutlet var imageViewSignature4: UIImageView!
    @IBOutlet var labelPatientName: FormLabel!
    @IBOutlet var labelDate: FormLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature1.image = patient.signature
        imageViewSignature2.image = patient.signature2
        imageViewSignature3.image = patient.signature3
        imageViewSignature4.image = patient.signature4
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
