//
//  ImplantPatientInformationVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 03/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantPatientInformationVc: PDViewController {
    
    @IBOutlet var imageViewPtIntials: SignatureView!
    @IBOutlet var imageViewSignature1: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    @IBOutlet var imageViewSignature3: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !imageViewPtIntials.isSigned() || !imageViewSignature1.isSigned() || !imageViewSignature2.isSigned() || !imageViewSignature3.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "ImplantPatientInformationForm") as! ImplantPatientInformationForm
            patient.signature = imageViewPtIntials.image
            patient.signature2 = imageViewSignature1.image
            patient.signature3 = imageViewSignature2.image
            patient.signature4 = imageViewSignature3.image
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
