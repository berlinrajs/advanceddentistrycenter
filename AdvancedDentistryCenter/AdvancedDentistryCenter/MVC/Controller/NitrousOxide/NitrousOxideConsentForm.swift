//
//  NitrousOxideConsentForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 03/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NitrousOxideConsentForm: PDViewController {
    
    @IBOutlet var imageViewPatientSignature: UIImageView!
    @IBOutlet var imageViewWitnessSignature: UIImageView!
    @IBOutlet var imageViewDoctorSignature: UIImageView!
    @IBOutlet var labelDate1: FormLabel!
    @IBOutlet var labelDate2: FormLabel!
    @IBOutlet var labelDate3: FormLabel!
    @IBOutlet var labelIdentification: FormLabel!
    @IBOutlet var labelWitnessName: FormLabel!
    @IBOutlet var buttonSelected1: UIButton!
    @IBOutlet var buttonSelected2: UIButton!
    @IBOutlet var buttonSelected3: UIButton!
    @IBOutlet var buttonSelected4: UIButton!
    @IBOutlet var buttonSelected5: UIButton!
    @IBOutlet var buttonSelected6: UIButton!
    @IBOutlet var buttonSelected7: UIButton!
    @IBOutlet var buttonSelected8: UIButton!
    @IBOutlet var buttonSelected9: UIButton!
    @IBOutlet var buttonSelected10: UIButton!
    @IBOutlet var buttonSelected11: UIButton!
    @IBOutlet var buttonSelected12: UIButton!
    @IBOutlet var buttonSelected13: UIButton!
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewPatientSignature.image = patient.signature
        imageViewWitnessSignature.image = patient.signature2
        imageViewDoctorSignature.image = patient.signature3
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        labelIdentification.text = patient.patientIdentification
        labelWitnessName.text = patient.WitnessName
        buttonSelected1.isSelected = patient.isSelectedButton1
        buttonSelected2.isSelected = patient.isSelectedButton2
        buttonSelected3.isSelected = patient.isSelectedButton3
        buttonSelected4.isSelected = patient.isSelectedButton4
        buttonSelected5.isSelected = patient.isSelectedButton5
        buttonSelected6.isSelected = patient.isSelectedButton6
        buttonSelected7.isSelected = patient.isSelectedButton7
        buttonSelected8.isSelected = patient.isSelectedButton8
        buttonSelected9.isSelected = patient.isSelectedButton9
        buttonSelected10.isSelected = patient.isSelectedButton10
        buttonSelected11.isSelected = patient.isSelectedButton11
        buttonSelected12.isSelected = patient.isSelectedButton12
        buttonSelected13.isSelected = patient.isSelectedButton13
//        for button in buttonSelection {
//            button.selected = patient.nitrousOxideArray.contains(button.tag)
//        }

 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
