//
//  NitroxideOxideConsentVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 03/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NitroxideOxideConsentVc: PDViewController {

    
    @IBOutlet var imageViewPatientSignature: SignatureView!
    @IBOutlet var imageViewWitnessSignature: SignatureView!
    @IBOutlet var imageViewDoctorSignature: SignatureView!
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    @IBOutlet var labelDate3: DateLabel!
    @IBOutlet var txtFieldIdentification: PDTextField!
    @IBOutlet var txtFieldWitnessName: PDTextField!
//    var arrayNitrousOxide : NSMutableArray = NSMutableArray()
    @IBOutlet var buttonSelected1: UIButton!
    @IBOutlet var buttonSelected2: UIButton!
    @IBOutlet var buttonSelected3: UIButton!
    @IBOutlet var buttonSelected4: UIButton!
    @IBOutlet var buttonSelected5: UIButton!
    @IBOutlet var buttonSelected6: UIButton!
    @IBOutlet var buttonSelected7: UIButton!
    @IBOutlet var buttonSelected8: UIButton!
    @IBOutlet var buttonSelected9: UIButton!
    @IBOutlet var buttonSelected10: UIButton!
    @IBOutlet var buttonSelected11: UIButton!
     @IBOutlet var buttonSelected12: UIButton!
     @IBOutlet var buttonSelected13: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
    }
    
    @IBAction func multiplebuttonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if !buttonSelected5.isSelected == true{
            buttonSelected6.isUserInteractionEnabled = false
            buttonSelected6.isSelected = false
            buttonSelected6.alpha = 0.5
            buttonSelected7.isUserInteractionEnabled = false
            buttonSelected7.isSelected = false
            buttonSelected7.alpha = 0.5
            buttonSelected8.isUserInteractionEnabled = false
            buttonSelected8.isSelected = false
            buttonSelected8.alpha = 0.5
        } else {
            buttonSelected6.isUserInteractionEnabled = true
            buttonSelected6.alpha = 1
            buttonSelected7.isUserInteractionEnabled = true
            buttonSelected7.alpha = 1
            buttonSelected8.isUserInteractionEnabled = true
            buttonSelected8.alpha = 1
        }
        
//        if arrayNitrousOxide.containsObject(sender.tag)
//        {
//            arrayNitrousOxide.removeObject(sender.tag)
//        } else
//        {
//            arrayNitrousOxide.addObject(sender.tag)
//        }
        
    }

    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !buttonSelected1.isSelected && !buttonSelected2.isSelected && !buttonSelected3.isSelected && !buttonSelected4.isSelected && !buttonSelected5.isSelected && !buttonSelected9.isSelected && !buttonSelected10.isSelected && !buttonSelected11.isSelected && !buttonSelected12.isSelected && !buttonSelected13.isSelected{
            let alert = Extention.alert("PLEASE SELECT ANY CONFIRMATION ")
            self.present(alert, animated: true, completion: nil)
        }else if buttonSelected5.isSelected == true && !buttonSelected6.isSelected && !buttonSelected7.isSelected && !buttonSelected8.isSelected{
                let alert = Extention.alert("PLEASE SELECT ANY CONFIRMATION ")
                self.present(alert, animated: true, completion: nil)
        }else if !imageViewPatientSignature.isSigned() || !imageViewWitnessSignature.isSigned() || !imageViewDoctorSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if txtFieldIdentification.isEmpty || txtFieldWitnessName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE ALL REQUIRED FIELDS ")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "NitrousOxideConsentForm") as! NitrousOxideConsentForm
            patient.signature = imageViewPatientSignature.image
            patient.signature2 = imageViewWitnessSignature.image
            patient.signature3 = imageViewDoctorSignature.image
            patient.patientIdentification = txtFieldIdentification.text
            patient.WitnessName = txtFieldWitnessName.text
            patient.isSelectedButton1 = buttonSelected1.isSelected
            patient.isSelectedButton2 = buttonSelected2.isSelected
            patient.isSelectedButton3 = buttonSelected3.isSelected
            patient.isSelectedButton4 = buttonSelected4.isSelected
            patient.isSelectedButton5 = buttonSelected5.isSelected
            patient.isSelectedButton6 = buttonSelected6.isSelected
            patient.isSelectedButton7 = buttonSelected7.isSelected
            patient.isSelectedButton8 = buttonSelected8.isSelected
            patient.isSelectedButton9 = buttonSelected9.isSelected
            patient.isSelectedButton10 = buttonSelected10.isSelected
            patient.isSelectedButton11 = buttonSelected11.isSelected
            patient.isSelectedButton12 = buttonSelected12.isSelected
            patient.isSelectedButton13 = buttonSelected13.isSelected
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
