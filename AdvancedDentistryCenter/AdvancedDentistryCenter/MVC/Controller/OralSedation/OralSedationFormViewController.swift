//
//  OralSedationFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/3/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSedationFormViewController: PDViewController {

    var dictDetails : NSDictionary!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var signPatient : UIImageView!
    @IBOutlet weak var signDoctor : UIImageView!
    @IBOutlet weak var signWitness : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        signPatient.image = dictDetails["Patient"] as? UIImage
        signDoctor.image = dictDetails["Doctor"] as? UIImage
        signWitness.image = dictDetails["Witness"] as? UIImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
