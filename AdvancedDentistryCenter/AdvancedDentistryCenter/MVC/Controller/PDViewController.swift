//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let consentStoryBoard = UIStoryboard(name: "Consent", bundle: Bundle.main)
let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
let adultStoryBoard = UIStoryboard(name: "AdultRegistration", bundle: Bundle.main)
let patientStoryBoard = UIStoryboard(name: "PatientInformation", bundle: Bundle.main)


class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: PDPatient!
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    var isNotFirstForm: Bool {
        get {
            return navigationController?.viewControllers.count > 2 ? true : false
        }
    }
    var completion: ((_ success: Bool) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = isFromPreviousForm && buttonSubmit == nil
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonBackAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed (_ sender : UIButton){
    
        self.buttonSubmit?.isUserInteractionEnabled = false
        self.buttonBack?.isUserInteractionEnabled = false
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Advanced Dentistry Center", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            self.buttonSubmit?.isUserInteractionEnabled = true
            self.buttonBack?.isUserInteractionEnabled = true
            return
        }
        uploadPdfToDrive()
        
    }
    func sendFormToGoogleDrive(completion: @escaping (_ success: Bool) -> Void) {
        self.uploadPdfToDrive()
    }
    
    func uploadPdfToDrive (){
        let pdfManager = PDFManager.sharedInstance()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        if let _ = self.completion {
                            self.completion!(true)
                        } else {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        }
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                        self.buttonSubmit?.isUserInteractionEnabled = true
                        self.buttonBack?.isUserInteractionEnabled = true
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
                self.buttonSubmit?.isUserInteractionEnabled = true
                self.buttonBack?.isUserInteractionEnabled = true
            }
        }
    }
    
    func gotoNextForm() {
        
        //        if isNotFirstForm  {
        //            patient.selectedForms.removeFirst()
        //            if self.navigationController?.viewControllers.count > 2 {
        //                self.navigationController?.viewControllers.removeRange(2...(self.navigationController?.viewControllers.count)! - 2)
        //            }
        //        }
        
        if isFromPreviousForm && patient.selectedForms.count > 0{
//            patient.selectedForms.removeFirst()
            if self.navigationController?.viewControllers.count > 6 {
                self.navigationController?.viewControllers.removeSubrange(5...(self.navigationController?.viewControllers.count)! - 2)
            }
        }
        
        
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        //        if formNames.contains(kHealthHistory) {
        //            let healthHistoryVC = adultStoryBoard.instantiateViewControllerWithIdentifier("kPatientRegistrationStep1VCRef") as! PatientRegistrationStep1VC
        //            healthHistoryVC.patient = self.patient
        //            self.navigationController?.pushViewController(healthHistoryVC, animated: true)
        //        } else
        if formNames.contains(kNewPatientSignInForm){
            if patient.isPrivacySkipPressed{
                let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "PrivacePractice1VC") as! PrivacyPractice1ViewController
                new1VC.patient = self.patient
                self.navigationController?.pushViewController(new1VC, animated: true)
            }else{
                let healthHistoryVC = adultStoryBoard.instantiateViewController(withIdentifier: "kPatientRegistrationStep1VCRef") as! PatientRegistrationStep1VC
                healthHistoryVC.patient = self.patient
                self.navigationController?.pushViewController(healthHistoryVC, animated: true)
                
                //                let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("PrivacePracticeFormVC") as! PrivacyPracticeFormViewController
                //                new1VC.patient = self.patient
                //                self.navigationController?.pushViewController(new1VC, animated: true)
            }
        }else if formNames.contains(kFamilyMedicalHistory){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "FamilyMedicalHistoryVc") as! FamilyMedicalHistoryVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kHealthHistoryUpdate){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "HealthHistoryUpdateVc") as! HealthHistoryUpdateVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            //        }  else if formNames.contains(kNewPatientInformation){
            //                let new1VC = patientStoryBoard.instantiateViewControllerWithIdentifier("PatientInformation1VC") as! PatientInformation1ViewController
            //                new1VC.patient = self.patient
            //                self.navigationController?.pushViewController(new1VC, animated: true)
            //        }
            //        else if formNames.contains(kSmileAnalysis){
            //                let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("Smile1VC") as! SmileAnalysis1ViewController
            //                new1VC.patient = self.patient
            //                self.navigationController?.pushViewController(new1VC, animated: true)
            //
        }else if formNames.contains(kInsuranceCard) {
            let insuranceCard = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            insuranceCard.patient = self.patient
            self.navigationController?.pushViewController(insuranceCard, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let drivingLicense = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            drivingLicense.patient = self.patient
            drivingLicense.isDrivingLicense = true
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        }else if formNames.contains(kBoneSurgery){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "BoneSurgery1VC") as! BoneSurgey1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kTissueSurgery){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Tissue1VC") as! TissueGraft1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kTreatment){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Treatment1VC") as! Treatment1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kAgreement){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "ConsentInvisalignVC") as! ConsentForInvisalignPatientVC1
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kSinusSurgery){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Sinus1VC") as! SinusSurgery1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kAnesthesia){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Anesthesia1VC") as! Anesthesia1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else if formNames.contains(kCrownLengthening){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "CrownLengthiningVc") as! CrownLengthiningVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kPatientInformation){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "ImplantPatientInformationVc") as! ImplantPatientInformationVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kOcclusalEquilibration){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "EquilibrationVC") as! EquilibrationViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }else if formNames.contains(kNitrousOxide){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "NitroxideOxideConsentVc") as! NitroxideOxideConsentVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else if formNames.contains(kOralSedation){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Oral1VC") as! OralSedation1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(KPhotography){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Photography1VC") as! Photography1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kOrthodontic){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kReleaseEarlyOrthoVC") as! ReleaseEarlyOrthoVC
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(KXray){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kReleaseOfLiabilityVC") as! ReleaseOfLiabilityVC
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(KEndodonticTherapy){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "EndodonticTheraphyVc") as! EndodonticTheraphyVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kToothWhitening) {
            let toothwhiteVC = mainStoryBoard.instantiateViewController(withIdentifier: "kToothWhiteningStep1VC") as! ToothWhiteningStep1VC
            toothwhiteVC.patient = self.patient
            self.navigationController?.pushViewController(toothwhiteVC, animated: true)
        } else if formNames.contains(kDeclineTreatment){
            let new1VC = mainStoryBoard.instantiateViewController(withIdentifier: "DeclineOfTreatmentVc") as! DeclineOfTreatmentVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kHealthCare){
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "ReleaseHealthCareVc") as! ReleaseHealthCareVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}
