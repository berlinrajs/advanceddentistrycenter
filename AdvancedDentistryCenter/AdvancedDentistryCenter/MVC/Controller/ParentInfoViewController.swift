//
//  ParentInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ParentInfoViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldRelation.isEmpty {
            let alert = Extention.alert("PLEASE ENTER RELATIONSHIP")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.parentFirstName = textFieldFirstName.text
            patient.parentLastName = textFieldLastName.text
            patient.relation = textFieldRelation.text
            self.gotoNextForm()
        }
    }


}
