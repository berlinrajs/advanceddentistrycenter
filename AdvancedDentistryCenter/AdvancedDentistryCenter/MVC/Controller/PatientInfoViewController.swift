//
//  PatientInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
//    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var textFieldInitial: PDTextField!
    @IBOutlet weak var textFieldNickName: PDTextField!
    
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewDoctorList: PDTableView!
    @IBOutlet weak var labelDoctorName: UILabel!
    
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    @IBOutlet weak var pickerMonth : UIPickerView!
    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]


    
    override func viewDidLoad() {
        super.viewDidLoad()

       // datePicker.maximumDate = NSDate()
//        textFieldDateOfBirth.inputView = datePicker
//        textFieldDateOfBirth.inputAccessoryView = toolBar
        labelDate.text = patient.dateToday
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        textfieldMonth.inputView = pickerMonth
        textfieldMonth.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
        
        tableViewDoctorList.layer.cornerRadius = 4.0
        tableViewDoctorList.delegatePDTableView = self
        self.tableViewDoctorList.arrayValues = ["DR. HESSAM RAHIMI", "DR. NILOOFAR KHALESSEH", "DR. NEDA MODARESI"]
        self.tableViewDoctorList.reloadData()
    }
    
    @IBAction func buttonActionSelectDoctor(_ sender: AnyObject) {
        if constraintTableViewHeight.constant == 0 {
            self.view.layoutIfNeeded()
            constraintTableViewHeight.constant = 3 * (70.0 * screenSize.height/1024.0)
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        } else {
            self.view.layoutIfNeeded()
            constraintTableViewHeight.constant = 0
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.present(alert, animated: true, completion: nil)
        }else if textfieldDate.isEmpty || textfieldMonth.isEmpty || textfieldYear.isEmpty{
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.present(alert, animated: true, completion: nil)
        }else if invalidDateofBirth{
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
            self.present(alert, animated: true, completion: nil)
        } else {
           
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.initial = textFieldInitial.isEmpty ? "" : textFieldInitial.text
            patient.nickName = textFieldNickName.isEmpty ? "N/A" : textFieldNickName.text

            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            
           
            
//            let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//            if formNames.contains(kHealthHistory) {
//                let parentInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier( "kParentInfoVC") as! ParentInfoViewController
//                parentInfoVC.patient = patient
//                self.navigationController?.pushViewController(parentInfoVC, animated: true)
//                
//            } else {
            
                 self.gotoNextForm()
            
            
//            }
            

        }
    }
    
    
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }

    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        textfieldMonth.resignFirstResponder()
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textfieldMonth.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
    }

}

extension PatientInfoViewController : PDTableViewDelegate {
    func selectedValue(_ value: String, id: Int) {
        
        labelDoctorName.text = value.uppercased()
        self.view.layoutIfNeeded()
        constraintTableViewHeight.constant = 0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
}
extension PatientInfoViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldInitial {
            return textField.formatInitial(range, string: string)
        }else if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)

        }
        return true
}
}

extension PatientInfoViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        textfieldMonth.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
    }
}
