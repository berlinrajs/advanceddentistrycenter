//
//  PatientInformation4ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/31/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation4ViewController: PDViewController {

    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var textfieldDateOfBirth : UITextField!
    @IBOutlet weak var textfieldSocialSecurityNumber : UITextField!
    @IBOutlet weak var textfieldEmployerName : UITextField!
    @IBOutlet weak var radioPractice : RadioButton!
    var isSelfSelected : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForTextField(textfieldDateOfBirth)
        if isSelfSelected == true{
            textfieldName.text = patient.fullName
            textfieldRelationship.text = "PATIENT"
            textfieldDateOfBirth.text = patient.dateOfBirth
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
//        if textfieldName.isEmpty || textfieldRelationship.isEmpty || textfieldDateOfBirth.isEmpty || radioPractice.selectedButton == nil {
//            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }else 
        if !textfieldSocialSecurityNumber.isEmpty && textfieldSocialSecurityNumber.text!.characters.count  < 9{
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            patient.patientInformation.secondaryName = textfieldName.isEmpty ? "N/A" : textfieldName.text!
            patient.patientInformation.secondaryRelationship = textfieldRelationship.isEmpty ? "N/A" :  textfieldRelationship.text!
            patient.patientInformation.secondaryDateOfBirth = textfieldDateOfBirth.isEmpty ? "N/A" : textfieldDateOfBirth.text!
            patient.patientInformation.secondarySocialSecurityNumber = textfieldSocialSecurityNumber.isEmpty ? "N/A" : textfieldSocialSecurityNumber.text!
            patient.patientInformation.secondaryEmployerName  = textfieldEmployerName.isEmpty ? "N/A" : textfieldEmployerName.text!
            patient.patientInformation.secondaryPracticeTag = radioPractice.selected == nil ? 0 : radioPractice.selected.tag
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation5VC") as! PatientInformation5ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
        
    }
    
}

extension PatientInformation4ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldSocialSecurityNumber{
            return textField.formatNumbers(range, string: string, count: 9)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

