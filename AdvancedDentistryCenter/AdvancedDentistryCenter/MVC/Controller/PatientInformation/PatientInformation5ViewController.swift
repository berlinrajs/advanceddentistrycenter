//
//  PatientInformation5ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/31/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation5ViewController: PDViewController {

    @IBOutlet weak var radioSelf : RadioButton!
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldPhoneNumber : UITextField!
    @IBOutlet weak var textfieldDateOfBirth : UITextField!
    @IBOutlet weak var heightSelf : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)
        heightSelf.constant = patient.addressLine != nil ? 38 : 0
        DateInputView.addDatePickerForTextField(textfieldDateOfBirth)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if textfieldName.isEmpty || textfieldRelationship.isEmpty || textfieldDateOfBirth.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
            
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
            
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            patient.patientInformation.responsibleName = textfieldName.text!
            patient.patientInformation.responsibleRelationship = textfieldRelationship.text!
            patient.patientInformation.responsiblePhoneNUmber  = textfieldPhoneNumber.isEmpty ? "N/A" : textfieldPhoneNumber.text!
            patient.patientInformation.responsibleDateOfBirth = textfieldDateOfBirth.text!
            patient.patientInformation.responsibleAddress = textfieldAddress.text!
            patient.patientInformation.responsibleCity = textfieldCity.text!
            patient.patientInformation.responsibleState = textfieldState.text!
            patient.patientInformation.responsibleZipcode = textfieldZipcode.text!
            
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation6VC") as! PatientInformation6ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }

    @IBAction func radioSelfPressed (_ sender : RadioButton){
        if sender.tag == 1{
            textfieldName.text = patient.fullName
            textfieldRelationship.text = "PATIENT"
            textfieldDateOfBirth.text = patient.dateOfBirth
            textfieldPhoneNumber.text = patient.phoneNumber == "N/A" ? "" : patient.phoneNumber
            textfieldAddress.text = patient.addressLine
            textfieldCity.text = patient.city
            textfieldState.text = patient.state
            textfieldZipcode.text = patient.zipCode
        }else{
            textfieldAddress.text = ""
            textfieldCity.text = ""
            textfieldState.text = "MI"
            textfieldZipcode.text = ""
            textfieldName.text = ""
            textfieldRelationship.text = ""
            textfieldDateOfBirth.text = ""
            textfieldPhoneNumber.text = ""
        }
        
    }
    
    
    
}

extension PatientInformation5ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhoneNumber{
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

