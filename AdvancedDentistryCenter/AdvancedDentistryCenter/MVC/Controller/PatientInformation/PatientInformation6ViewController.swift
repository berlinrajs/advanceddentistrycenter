//
//  PatientInformation6ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation6ViewController: PDViewController {

    @IBOutlet weak var textfieldLicense : UITextField!
    @IBOutlet weak var textfieldEmployerName : UITextField!
    @IBOutlet weak var textfieldWorkPhone : UITextField!
    @IBOutlet weak var textfieldCardNUmber : UITextField!
    @IBOutlet weak var textfieldExpiry : UITextField!
    @IBOutlet weak var textfieldSSN : UITextField!
    @IBOutlet weak var dropDownPayment : BRDropDown!
    @IBOutlet weak var dropDownCard : BRDropDown!
    @IBOutlet weak var viewCard : UIView!
    var datePicker : DatePickerView!

    override func viewDidLoad() {
        super.viewDidLoad()

        dropDownPayment.items = ["Cash","Credit Card","Check"]
        dropDownPayment.delegate = self
        dropDownCard.items = ["Visa","MC","AMEX"]
        DateInputView.addExpiryDatePickerForTextField(textfieldExpiry)
        
        datePicker = DatePickerView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        let year = (gregorian as NSCalendar).component(.year, from: Date())
        datePicker.minYear = year
        datePicker.maxYear = year + 5
        datePicker.selectToday()
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        textfieldExpiry.inputView = datePicker
        textfieldExpiry.inputAccessoryView = toolbar

        // Do any additional setup after loading the view.
    }

    func donePressed() {
        textfieldExpiry.resignFirstResponder()
        textfieldExpiry.text = datePicker.date
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
        if !textfieldSSN.isEmpty && textfieldSSN.text!.characters.count != 9{
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)

        }else if dropDownPayment.selectedOption == nil{
            let alert = Extention.alert("PLEASE SELECT THE PAYMENT TYPE")
            self.present(alert, animated: true, completion: nil)

        }else if dropDownPayment.selectedOption == "Credit Card" {
            if dropDownCard.selectedOption == nil{
                let alert = Extention.alert("PLEASE SELECT THE CARD TYPE")
                self.present(alert, animated: true, completion: nil)

            }else if dropDownCard.selectedOption == "AMEX" && textfieldCardNUmber.text!.characters.count != 15{
                let alert = Extention.alert("PLEASE ENTER THE VALID CARD NUMBER")
                self.present(alert, animated: true, completion: nil)

            }else if dropDownCard.selectedOption == "AMEX" && textfieldExpiry.isEmpty{
                let alert = Extention.alert("PLEASE ENTER THE VALID EXP DATE")
                self.present(alert, animated: true, completion: nil)

            }else if dropDownCard.selectedOption != "AMEX"{
                if textfieldCardNUmber.text!.characters.count != 16{
                    let alert = Extention.alert("PLEASE ENTER THE VALID CARD NUMBER")
                    self.present(alert, animated: true, completion: nil)
                }else if textfieldExpiry.isEmpty{
                    let alert = Extention.alert("PLEASE ENTER THE VALID EXP DATE")
                    self.present(alert, animated: true, completion: nil)
                }else{
                    goToNextPage()
                }
            }else{
                goToNextPage()
            }
            
        }else{
            goToNextPage()
    }


}

func goToNextPage()  {
    patient.patientInformation.responsibleDriverLicense = textfieldLicense.isEmpty ? "N/A" : textfieldLicense.text!
    patient.patientInformation.responsibleEmployerName = textfieldEmployerName.isEmpty ? "N/A" : textfieldEmployerName.text!
    patient.patientInformation.responsibleWOrkPhone = textfieldWorkPhone.isEmpty ? "N/A" : textfieldWorkPhone.text!
    patient.patientInformation.responsiblePaymentType = dropDownPayment.selectedIndex
    patient.patientInformation.responsibleCardNumber = dropDownPayment.selectedIndex == 2 ? textfieldCardNUmber.text! : "N/A"
    patient.patientInformation.responsibleExpDate = dropDownPayment.selectedIndex == 2 ? textfieldExpiry.text! : "N/A"
    patient.patientInformation.responsibleSocialSecurityNumber = textfieldSSN.isEmpty ? "N/A" : textfieldSSN.text!
    if patient.dateOfBirth.is18YearsOld{
        let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation9VC") as! PatientInformation9ViewController
        new1VC.patient = self.patient
        self.navigationController?.pushViewController(new1VC, animated: true)
        
    }else{
        let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation7VC") as! PatientInformation7ViewController
        new1VC.patient = self.patient
        self.navigationController?.pushViewController(new1VC, animated: true)
    }
    }

}

extension PatientInformation6ViewController : BRDropDownDelegate{
    func didFinishSelectingValues(_ selectedOption: String) {
        if selectedOption == "Credit Card"{
            viewCard.isUserInteractionEnabled = true
            viewCard.alpha = 1.0
        }else{
            viewCard.isUserInteractionEnabled = false
            viewCard.alpha = 0.5
            textfieldExpiry.text = ""
            textfieldCardNUmber.text = ""
        }
        
    }
}

extension PatientInformation6ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldWorkPhone{
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textfieldCardNUmber {
            return textField.formatNumbers(range, string: string, count: 16)
//        } else if textField == textfieldExpiry {
//            return textField.formatNumbers(range, string: string, count: 4)
        }else if textField == textfieldSSN{
            return textField.formatNumbers(range, string: string, count: 9)
        }
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
