//
//  PatientInformation7ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation7ViewController: PDViewController {
    
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var radioPractice : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if textfieldName.isEmpty || textfieldRelationship.isEmpty || radioPractice.selected == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }else{
            patient.patientInformation.legalGuardianName = textfieldName.text!
            patient.patientInformation.legalGuardianRelationship = textfieldRelationship.text!
            patient.patientInformation.legalGuardianPractice = radioPractice.selected.tag
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation9VC") as! PatientInformation9ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)


        }
    }

}

extension PatientInformation7ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
