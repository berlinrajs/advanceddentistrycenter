//
//  PatientInformation8ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation8ViewController: PDViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labeldate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labeldate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !signaturePatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labeldate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            patient.patientInformation.signature = signaturePatient.signatureImage()
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation10VC") as! PatientInformation10ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
    }
}
