//
//  PatientInformation9ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformation9ViewController: PDViewController {
    
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldCellNumber : UITextField!
    @IBOutlet weak var textfieldHomeNumber : UITextField!
    @IBOutlet weak var textfieldWorkNumber : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !textfieldCellNumber.isEmpty && !textfieldCellNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)

        }else if !textfieldHomeNumber.isEmpty && !textfieldHomeNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)

        }else if !textfieldWorkNumber.isEmpty && !textfieldWorkNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID WORK NUMBER")
            self.present(alert, animated: true, completion: nil)
        }else{
            patient.patientInformation.emergencyName =  textfieldName.isEmpty ? "N/A" : textfieldName.text!
            patient.patientInformation.emergencyRelationship = textfieldRelationship.isEmpty ? "N/A" : textfieldRelationship.text!
            patient.patientInformation.emergencyCity = textfieldCity.isEmpty ? "N/A" : textfieldCity.text!
            patient.patientInformation.emergencyState = textfieldName.isEmpty ? "N/A" : textfieldState.text!
            patient.patientInformation.emergencyCellNumber = textfieldCellNumber.text!
            patient.patientInformation.emergencyHomeNumber = textfieldHomeNumber.isEmpty ? "N/A" : textfieldHomeNumber.text!
            patient.patientInformation.emergencyWorkNumber = textfieldWorkNumber.isEmpty ? "N/A" : textfieldWorkNumber.text!
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation8VC") as! PatientInformation8ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
        
    }

}

extension PatientInformation9ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldWorkNumber || textField == textfieldHomeNumber || textField == textfieldCellNumber{
            return textField.formatPhoneNumber(range, string: string)
        }
        
        return true
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
