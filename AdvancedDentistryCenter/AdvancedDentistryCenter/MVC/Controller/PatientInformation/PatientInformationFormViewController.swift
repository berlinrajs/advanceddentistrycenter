//
//  PatientInformationFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 9/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelPatientName1 : UILabel!

    @IBOutlet weak var labelPrimaryCompanyName : UILabel!
    @IBOutlet weak var labelPrimaryCompanyPhone : UILabel!
    @IBOutlet weak var labelPrimaryAddress : UILabel!
    @IBOutlet weak var labelPrimaryGroupNumber : UILabel!
    @IBOutlet weak var labelPrimaryInsuredId : UILabel!
    @IBOutlet weak var labelPrimaryInsuredName : UILabel!
    @IBOutlet weak var labelPrimaryRelationship : UILabel!
    @IBOutlet weak var labelPrimaryDateOfBirth : UILabel!
    @IBOutlet weak var labelPrimarySocialSecurityNumber : UILabel!
    @IBOutlet weak var labelPrimaryEmployerName : UILabel!
    @IBOutlet weak var radioPrimaryPractice : RadioButton!
    
    @IBOutlet weak var labelSecondaryCompanyName : UILabel!
    @IBOutlet weak var labelSecondaryCompanyPhone : UILabel!
    @IBOutlet weak var labelSecondaryAddress : UILabel!
    @IBOutlet weak var labelSecondaryGroupNumber : UILabel!
    @IBOutlet weak var labelSecondaryInsuredId : UILabel!
    @IBOutlet weak var labelSecondaryInsuredName : UILabel!
    @IBOutlet weak var labelSecondaryRelationship : UILabel!
    @IBOutlet weak var labelSecondaryDateOfBirth : UILabel!
    @IBOutlet weak var labelSecondarySocialSecurityNumber : UILabel!
    @IBOutlet weak var labelSecondaryEmployerName : UILabel!
    @IBOutlet weak var radioSecondaryPractice : RadioButton!

    @IBOutlet weak var labelResponsibleName : UILabel!
    @IBOutlet weak var labelResponsibleRelationship : UILabel!
    @IBOutlet weak var labelResponsibleSocialSecurityNumber : UILabel!
    @IBOutlet weak var labelResponsiblePhone : UILabel!
    @IBOutlet weak var labelDriverLicense : UILabel!
    @IBOutlet weak var labelResponsibleDateOfBirth : UILabel!
    @IBOutlet weak var labelResponsibleAddress : UILabel!
    @IBOutlet weak var labelResponsibleEmployer : UILabel!
    @IBOutlet weak var labelResponsibleWorkNumber : UILabel!
    @IBOutlet weak var radioPaymentMethod : RadioButton!
    @IBOutlet weak var labelCardNumber : UILabel!
    @IBOutlet weak var labelExpDate : UILabel!
    @IBOutlet weak var labelGuardianDetails : UILabel!
    @IBOutlet weak var radioResponsiblePractice : RadioButton!
    
    @IBOutlet weak var imageviewSignaturePatient : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    
    @IBOutlet weak var labelEmergencyName : UILabel!
    @IBOutlet weak var labelEmergencyRelationship : UILabel!
    @IBOutlet weak var labelEmergencyCity : UILabel!
    @IBOutlet weak var labelEmergencyState : UILabel!
    @IBOutlet weak var labelEmergencyCellPhone : UILabel!
    @IBOutlet weak var labelEmergencyHomePhone : UILabel!
    @IBOutlet weak var labelEmergencyWorkPhone : UILabel!
    
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var imageviewOfficeInitial : UIImageView!
    
    
    
    ///PRIVACY PRACTICE
    @IBOutlet weak var labelName1 : UILabel!
    @IBOutlet weak var labelName2 : UILabel!
    @IBOutlet weak var labelName3 : UILabel!
    @IBOutlet weak var labelDate6 : UILabel!
    @IBOutlet weak var labelDate5 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!
    @IBOutlet weak var labelOther : UILabel!
    @IBOutlet weak var signature1 : UIImageView!
    @IBOutlet weak var signature2 : UIImageView!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var radioOfficeUse : RadioButton!

    
    ///PATIENT REGISTRATION
    @IBOutlet var radioButtonStep3: [RadioButton]!
    @IBOutlet var radioButtonStep4: [RadioButton]!
    @IBOutlet var radioButtonStep6: [RadioButton]!
    @IBOutlet var radioButtonStep8A: [RadioButton]!
    @IBOutlet var radioButtonStep8B: [RadioButton]!
    @IBOutlet var radioButtonStep9: [RadioButton]!
    @IBOutlet var radioButtonMedical: [RadioButton]!
    @IBOutlet var radioButtonStep10: [RadioButton]!
    
    @IBOutlet weak var radioButtonWater: RadioButton!
    
    //STEP6
    @IBOutlet weak var labelCondition: UILabel!
    @IBOutlet weak var labelSeriousIllness: UILabel!
    @IBOutlet var labelSupplements: [UILabel]!
    
    //STEP9
    @IBOutlet var labelStep9: [UILabel]!
    
    //MEDICAL
    @IBOutlet var labelAnswer: [UILabel]!
    
    //STEP8A
    @IBOutlet weak var labelJointDate: UILabel!
    @IBOutlet weak var labelJointComplications: UILabel!
    @IBOutlet weak var labelDateTreatmentBegan: UILabel!
    @IBOutlet weak var labelAlcoholDrink24Hours: UILabel!
    @IBOutlet weak var labelDrinkInWeek: UILabel!
    @IBOutlet var radioButtonStopping: RadioButton!
    
    //STEP8B
    @IBOutlet weak var labelNumberOfWeeks: UILabel!
    
    
    //GENERAL
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelDateToday: UILabel!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelInitial: UILabel!
    @IBOutlet weak var labelhomePhone: UILabel!
    @IBOutlet weak var labelCellPhone: UILabel!
    @IBOutlet weak var labelMailingAddress: UILabel!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var labelZip: UILabel!
    @IBOutlet weak var labelOccupation: UILabel!
    @IBOutlet weak var labelheight: UILabel!
    @IBOutlet weak var labelWeight: UILabel!
    @IBOutlet weak var labelDateofBirth: UILabel!
    @IBOutlet weak var radioButtonGender: RadioButton!
    @IBOutlet weak var labelSSN: UILabel!
    @IBOutlet weak var labelEmergencyContact: UILabel!
    @IBOutlet weak var labelRelationship: UILabel!
    @IBOutlet weak var labelRelationHomePhone: UILabel!
    @IBOutlet weak var labelRelationCellPhone: UILabel!
    
    @IBOutlet weak var labelOtherPerson: UILabel!
    @IBOutlet weak var labelOtherPersonRelation: UILabel!
    
    //PHYSICIAN DETAILS
    @IBOutlet weak var labelPhysicianName: UILabel!
    @IBOutlet weak var labelPhysicianPhone: UILabel!
    @IBOutlet weak var labelPhysicianAddress: UILabel!
    @IBOutlet weak var labelPhysicianCityState: UILabel!
    @IBOutlet weak var labelPhysicianZip: UILabel!
    @IBOutlet weak var labelPhysicalExam: UILabel!
    
    //DENTALINFO
    @IBOutlet weak var labelReasonForVisitToday: UILabel!
    @IBOutlet weak var labelFeelAboutSmile: UILabel!
    @IBOutlet weak var labelLastDentalExam: UILabel!
    @IBOutlet weak var labelDoneAtThatTime: UILabel!
    @IBOutlet weak var labelDateOfLastXray: UILabel!
    
    //PREVIOUS DENTIST
    @IBOutlet weak var labelPreviousDentistName: UILabel!
    @IBOutlet weak var labelPreviousDentistPhone: UILabel!
    @IBOutlet weak var labelAboutDisease: UILabel!
    
    
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelDate7: UILabel!
    @IBOutlet weak var labelDate8: UILabel!

    ///SMILE ANALYSIS
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelPatientNumber : UILabel!
    @IBOutlet weak var radioSmile : RadioButton!
    @IBOutlet weak var radioTeeth : RadioButton!
    @IBOutlet var arraySmileButtons : [UIButton]!
    @IBOutlet weak var labelSmileOther : UILabel!
    @IBOutlet var arrayTeethButtons : [UIButton]!
    @IBOutlet weak var labelTeethOther : UILabel!
    @IBOutlet var arrayLifestyleButtons : [UIButton]!
    @IBOutlet weak var labelLifestyleOther : UILabel!
    @IBOutlet var arrayMakeoverButtons : [UIButton]!
    @IBOutlet weak var labelMakeoverOther : UILabel!
    @IBOutlet var arrayFamilyButtons : [UIButton]!
    @IBOutlet weak var labelFamilyOthers : UILabel!
    @IBOutlet var arrayAppointmentButtons : [UIButton]!
    @IBOutlet weak var labelAppointmentOther : UILabel!
    @IBOutlet  var labelUpcomingEvents : [UILabel]!
    @IBOutlet var arrayMusicButtons : [UIButton]!
    @IBOutlet weak var labelMusicOthers : UILabel!
    @IBOutlet  var labelHobbies : [UILabel]!
    @IBOutlet  var labelchildren : [UILabel]!
    @IBOutlet var labelKnowMore : [UILabel]!
    @IBOutlet weak var labelPatientNameSmile : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!

    

    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData () {
        labelPatientName.text = "Patient Name : " + patient.fullName
        labelPatientName1.text =  patient.fullName

        labelPrimaryCompanyName.text = patient.patientInformation.primaryCompanyName
        labelPrimaryCompanyPhone.text = patient.patientInformation.primaryCompanyNumber
        labelPrimaryAddress.text = patient.patientInformation.primaryAddress + ", " + patient.patientInformation.primaryCity + ", " + patient.patientInformation.primaryState + ", " +
            patient.patientInformation.primaryZipcode
        labelPrimaryGroupNumber.text = patient.patientInformation.primaryGroupNumber
        labelPrimaryInsuredId.text = patient.patientInformation.primaryInsuredId
        labelPrimaryInsuredName.text = patient.patientInformation.primaryName
        labelPrimaryRelationship.text = patient.patientInformation.primaryRelationship
        labelPrimaryDateOfBirth.text = patient.patientInformation.primaryDateOfBirth
        labelPrimarySocialSecurityNumber.text = patient.patientInformation.primarySocialSecurityNumber.socialSecurityNumber
        labelPrimaryEmployerName.text = patient.patientInformation.primaryEmployerName
        radioPrimaryPractice.setSelectedWithTag(patient.patientInformation.primaryPracticeTag)
        
        
        labelSecondaryCompanyName.text = patient.patientInformation.secondaryCompanyName
        labelSecondaryCompanyPhone.text = patient.patientInformation.secondaryCompanyNumber
        labelSecondaryAddress.text = patient.patientInformation.secondaryAddress + ", " + patient.patientInformation.secondaryCity + ", " + patient.patientInformation.secondaryState + ", " +
            patient.patientInformation.secondaryZipcode
        labelSecondaryGroupNumber.text =  patient.patientInformation.secondaryGroupNumber
        labelSecondaryInsuredId.text = patient.patientInformation.secondaryInsuredId
        labelSecondaryInsuredName.text = patient.patientInformation.secondaryName
        labelSecondaryRelationship.text = patient.patientInformation.secondaryRelationship
        labelSecondaryDateOfBirth.text = patient.patientInformation.secondaryDateOfBirth
        labelSecondarySocialSecurityNumber.text = patient.patientInformation.secondarySocialSecurityNumber.socialSecurityNumber
        labelSecondaryEmployerName.text = patient.patientInformation.secondaryEmployerName
        radioSecondaryPractice.setSelectedWithTag(patient.patientInformation.secondaryPracticeTag)
        
        
        labelResponsibleName.text = patient.patientInformation.responsibleName
        labelResponsibleRelationship.text = patient.patientInformation.responsibleRelationship
        labelResponsibleSocialSecurityNumber.text = patient.patientInformation.responsibleSocialSecurityNumber.socialSecurityNumber
        labelResponsiblePhone.text = patient.patientInformation.responsiblePhoneNUmber
        labelDriverLicense.text = patient.patientInformation.responsibleDriverLicense
        labelResponsibleDateOfBirth.text = patient.patientInformation.responsibleDateOfBirth
        labelResponsibleAddress.text = patient.patientInformation.responsibleAddress + ", " + patient.patientInformation.responsibleCity + ", " + patient.patientInformation.responsibleState + ", " +
            patient.patientInformation.responsibleZipcode
        labelResponsibleEmployer.text = patient.patientInformation.responsibleEmployerName
        labelResponsibleWorkNumber.text = patient.patientInformation.responsibleWOrkPhone
        radioPaymentMethod.setSelectedWithTag(patient.patientInformation.responsiblePaymentType)
        labelCardNumber.text = patient.patientInformation.responsibleCardNumber
        labelExpDate.text = patient.patientInformation.responsibleExpDate
        labelGuardianDetails.text = patient.patientInformation.legalGuardianName + ", " + patient.patientInformation.legalGuardianRelationship
        radioResponsiblePractice.setSelectedWithTag(patient.patientInformation.legalGuardianPractice)
        
        
        imageviewSignaturePatient.image = patient.patientInformation.signature
        labelDate1.text = patient.dateToday
        
        
        labelEmergencyName.text = patient.patientInformation.emergencyName
        labelEmergencyRelationship.text = patient.patientInformation.emergencyRelationship
        labelEmergencyCity.text = patient.patientInformation.emergencyCity
        labelEmergencyState.text = patient.patientInformation.emergencyState
        labelEmergencyCellPhone.text = patient.patientInformation.emergencyCellNumber
        labelEmergencyHomePhone.text = patient.patientInformation.emergencyHomeNumber
        labelEmergencyWorkPhone.text = patient.patientInformation.emergencyWorkNumber
            
            
        labelDate2.text = patient.dateToday
        imageviewOfficeInitial.image = patient.patientInformation.signatureOfficial
        
        
        
        ///PRIVACY PRACTICE
        if patient.isPrivacySkipPressed{
            labelName1.text = patient.fullName
            labelName2.text = patient.fullName
            labelName3.text = patient.fullName
            labelDate4.text = patient.dateToday
            labelDate5.text = patient.dateToday
            labelDate3.text = patient.dateToday
            signature1.image = patient.patientInformation.signature
            signature2.image = patient.patientInformation.signature
            
        }else{
            signatureOffice.image = patient.privacySignatureOffice
            radioOfficeUse.setSelectedWithTag(patient.privacyPracticeTag)
            labelOther.text = patient.privacyOthers
            labelDate6.text = patient.dateToday
            
            
        }
        
        ///PATIENT REGISTRATION
        labelEmail.text = patient.email
        labelDateToday.text = patient.dateToday
        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.initial
        labelhomePhone.text = patient.phoneNumber
        labelCellPhone.text = patient.adultRegistration.cellPhone
        labelMailingAddress.text = patient.addressLine
        labelCity.text = patient.city
        labelState.text = patient.state
        labelZip.text = patient.zipCode
        labelOccupation.text = patient.adultRegistration.occupation
        labelheight.text = patient.adultRegistration.height
        labelWeight.text = patient.adultRegistration.weight
        labelDateofBirth.text = patient.dateOfBirth
        radioButtonGender.setSelectedWithTag(patient.gender)
        labelSSN.text = patient.socialSecurityNumber
        
        labelEmergencyContact.text = patient.patientInformation.emergencyName//patient.emergencyContactName
        labelRelationship.text = patient.patientInformation.emergencyRelationship//patient.relation
        labelRelationHomePhone.text = patient.patientInformation.emergencyHomeNumber//patient.emergencyContactPhoneNumber
        labelRelationCellPhone.text = patient.patientInformation.emergencyCellNumber//patient.adultRegistration.emergencyCellPhone
        
        labelOtherPerson.text = patient.adultRegistration.responsiblePerson
        labelOtherPersonRelation.text = patient.adultRegistration.responsiblePersonRelation
        
        self.setValues(patient.adultRegistration.patientRegistrationStep3Questions, buttons: radioButtonStep3, index: 0)
        if checkSelectedYes() == nil {
            self.setValues(patient.adultRegistration.patientRegistrationStep4Questions, buttons: radioButtonStep4, index: 1)
            self.setValues(patient.adultRegistration.patientRegistrationStep6Questions, buttons: radioButtonStep6, index: 2)
            self.setValues(patient.adultRegistration.patientRegistrationStep8Questions[0], buttons: radioButtonStep8A, index: 3)
            self.setValues(patient.adultRegistration.patientRegistrationStep8Questions[1], buttons: radioButtonStep8B, index: 4)
            self.setValues(patient.adultRegistration.patientRegistrationStep9Questions, buttons: radioButtonStep9, index: 5)
            self.setValues(patient.adultRegistration.patientRegistrationMedicalQuestions, buttons: radioButtonMedical, index: 6)
            self.setValues(patient.adultRegistration.patientRegistrationStep10Questions, buttons: radioButtonStep10, index: 7)
            
            if patient.adultRegistration.patientRegistrationStep6Questions[0].selectedOption != nil && patient.adultRegistration.patientRegistrationStep6Questions[0].selectedOption == true {
                labelPhysicianName.text = patient.adultRegistration.physician.name
                labelPhysicianPhone.text = patient.adultRegistration.physician.phoneNumber
                labelPhysicianAddress.text = patient.adultRegistration.physician.address
                labelPhysicianCityState.text = "\(patient.adultRegistration.physician.city), \(patient.adultRegistration.physician.state)"
                labelPhysicianZip.text = patient.adultRegistration.physician.zip
            }
            
            labelPhysicalExam.text = patient.adultRegistration.lastPhysicalExam
            labelReasonForVisitToday.text = patient.adultRegistration.reasonForTodayVisit
            labelFeelAboutSmile.text = patient.adultRegistration.feelAboutSmile
            labelLastDentalExam.text = patient.adultRegistration.lastDentalExam
            labelDoneAtThatTime.text = patient.adultRegistration.doneOnLastDentalExam
            labelDateOfLastXray.text = patient.adultRegistration.lastXrayDate
            imageViewSignature1.image = patient.patientInformation.signature//patient.signature1
            imageViewSignature2.image = patient.patientInformation.signatureOfficial//patient.signature2
            labelDate7.text = patient.dateToday
            labelDate8.text = patient.dateToday
            
            labelNumberOfWeeks.text = patient.adultRegistration.patientRegistrationStep8Questions[1][0].answer
        }

        //SMILE ANALYSIS
        labelDate.text = patient.dateToday
        labelPatientNumber.text = patient.patientNumber
        radioSmile.setSelectedWithTag(patient.radioSmileTag)
        radioTeeth.setSelectedWithTag(patient.radioTeethTag)
        for btn in arraySmileButtons{
            btn.isSelected = patient.arraySmileTags.contains(btn.tag)
        }
        labelSmileOther.text = patient.smileOther
        for btn in arrayTeethButtons{
            btn.isSelected = patient.arrayTeethTags.contains(btn.tag)
        }
        labelTeethOther.text = patient.teethOther
        for btn in arrayLifestyleButtons{
            btn.isSelected = patient.arrayLifestyleTag.contains(btn.tag)
        }
        labelLifestyleOther.text = patient.lifestyleOther
        for btn in arrayMakeoverButtons{
            btn.isSelected = patient.arrayMakeOverTag.contains(btn.tag)
        }
        labelMakeoverOther.text = patient.makeOverOther
        for btn in arrayFamilyButtons{
            btn.isSelected = patient.arrayFamilyTag.contains(btn.tag)
        }
        labelFamilyOthers.text = patient.familyOther
        for btn in arrayAppointmentButtons{
            btn.isSelected = patient.arrayAppointmentTags.contains(btn.tag)
        }
        labelAppointmentOther.text = patient.appointmentOther
        patient.upcomingEvents.setTextForArrayOfLabels(labelUpcomingEvents)
        for btn in arrayMusicButtons{
            btn.isSelected = patient.arrayMusicTags.contains(btn.tag)
        }
        labelMusicOthers.text = patient.musicOther
        patient.hobbies.setTextForArrayOfLabels(labelHobbies)
        patient.childrens.setTextForArrayOfLabels(labelchildren)
        patient.knowMore.setTextForArrayOfLabels(labelKnowMore)
        labelPatientNameSmile.text = patient.fullName
        signaturePatient.image = patient.patientInformation.signature

        labelPreviousDentistName.text = patient.adultRegistration.previousDentist
        labelPreviousDentistPhone.text = patient.adultRegistration.previousDentistPhone


    }
    
    func checkSelectedYes() -> PDQuestion? {
        for question in patient.adultRegistration.patientRegistrationStep3Questions {
            if question.selectedOption != nil && question.selectedOption == true {
                return question
            }
        }
        return nil
    }
    
    func setValues(_ questions: [PDQuestion], buttons: [RadioButton], index: Int) {
        for (idx, button) in buttons.enumerated() {
            let question = questions[idx]
            button.setSelectedWithTag(question.selectedOption == nil ? 3 : question.selectedOption == false ? 2 : 1)
            if question.isAnswerRequired == true && (question.selectedOption != nil && question.selectedOption == true) {
                switch index {
                case 1:
                    for (idx, option) in question.options!.enumerated() {
                        if option.isSelected != nil && option.isSelected == true {
                            radioButtonWater.setSelectedWithTag(idx + 1)
                        }
                    }
                    break
                case 2:
                    if idx == 2 {
                        labelCondition.text = question.answer
                    } else if idx == 3 {
                        labelSeriousIllness.text = question.answer
                    } else {
                        question.answer?.setTextForArrayOfLabels(labelSupplements)
                    }
                    break
                case 3:
                    if idx == 1 {
                        let text = question.answer?.components(separatedBy: "kSecondValue")
                        labelJointDate.text = text?.first
                        labelJointComplications.text = text?.last
                    } else if idx == 3 {
                        labelDateTreatmentBegan.text = question.answer
                    } else if idx == 5 {
                        for (idx, option) in question.options!.enumerated() {
                            if option.isSelected != nil && option.isSelected == true {
                                radioButtonStopping.setSelectedWithTag(idx + 1)
                            }
                        }
                    } else {
                        let text = question.answer?.components(separatedBy: "kSecondValue")
                        labelAlcoholDrink24Hours.text = text?.first
                        labelDrinkInWeek.text = text?.last
                    }
                    break
                case 4:
                    labelNumberOfWeeks.text = question.answer
                    break
                case 5:
                    labelStep9[idx].text = question.answer
                    break
                case 6:
                    if idx == 16 {
                        labelAnswer[0].text = question.answer
                    } else if idx == 43 {
                        labelAnswer[1].text = question.answer
                    } else if idx == 46 {
                        labelAnswer[2].text = question.answer
                    } else {
                        labelAnswer[3].text = question.answer
                    }
                    break
                case 7:
                    if idx == 7 {
                        labelPreviousDentistName.text = patient.adultRegistration.previousDentist
                        labelPreviousDentistPhone.text = patient.adultRegistration.previousDentistPhone
                    } else {
                        labelAboutDisease.text = question.answer
                    }
                    break
                default:
                    break
                }
            }
        }
    }

}
