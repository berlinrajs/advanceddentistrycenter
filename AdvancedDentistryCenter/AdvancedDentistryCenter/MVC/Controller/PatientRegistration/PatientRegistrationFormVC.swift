//
//  PatientRegistrationFormVC.swift
//  FusionDental
//
//  Created by Leojin Bose on 6/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationFormVC: PDViewController {

    @IBOutlet var radioButtonStep3: [RadioButton]!
    @IBOutlet var radioButtonStep4: [RadioButton]!
    @IBOutlet var radioButtonStep6: [RadioButton]!
    @IBOutlet var radioButtonStep8A: [RadioButton]!
    @IBOutlet var radioButtonStep8B: [RadioButton]!
    @IBOutlet var radioButtonStep9: [RadioButton]!
    @IBOutlet var radioButtonMedical: [RadioButton]!
    @IBOutlet var radioButtonStep10: [RadioButton]!

    @IBOutlet weak var radioButtonWater: RadioButton!
    
    //STEP6
    @IBOutlet weak var labelCondition: UILabel!
    @IBOutlet weak var labelSeriousIllness: UILabel!
    @IBOutlet var labelSupplements: [UILabel]!
    
    //STEP9
    @IBOutlet var labelStep9: [UILabel]!
    
    //MEDICAL
    @IBOutlet var labelAnswer: [UILabel]!
    
    //STEP8A
    @IBOutlet weak var labelJointDate: UILabel!
    @IBOutlet weak var labelJointComplications: UILabel!
    @IBOutlet weak var labelDateTreatmentBegan: UILabel!
    @IBOutlet weak var labelAlcoholDrink24Hours: UILabel!
    @IBOutlet weak var labelDrinkInWeek: UILabel!
    @IBOutlet var radioButtonStopping: RadioButton!
    
    //STEP8B
    @IBOutlet weak var labelNumberOfWeeks: UILabel!
    
    
    //GENERAL
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelDateToday: UILabel!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelInitial: UILabel!
    @IBOutlet weak var labelhomePhone: UILabel!
    @IBOutlet weak var labelCellPhone: UILabel!
    @IBOutlet weak var labelMailingAddress: UILabel!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var labelZip: UILabel!
    @IBOutlet weak var labelOccupation: UILabel!
    @IBOutlet weak var labelheight: UILabel!
    @IBOutlet weak var labelWeight: UILabel!
    @IBOutlet weak var labelDateofBirth: UILabel!
    @IBOutlet weak var radioButtonGender: RadioButton!
    @IBOutlet weak var labelSSN: UILabel!
    @IBOutlet weak var labelEmergencyContact: UILabel!
    @IBOutlet weak var labelRelationship: UILabel!
    @IBOutlet weak var labelRelationHomePhone: UILabel!
    @IBOutlet weak var labelRelationCellPhone: UILabel!
    
    @IBOutlet weak var labelOtherPerson: UILabel!
    @IBOutlet weak var labelOtherPersonRelation: UILabel!
    
    //PHYSICIAN DETAILS
    @IBOutlet weak var labelPhysicianName: UILabel!
    @IBOutlet weak var labelPhysicianPhone: UILabel!
    @IBOutlet weak var labelPhysicianAddress: UILabel!
    @IBOutlet weak var labelPhysicianCityState: UILabel!
    @IBOutlet weak var labelPhysicianZip: UILabel!
    @IBOutlet weak var labelPhysicalExam: UILabel!
    
    //DENTALINFO
    @IBOutlet weak var labelReasonForVisitToday: UILabel!
    @IBOutlet weak var labelFeelAboutSmile: UILabel!
    @IBOutlet weak var labelLastDentalExam: UILabel!
    @IBOutlet weak var labelDoneAtThatTime: UILabel!
    @IBOutlet weak var labelDateOfLastXray: UILabel!
    
    //PREVIOUS DENTIST
    @IBOutlet weak var labelPreviousDentistName: UILabel!
    @IBOutlet weak var labelPreviousDentistPhone: UILabel!
    @IBOutlet weak var labelAboutDisease: UILabel!
    
    
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    
    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func loadView() {
        super.loadView()
        
        labelEmail.text = patient.email
        labelDateToday.text = patient.dateToday
        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.initial
        labelhomePhone.text = patient.phoneNumber
        labelCellPhone.text = patient.adultRegistration.cellPhone
        labelMailingAddress.text = patient.addressLine
        labelCity.text = patient.city
        labelState.text = patient.state
        labelZip.text = patient.zipCode
        labelOccupation.text = patient.adultRegistration.occupation
        labelheight.text = patient.adultRegistration.height
        labelWeight.text = patient.adultRegistration.weight
        labelDateofBirth.text = patient.dateOfBirth
        radioButtonGender.setSelectedWithTag(patient.gender)
        labelSSN.text = patient.socialSecurityNumber
        labelEmergencyContact.text = patient.emergencyContactName
        labelRelationship.text = patient.relation
        labelRelationHomePhone.text = patient.emergencyContactPhoneNumber
        labelRelationCellPhone.text = patient.adultRegistration.emergencyCellPhone
        labelOtherPerson.text = patient.adultRegistration.responsiblePerson
        labelOtherPersonRelation.text = patient.adultRegistration.responsiblePersonRelation
        
        self.setValues(patient.adultRegistration.patientRegistrationStep3Questions, buttons: radioButtonStep3, index: 0)
        if checkSelectedYes() == nil {
            self.setValues(patient.adultRegistration.patientRegistrationStep4Questions, buttons: radioButtonStep4, index: 1)
            self.setValues(patient.adultRegistration.patientRegistrationStep6Questions, buttons: radioButtonStep6, index: 2)
            self.setValues(patient.adultRegistration.patientRegistrationStep8Questions[0], buttons: radioButtonStep8A, index: 3)
           self.setValues(patient.adultRegistration.patientRegistrationStep8Questions[1], buttons: radioButtonStep8B, index: 4)
            self.setValues(patient.adultRegistration.patientRegistrationStep9Questions, buttons: radioButtonStep9, index: 5)
            self.setValues(patient.adultRegistration.patientRegistrationMedicalQuestions, buttons: radioButtonMedical, index: 6)
            self.setValues(patient.adultRegistration.patientRegistrationStep10Questions, buttons: radioButtonStep10, index: 7)
            
            if patient.adultRegistration.patientRegistrationStep6Questions[0].selectedOption != nil && patient.adultRegistration.patientRegistrationStep6Questions[0].selectedOption == true {
                labelPhysicianName.text = patient.adultRegistration.physician.name
                labelPhysicianPhone.text = patient.adultRegistration.physician.phoneNumber
                labelPhysicianAddress.text = patient.adultRegistration.physician.address
                labelPhysicianCityState.text = "\(patient.adultRegistration.physician.city), \(patient.adultRegistration.physician.state)"
                labelPhysicianZip.text = patient.adultRegistration.physician.zip
            }
            
            labelPhysicalExam.text = patient.adultRegistration.lastPhysicalExam
            labelReasonForVisitToday.text = patient.adultRegistration.reasonForTodayVisit
            labelFeelAboutSmile.text = patient.adultRegistration.feelAboutSmile
            labelLastDentalExam.text = patient.adultRegistration.lastDentalExam
            labelDoneAtThatTime.text = patient.adultRegistration.doneOnLastDentalExam
            labelDateOfLastXray.text = patient.adultRegistration.lastXrayDate
            imageViewSignature1.image = patient.signature1
            imageViewSignature2.image = patient.signature2
            labelDate1.text = patient.dateToday
            labelDate2.text = patient.dateToday
            
            labelNumberOfWeeks.text = patient.adultRegistration.patientRegistrationStep8Questions[1][0].answer
        }
    }
    
    func checkSelectedYes() -> PDQuestion? {
        for question in patient.adultRegistration.patientRegistrationStep3Questions {
            if question.selectedOption != nil && question.selectedOption == true {
                return question
            }
        }
        return nil
    }
    
    func setValues(_ questions: [PDQuestion], buttons: [RadioButton], index: Int) {
        for (idx, button) in buttons.enumerated() {
            let question = questions[idx]
            button.setSelectedWithTag(question.selectedOption == nil ? 3 : question.selectedOption == false ? 2 : 1)
            if question.isAnswerRequired == true && (question.selectedOption != nil && question.selectedOption == true) {
                switch index {
                case 1:
                    for (idx, option) in question.options!.enumerated() {
                        if option.isSelected != nil && option.isSelected == true {
                            radioButtonWater.setSelectedWithTag(idx + 1)
                        }
                    }
                    break
                case 2:
                    if idx == 2 {
                        labelCondition.text = question.answer
                    } else if idx == 3 {
                        labelSeriousIllness.text = question.answer
                    } else {
                        question.answer?.setTextForArrayOfLabels(labelSupplements)
                    }
                    break
                case 3:
                    if idx == 1 {
                        let text = question.answer?.components(separatedBy: "kSecondValue")
                        labelJointDate.text = text?.first
                        labelJointComplications.text = text?.last
                    } else if idx == 3 {
                        labelDateTreatmentBegan.text = question.answer
                    } else if idx == 5 {
                        for (idx, option) in question.options!.enumerated() {
                            if option.isSelected != nil && option.isSelected == true {
                                radioButtonStopping.setSelectedWithTag(idx + 1)
                            }
                        }
                    } else {
                        let text = question.answer?.components(separatedBy: "kSecondValue")
                        labelAlcoholDrink24Hours.text = text?.first
                        labelDrinkInWeek.text = text?.last
                    }
                    break
                case 4:
                    labelNumberOfWeeks.text = question.answer
                    break
                case 5:
                    labelStep9[idx].text = question.answer
                    break
                case 6:
                    if idx == 16 {
                        labelAnswer[0].text = question.answer
                    } else if idx == 43 {
                        labelAnswer[1].text = question.answer
                    } else if idx == 46 {
                        labelAnswer[2].text = question.answer
                    } else {
                        labelAnswer[3].text = question.answer
                    }
                    break
                case 7:
                    if idx == 7 {
                        labelPreviousDentistName.text = patient.adultRegistration.previousDentist
                        labelPreviousDentistPhone.text = patient.adultRegistration.previousDentistPhone
                    } else {
                        labelAboutDisease.text = question.answer
                    }
                    break
                default:
                    break
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
