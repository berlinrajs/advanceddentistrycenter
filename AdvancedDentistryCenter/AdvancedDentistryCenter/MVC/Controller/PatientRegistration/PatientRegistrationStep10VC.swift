//
//  PatientRegistrationStep10VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 6/9/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep10VC: PDViewController {

    @IBOutlet weak var tableViewQuestions1: YNTableView!
    @IBOutlet weak var tableViewQuestions2: YNTableView!
    @IBOutlet weak var tableViewQuestions3: YNTableView!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!

    var isSpanish: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        patient.adultRegistration.patientRegistrationStep10Questions = PDQuestion.patientRegistrationStep10Questions(isSpanish)
        self.tableViewQuestions1.arrayOfQuestions = [PDQuestion](patient.adultRegistration.patientRegistrationStep10Questions[0...2])
        self.tableViewQuestions2.arrayOfQuestions = [PDQuestion](patient.adultRegistration.patientRegistrationStep10Questions[3...5])
        
        self.tableViewQuestions3.delegateYNTableView = self
        self.tableViewQuestions3.arrayOfQuestions = [PDQuestion](patient.adultRegistration.patientRegistrationStep10Questions[6...7])

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        func gotoNextPage() {
            if !buttonVerified.isSelected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.present(alert, animated: true, completion: nil)
            } else {
//                patient.adultRegistration.patientRegistrationStep10Questions = [self.tableViewQuestions1.arrayOfQuestions!, self.tableViewQuestions2.arrayOfQuestions!, self.tableViewQuestions3.arrayOfQuestions!]

                let patientRegistrationStep11 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep11SpanishVC" : "kPatientRegistrationStep11VCRef") as! PatientRegistrationStep11VC
                patientRegistrationStep11.patient = patient
                patientRegistrationStep11.isSpanish = self.isSpanish
                self.navigationController?.pushViewController(patientRegistrationStep11, animated: true)
            }
        }
        
        let question = self.tableViewQuestions3.arrayOfQuestions![1]
        if question.selectedOption != nil && question.selectedOption == true {
            if textFieldName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER PHYSICIAN OR DENTIST NAME")
                self.present(alert, animated: true, completion: nil)
            } else if !textFieldPhone.text!.isPhoneNumber {
                let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
                self.present(alert, animated: true, completion: nil)
            } else {
                patient.adultRegistration.previousDentist = textFieldName.text
                patient.adultRegistration.previousDentistPhone = textFieldPhone.text
                gotoNextPage()
            }
        } else {
            gotoNextPage()
        }
        
        
    }
    
}

extension PatientRegistrationStep10VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.formatPhoneNumber(range, string: string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension PatientRegistrationStep10VC: YNQuestionsTableViewCellDelegate {
    func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell) {
        if cell.tag == 0 {
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE EXPLAIN") { (textView, isEdited) in
                if isEdited {
                    cell.question.selectedOption = true
                    cell.question.answer = textView.text
                } else {
                    cell.question.selectedOption = false
                    self.tableViewQuestions3.reloadData()
                }
            }
        } else {
            let question = self.tableViewQuestions3.arrayOfQuestions![1]
            question.selectedOption = true
            textFieldName.isEnabled = true
            textFieldPhone.isEnabled = true
        }
    }
    
    func questionNoClicked(forCell cell: YNQuestionsTableViewCell) {
        textFieldName.isEnabled = false
        textFieldPhone.isEnabled = false
        textFieldName.text = ""
        textFieldPhone.text = ""
    }
}
