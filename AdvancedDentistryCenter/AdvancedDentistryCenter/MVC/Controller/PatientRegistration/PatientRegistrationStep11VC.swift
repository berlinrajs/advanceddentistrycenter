//
//  PatientRegistrationStep11VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 6/9/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep11VC: PDViewController {
//    @IBOutlet weak var dateLabel1: DateLabel!
//    @IBOutlet weak var dateLabel2: DateLabel!
//    @IBOutlet weak var signatureView1: SignatureView!
//    @IBOutlet weak var signatureView2: SignatureView!
//    @IBOutlet weak var labelPatientName : UILabel!
    
    var isSpanish: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        dateLabel1.todayDate = patient.dateToday
//        dateLabel2.todayDate = patient.dateToday
//        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
//        if !signatureView1.isSigned() || !signatureView2.isSigned() {
//            let alert = Extention.alert("PLEASE SIGN THE FORM")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else if !dateLabel1.dateTapped || !dateLabel2.dateTapped {
//            let alert = Extention.alert("PLEASE SELECT DATE")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else {
//            patient.signature1 = signatureView1.signatureImage()
//            patient.signature2 = signatureView2.signatureImage()
            //            let patientRegistrationForm = self.storyboard?.instantiateViewControllerWithIdentifier(isSpanish ? "kPatientRegistrationFormSpanishVC" : "kPatientRegistrationFormVCRef") as! PatientRegistrationFormVC
            //            patientRegistrationForm.patient = patient
            //            patientRegistrationForm.isSpanish = self.isSpanish
            //            self.navigationController?.pushViewController(patientRegistrationForm, animated: true)
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Smile1VC") as! SmileAnalysis1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
//        }
        
    }
    
}
