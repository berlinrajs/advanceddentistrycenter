//
//  PatientRegistrationStep1VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep1VC: PDViewController {

    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldHomePhone: PDTextField!
    @IBOutlet weak var textFieldCellPhone: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZip: PDTextField!
    @IBOutlet weak var textFieldOccupation: PDTextField!
    @IBOutlet weak var textFieldHeight: PDTextField!
    @IBOutlet weak var textFieldWeight: PDTextField!
    @IBOutlet weak var textFieldSocialSecurity: PDTextField!
    @IBOutlet weak var radioButtonGender: RadioButton!
    
    var isSpanish: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER VALID EMAIL")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldHomePhone.isEmpty && !textFieldHomePhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID BUSINESS/CELL PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        } else if radioButtonGender.selected == nil {
            let alert = Extention.alert("PLEASE SELECT GENDER")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.adultRegistration = AdultRegistration()
            patient.phoneNumber = textFieldHomePhone.isEmpty ? "N/A" : textFieldHomePhone.text
            patient.addressLine = textFieldAddress.text
            patient.city = textFieldCity.text
            patient.state = textFieldState.text
            patient.zipCode = textFieldZip.text
            patient.gender = radioButtonGender.selected.tag
            patient.email = textFieldEmail.isEmpty ? "N/A" : textFieldEmail.text
            patient.socialSecurityNumber = textFieldSocialSecurity.isEmpty ? "N/A" : textFieldSocialSecurity.text

            patient.adultRegistration.cellPhone = textFieldCellPhone.isEmpty ? "N/A" : textFieldCellPhone.text
            patient.adultRegistration.occupation = textFieldOccupation.isEmpty ? "N/A" : textFieldOccupation.text
            patient.adultRegistration.height = textFieldHeight.isEmpty ? "N/A" : textFieldHeight.text
            patient.adultRegistration.weight = textFieldWeight.isEmpty ? "N/A" : textFieldWeight.text

            let patientRegistrationStep2 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep2SpanishVC" : "kPatientRegistrationStep2VCRef") as! PatientRegistrationStep2VC
            patientRegistrationStep2.patient = patient
            patientRegistrationStep2.isSpanish = self.isSpanish
            self.navigationController?.pushViewController(patientRegistrationStep2, animated: true)
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [ textFieldAddress, textFieldCity, textFieldState, textFieldZip]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }

}


extension PatientRegistrationStep1VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldHomePhone || textField == textFieldCellPhone {
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textFieldHeight || textField == textFieldWeight {
            return textField.formatNumbers(range, string: string, count: 3)
        } else {
            return textField.formatZipCode(range, string: string)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
