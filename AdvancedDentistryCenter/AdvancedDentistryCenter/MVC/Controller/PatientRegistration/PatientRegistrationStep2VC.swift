//
//  PatientRegistrationStep2VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep2VC: PDViewController {

//    @IBOutlet weak var textFieldEmergencyContactName: PDTextField!
//    @IBOutlet weak var textFieldRelationship: PDTextField!
//    @IBOutlet weak var textFieldHomePhone: PDTextField!
//    @IBOutlet weak var textFieldCellPhone: PDTextField!
    
    @IBOutlet weak var textFieldResponsiblePerson: PDTextField!
    @IBOutlet weak var textFieldResponsibleRelationship: PDTextField!
    
    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        
//        if let _ = findEmptyTextField() {
//            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else if !textFieldHomePhone.isEmpty && !textFieldHomePhone.text!.isPhoneNumber {
//            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
//            let alert = Extention.alert("PLEASE ENTER VALID CELL PHONE")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else {
            patient.emergencyContactName = ""//textFieldEmergencyContactName.text
            patient.emergencyContactPhoneNumber = ""//textFieldHomePhone.isEmpty ? "N/A" : textFieldHomePhone.text
            patient.relation = ""//textFieldRelationship.isEmpty ? "N/A" : textFieldRelationship.text
            
            patient.adultRegistration.emergencyCellPhone = ""//textFieldCellPhone.isEmpty ? "N/A" : textFieldCellPhone.text
            patient.adultRegistration.responsiblePerson = textFieldResponsiblePerson.isEmpty ? "N/A" : textFieldResponsiblePerson.text
            patient.adultRegistration.responsiblePersonRelation = textFieldResponsibleRelationship.isEmpty ? "N/A" : textFieldResponsibleRelationship.text

            let patientRegistrationStep3 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep3SpanishVC" : "kPatientRegistrationStep3VCRef") as! PatientRegistrationStep3VC
            patientRegistrationStep3.patient = patient
            patientRegistrationStep3.isSpanish = self.isSpanish
            self.navigationController?.pushViewController(patientRegistrationStep3, animated: true)
//        }
    }
    
//    func findEmptyTextField() -> UITextField? {
//        let textFields = [textFieldEmergencyContactName]
//        for textField in textFields {
//            if textField.isEmpty {
//                return textField
//            }
//        }
//        return nil
//    }
}

extension PatientRegistrationStep2VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.formatPhoneNumber(range, string: string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
