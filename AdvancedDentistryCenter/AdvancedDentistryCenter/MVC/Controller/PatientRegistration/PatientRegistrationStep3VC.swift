//
//  PatientRegistrationStep3VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep3VC: PDViewController {
    

    @IBOutlet weak var tableViewQuestions: YNTableView!
    @IBOutlet weak var buttonVerified: UIButton!

    var isSpanish: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewQuestions.delegateYNTableView = self
        self.tableViewQuestions.arrayOfQuestions = PDQuestion.patientRegistrationStep3Questions(isSpanish)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.adultRegistration.patientRegistrationStep3Questions = self.tableViewQuestions.arrayOfQuestions!
            if checkSelectedYes() != nil {
//                let patientRegistrationForm = self.storyboard?.instantiateViewControllerWithIdentifier(isSpanish ? "kPatientRegistrationFormSpanishVC" : "kPatientRegistrationFormVCRef") as! PatientRegistrationFormVC
//                patientRegistrationForm.patient = patient
//                patientRegistrationForm.isSpanish = self.isSpanish
//                self.navigationController?.pushViewController(patientRegistrationForm, animated: true)
                let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Smile1VC") as! SmileAnalysis1ViewController
                new1VC.patient = self.patient
                self.navigationController?.pushViewController(new1VC, animated: true)
            } else {
                let patientRegistrationStep4 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep4SpanishVC" : "kPatientRegistrationStep4VCRef") as! PatientRegistrationStep4VC
                patientRegistrationStep4.patient = patient
                patientRegistrationStep4.isSpanish = self.isSpanish
                self.navigationController?.pushViewController(patientRegistrationStep4, animated: true)
            }
        }
    }
    
    func checkSelectedYes() -> PDQuestion? {
        for question in self.tableViewQuestions.arrayOfQuestions! {
            if question.selectedOption != nil && question.selectedOption == true {
                return question
            }
        }
        return nil
    }

}

extension PatientRegistrationStep3VC: YNQuestionsTableViewCellDelegate {
       
    func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell) {
        
    }
}
