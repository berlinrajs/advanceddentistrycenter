//
//  PatientRegistrationStep4VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep4VC: PDViewController {

    @IBOutlet weak var tableViewQuestions: YNTableView!
    @IBOutlet weak var buttonVerified: UIButton!

    @IBOutlet weak var labelTitle: UILabel!
    var isFromStep9: Bool = false
    var currentIndex: Int = 0
    
    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewQuestions.delegateYNTableView = self
        
        if isFromStep9 == false {
            self.tableViewQuestions.arrayOfQuestions = PDQuestion.patientRegistrationStep4Questions(isSpanish)
        } else {
            labelTitle.text = isSpanish ? "Información Médica" : "MEDICAL INFORMATION"
            let count = (16 * (currentIndex + 1)) - 1
            let questions = [PDQuestion](patient.adultRegistration.patientRegistrationMedicalQuestions[(currentIndex * 16)...(patient.adultRegistration.patientRegistrationMedicalQuestions.count > count ? count : patient.adultRegistration.patientRegistrationMedicalQuestions.count - 1)])
            self.tableViewQuestions.arrayOfQuestions = questions
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            if isFromStep9 == false {
                patient.adultRegistration.patientRegistrationStep4Questions = self.tableViewQuestions.arrayOfQuestions!
                let patientRegistrationStep5 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep5SpanishVC" : "kPatientRegistrationStep5VCRef") as! PatientRegistrationStep5VC
                patientRegistrationStep5.patient = patient
                patientRegistrationStep5.isSpanish = self.isSpanish
                self.navigationController?.pushViewController(patientRegistrationStep5, animated: true)
            } else {
                let count = (16 * (currentIndex + 1)) - 1
                if patient.adultRegistration.patientRegistrationMedicalQuestions.count > count {
                    let patientRegistrationStep4 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep4SpanishVC" : "kPatientRegistrationStep4VCRef") as! PatientRegistrationStep4VC
                    patientRegistrationStep4.patient = patient
                    patientRegistrationStep4.isFromStep9 = true
                    patientRegistrationStep4.currentIndex = currentIndex + 1
                    patientRegistrationStep4.isSpanish = self.isSpanish
                    self.navigationController?.pushViewController(patientRegistrationStep4, animated: true)
                } else {
//                    patient.adultRegistration.patientRegistrationMedicalQuestions = self.tableViewQuestions.arrayOfQuestions!
                    let patientRegistrationStep10 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep10SpanishVC" : "kPatientRegistrationStep10VCRef") as! PatientRegistrationStep10VC
                    patientRegistrationStep10.patient = patient
                    patientRegistrationStep10.isSpanish = self.isSpanish
                    self.navigationController?.pushViewController(patientRegistrationStep10, animated: true)
                }
            }
        }
    }
}


extension PatientRegistrationStep4VC: YNQuestionsTableViewCellDelegate {
    func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell) {
        if isFromStep9 == false {
            PopupButton.sharedInstance.show(nil, title: isSpanish ? "Marque su respuesta con un círculo" : "If yes, how often? Select one", selectionMode: .single, question: cell.question) { (popupView, selected) in
                popupView.close()
                if selected {
                    cell.question.selectedOption = true
                }
                self.tableViewQuestions.reloadData()
            }
        } else {
            if cell.question.question == (isSpanish ? "Transfusión sanguínea" : "Blood transfusion") {
                PopupTextField.sharedInstance.showDatePopupWithPlaceHolder(isSpanish ? "Seleccione fecha" : "SELECT DATE", minDate: nil, maxDate: Date(), completion: { (poupView, textField, isEdited) in
                    if isEdited {
                        cell.question.selectedOption = true
                        cell.question.answer = textField.text
                    } else {
                        cell.question.selectedOption = false
                        self.tableViewQuestions.reloadData()
                    }
                })
            } else {
                let placeHolder = cell.question.question == (isSpanish ? "Infecciones recurrentes" :  "Recurrent Infections") ? (isSpanish ? "Tipo de infección" : "TYPE OF INFECTION") : (isSpanish ? "Especifique" : "PLEASE SPECIFY")
                PopupTextField.sharedInstance.showWithPlaceHolder(placeHolder, keyboardType: .default, textFormat: .default) { (poupView, textField, isEdited) in
                    if isEdited {
                        cell.question.selectedOption = true
                        cell.question.answer = textField.text
                    } else {
                        cell.question.selectedOption = false
                        self.tableViewQuestions.reloadData()
                    }
                }
            }
        }
        
    }
}
