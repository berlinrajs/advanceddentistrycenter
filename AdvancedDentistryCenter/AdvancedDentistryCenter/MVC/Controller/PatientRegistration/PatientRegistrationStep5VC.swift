//
//  PatientRegistrationStep5VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep5VC: PDViewController {

    @IBOutlet weak var textFieldLastXray: PDTextField!
    @IBOutlet weak var textFieldLastDentalExam: PDTextField!
    @IBOutlet weak var textViewLastExam: PDTextView!
    @IBOutlet weak var textViewReasonVisitToday: PDTextView!
    @IBOutlet weak var textViewFeelAboutSmile: PDTextView!

    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForTextField(textFieldLastXray, selectedDate: "1 Jan 2016")
        DateInputView.addDatePickerForTextField(textFieldLastDentalExam, selectedDate: "1 Jan 2016")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        patient.adultRegistration.lastXrayDate = textFieldLastXray.isEmpty ? "N/A" : textFieldLastXray.text
        patient.adultRegistration.lastDentalExam = textFieldLastDentalExam.isEmpty ? "N/A" : textFieldLastDentalExam.text
        patient.adultRegistration.doneOnLastDentalExam = (textViewLastExam.isEmpty || textViewLastExam.text == "TYPE HERE") ? "N/A" : textViewLastExam.text
        patient.adultRegistration.reasonForTodayVisit = (textViewReasonVisitToday.isEmpty || textViewReasonVisitToday.text == "TYPE HERE") ? "N/A" : textViewReasonVisitToday.text
        patient.adultRegistration.feelAboutSmile = (textViewFeelAboutSmile.isEmpty || textViewFeelAboutSmile.text == "TYPE HERE") ? "N/A" : textViewFeelAboutSmile.text
        let patientRegistrationStep6 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep6SpanishVC" : "kPatientRegistrationStep6VCRef") as! PatientRegistrationStep6VC
        patientRegistrationStep6.patient = patient
        patientRegistrationStep6.isSpanish = self.isSpanish
        self.navigationController?.pushViewController(patientRegistrationStep6, animated: true)
    }

}


extension PatientRegistrationStep5VC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
}
