//
//  PatientRegistrationStep6VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep6VC: PDViewController {

    @IBOutlet weak var tableViewQuestions: YNTableView!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var textFieldLastPhysicalExam: PDTextField!

    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewQuestions.delegateYNTableView = self
        self.tableViewQuestions.arrayOfQuestions = PDQuestion.patientRegistrationStep6Questions(isSpanish)
        //DateInputView.addDatePickerForTextField(textFieldLastPhysicalExam, minimumDate: nil, maximumDate: NSDate())
        DateInputView.addDatePickerForTextField(textFieldLastPhysicalExam, selectedDate: "1 Jan 2016")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            let question = self.tableViewQuestions.arrayOfQuestions![0]
            patient.adultRegistration.patientRegistrationStep6Questions = self.tableViewQuestions.arrayOfQuestions!
            patient.adultRegistration.lastPhysicalExam = textFieldLastPhysicalExam.isEmpty ? "N/A" : textFieldLastPhysicalExam.text

            if question.selectedOption != nil && question.selectedOption == true {
                let patientRegistrationStep7 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep7SpanishVC" : "kPatientRegistrationStep7VCRef") as! PatientRegistrationStep7VC
                patientRegistrationStep7.patient = patient
                patientRegistrationStep7.isSpanish = self.isSpanish
                self.navigationController?.pushViewController(patientRegistrationStep7, animated: true)
            } else {
                let patientRegistrationStep8 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep8SpanishVC" : "kPatientRegistrationStep8VCRef") as! PatientRegistrationStep8VC
                patientRegistrationStep8.patient = patient
                patientRegistrationStep8.isSpanish = self.isSpanish
                self.navigationController?.pushViewController(patientRegistrationStep8, animated: true)
            }
        }
        
    }

}

extension PatientRegistrationStep6VC: YNQuestionsTableViewCellDelegate {
    func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell) {
        let placeHolder = isSpanish ? ["qué condición le están tratando?", "cuál fue la enfermad o el problema?", "por favor indique cuáles son, incluyendo vitaminas, preparados naturales o a base de hierbas y/o suplementos dietéticos"] : ["What condition is being treated?", "What was the illness or problem?", "Please list all, including vitamins, natural or herbal preparations and/or dietary supplements"]
        PopupTextView.sharedInstance.showWithPlaceHolder(placeHolder[cell.tag - 2]) { (textView, isEdited) in
            if isEdited {
                cell.question.selectedOption = true
                cell.question.answer = textView.text
            } else {
                cell.question.selectedOption = false
                self.tableViewQuestions.reloadData()
            }
        }
    }
}
