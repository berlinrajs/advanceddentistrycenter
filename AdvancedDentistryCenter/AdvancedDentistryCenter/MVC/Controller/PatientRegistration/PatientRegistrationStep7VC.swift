//
//  PatientRegistrationStep7VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep7VC: PDViewController {

    @IBOutlet weak var textFieldPhysicianName: PDTextField!
    @IBOutlet weak var textFieldPhysicianPhone: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZip: PDTextField!
    
    var isSpanish: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !textFieldPhysicianPhone.isEmpty && !textFieldPhysicianPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldPhysicianPhone.isEmpty && !textFieldPhysicianPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER A VALID ZIP")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.adultRegistration.physician = Physician()
            patient.adultRegistration.physician.name = textFieldPhysicianName.isEmpty ? "N/A" : textFieldPhysicianName.text
            patient.adultRegistration.physician.phoneNumber = textFieldPhysicianPhone.isEmpty ? "N/A" : textFieldPhysicianPhone.text
            patient.adultRegistration.physician.address = textFieldAddress.isEmpty ? "N/A" : textFieldAddress.text
            patient.adultRegistration.physician.city = textFieldCity.isEmpty ? "N/A" : textFieldCity.text
            patient.adultRegistration.physician.state = textFieldState.isEmpty ? "N/A" : textFieldState.text
            patient.adultRegistration.physician.zip = textFieldZip.isEmpty ? "N/A" : textFieldZip.text

            
            let patientRegistrationStep8 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep8SpanishVC" : "kPatientRegistrationStep8VCRef") as! PatientRegistrationStep8VC
            patientRegistrationStep8.patient = patient
            patientRegistrationStep8.isSpanish = self.isSpanish
            self.navigationController?.pushViewController(patientRegistrationStep8, animated: true)
        }
    }
}


extension PatientRegistrationStep7VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhysicianPhone {
            return textField.formatPhoneNumber(range, string: string)
        } else {
            return textField.formatZipCode(range, string: string)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
