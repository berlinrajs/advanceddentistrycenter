//
//  PatientRegistrationStep8VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep8VC: PDViewController {
    
    
    @IBOutlet weak var tableViewQuestions1: YNTableView!
    @IBOutlet weak var tableViewQuestions2: YNTableView!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet var viewPopup: UIView!
    @IBOutlet weak var labelTitle1: UILabel!
    @IBOutlet weak var labelTitle2: UILabel!
    @IBOutlet weak var textFieldPopup1: PDTextField!
    @IBOutlet weak var textFieldPopup2: PDTextField!

    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewQuestions1.delegateYNTableView = self
        self.tableViewQuestions1.arrayOfQuestions = PDQuestion.patientRegistrationStep8Questions1(isSpanish)
        
        self.tableViewQuestions2.delegateYNTableView = self
        self.tableViewQuestions2.arrayOfQuestions = PDQuestion.patientRegistrationStep8Questions2(isSpanish)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onRadioWomanAction (_ sender : UIButton){
        if sender.tag == 1{
            tableViewQuestions2.isUserInteractionEnabled = true
            tableViewQuestions2.alpha = 1.0

        }else{
            tableViewQuestions2.isUserInteractionEnabled = false
            tableViewQuestions2.alpha = 0.5
            for ques in self.tableViewQuestions2.arrayOfQuestions! {
                ques.selectedOption = false
            }
            tableViewQuestions2.reloadData()
        }
    }
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.adultRegistration.patientRegistrationStep8Questions = [self.tableViewQuestions1.arrayOfQuestions!, self.tableViewQuestions2.arrayOfQuestions!]
            let patientRegistrationStep9 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep9SpanishVC" : "kPatientRegistrationStep9VCRef") as! PatientRegistrationStep9VC
            patientRegistrationStep9.patient = patient
            patientRegistrationStep9.isSpanish = self.isSpanish
            self.navigationController?.pushViewController(patientRegistrationStep9, animated: true)
        }
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        self.view.endEditing(true)
        if textFieldPopup1.isEmpty || textFieldPopup2.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else {
            let question = self.tableViewQuestions1.arrayOfQuestions![viewPopup.tag]
            question.answer = "\(textFieldPopup1.text!)kSecondValue\(textFieldPopup2.text!)"
            question.selectedOption = true
            viewPopup.removeFromSuperview()
        }
    }
    
    @IBAction func buttonActionPopupCancel(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.tableViewQuestions1.reloadData()
        viewPopup.removeFromSuperview()
    }
    
    func showPopup(_ tag : Int) {
        viewPopup.frame = screenSize
        viewPopup.tag = tag
        textFieldPopup1.text = ""
        textFieldPopup2.text = ""
        if tag == 6 {
            labelTitle1.text = isSpanish ? "cuánto alcohol bebió en las últimas 24 horas?" : "How much alcohol did you drink in the last 24 hours?"
            textFieldPopup1.placeholder = "PLEASE TYPE *"
            textFieldPopup1.inputView = nil
            textFieldPopup1.inputAccessoryView = nil
            labelTitle2.text = isSpanish ? "cuánto bebe por lo general en una semana?" : "How much do you typically drink in a week?"
        } else {
            labelTitle1.text = isSpanish ? "Fecha Articulaciones Artificiales" : "Joint replacement date"
            textFieldPopup1.placeholder = "SELECT DATE *"
            DateInputView.addDatePickerForTextField(textFieldPopup1, selectedDate: "1 Jan 2016")
            labelTitle2.text = isSpanish ? "ha tenido alguna complicación?" : "Have you had any complications?"

        }
        self.view.addSubview(viewPopup)
        viewPopup.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        viewPopup.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }

}

extension PatientRegistrationStep8VC: YNQuestionsTableViewCellDelegate {
    func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell) {
        if cell.question.question == (isSpanish ? "Embarazada?" : "Are you pregnant?") {
            PopupTextField.sharedInstance.showWithPlaceHolder(isSpanish ? "Número de semanas" : "NUMBER OF WEEKS", keyboardType: .numberPad, textFormat: .number) { (poupView, textField, isEdited) in
                
                
                if isEdited {
                    cell.question.selectedOption = true
                    cell.question.answer = textField.text
                } else {
                    cell.question.selectedOption = false
                    cell.question.answer = ""
                    self.tableViewQuestions2.reloadData()
                }
            }
        } else if cell.tag == 3  {
            PopupTextField.sharedInstance.showDatePopupWithPlaceHolder("DATE TREATMENT BEGAN", minDate: nil, maxDate: Date(), completion: { (poupView, textField, isEdited) in
                if isEdited {
                    cell.question.selectedOption = true
                    cell.question.answer = textField.text
                } else {
                    cell.question.selectedOption = false
                    self.tableViewQuestions1.reloadData()
                }
            })
        } else if cell.tag == 5 {
            PopupButton.sharedInstance.show(self, title: isSpanish ? "le interesaría dejar de hacerlo?" : "If so, how interested are you in stopping?", selectionMode: .single, question: cell.question, completion: { (popupView, selected) in
                popupView.close()
                if selected {
                    cell.question.selectedOption = true
                } else {
                    cell.question.selectedOption = false
                    self.tableViewQuestions1.reloadData()
                }
            })
        } else {
            showPopup(cell.tag)
        }
    }
}
