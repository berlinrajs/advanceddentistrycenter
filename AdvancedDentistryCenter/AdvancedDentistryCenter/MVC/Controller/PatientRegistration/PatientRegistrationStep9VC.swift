//
//  PatientRegistrationStep9VC.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep9VC: PDViewController {

    @IBOutlet weak var tableViewQuestions: YNTableView!
    @IBOutlet weak var buttonVerified: UIButton!

    var isSpanish: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewQuestions.delegateYNTableView = self
        self.tableViewQuestions.arrayOfQuestions = PDQuestion.patientRegistrationStep9Questions(isSpanish)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.adultRegistration.patientRegistrationStep9Questions = self.tableViewQuestions.arrayOfQuestions!
            patient.adultRegistration.patientRegistrationMedicalQuestions = PDQuestion.patientRegistrationMedicalQuestions(isSpanish)
            let patientRegistrationStep4 = self.storyboard?.instantiateViewController(withIdentifier: isSpanish ? "kPatientRegistrationStep4SpanishVC" : "kPatientRegistrationStep4VCRef") as! PatientRegistrationStep4VC
            patientRegistrationStep4.patient = patient
            patientRegistrationStep4.isFromStep9 = true
            patientRegistrationStep4.isSpanish = self.isSpanish
            self.navigationController?.pushViewController(patientRegistrationStep4, animated: true)
        }
    }

}


extension PatientRegistrationStep9VC: YNQuestionsTableViewCellDelegate {
    func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell) {
        PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE TYPE") { (textView, isEdited) in
            if isEdited {
                cell.question.selectedOption = true
                cell.question.answer = textView.text
            } else {
                cell.question.selectedOption = false
                self.tableViewQuestions.reloadData()
            }
        }
    }
}
