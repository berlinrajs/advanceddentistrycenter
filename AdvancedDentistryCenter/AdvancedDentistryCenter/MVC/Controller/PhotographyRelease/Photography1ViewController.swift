//
//  Photography1ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/3/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Photography1ViewController: PDViewController {
    @IBOutlet weak var patientSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var doctorSignature : SignatureView!
    @IBOutlet weak var witnessSignature : SignatureView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !patientSignature.isSigned() || !doctorSignature.isSigned() || !witnessSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped || !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "PhotographyFormVC") as! PhotographyFormViewController
            new1VC.patientSign = patientSignature.signatureImage()
            new1VC.doctorSign = doctorSignature.signatureImage()
            new1VC.witnessSign = witnessSignature.signatureImage()
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }

}
