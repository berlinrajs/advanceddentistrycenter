//
//  PhotographyFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/3/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PhotographyFormViewController: PDViewController {

    var patientSign : UIImage!
    var doctorSign : UIImage!
    var witnessSign : UIImage!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelPrintName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!

    @IBOutlet weak var imageviewSignature : UIImageView!
    @IBOutlet weak var imageviewSignatureDoctor : UIImageView!
    @IBOutlet weak var imageviewSignatureWitness : UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        labelPrintName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday

        imageviewSignature.image = patientSign
        imageviewSignatureDoctor.image = doctorSign
        imageviewSignatureWitness.image = witnessSign

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
