//
//  PrivacyPractice1ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPractice1ViewController: PDViewController {

    @IBOutlet weak var labelPrivacy : UILabel!
    @IBOutlet weak var labelDisclosure : UILabel!
//    @IBOutlet weak var labelDate1 : DateLabel!
//    @IBOutlet weak var labelDate2 : DateLabel!
//    @IBOutlet weak var signature1 : SignatureView!
//    @IBOutlet weak var signature2 : SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        labelDate1.todayDate = patient.dateToday
//        labelDate2.todayDate = patient.dateToday
        
        let string : NSString =  labelPrivacy.text!.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName) as NSString
        let range = string.range(of: patient.fullName)
        let range2 = string.range(of: "Health Information Portability and Accountability Act")
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue-Bold", size: 16)!, range: range2)
        labelPrivacy.attributedText = attributedString
        
        let string1 : NSString =  labelDisclosure.text!.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName) as NSString
        let range1 = string1.range(of: patient.fullName)
        let attributedString1 = NSMutableAttributedString(string: string1 as String)
        attributedString1.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        labelDisclosure.attributedText = attributedString1
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
//        if !signature1.isSigned() || !signature2.isSigned(){
//            let alert = Extention.alert("PLEASE SIGN THE FORM")
//            self.presentViewController(alert, animated: true, completion: nil)
//
//        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
//            let alert = Extention.alert("PLEASE SELECT THE DATE")
//            self.presentViewController(alert, animated: true, completion: nil)
//        }else{
//            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("PrivacePracticeFormVC") as! PrivacyPracticeFormViewController
//            new1VC.detailsDict = ["Signature1" : signature1.signatureImage(), "Signature2" : signature2.signatureImage()]
//            new1VC.patient = self.patient
//            self.navigationController?.pushViewController(new1VC, animated: true)
            let healthHistoryVC = adultStoryBoard.instantiateViewController(withIdentifier: "kPatientRegistrationStep1VCRef") as! PatientRegistrationStep1VC
            healthHistoryVC.patient = self.patient
            self.navigationController?.pushViewController(healthHistoryVC, animated: true)

//        }
    }


}
