//
//  PrivacyPracticeFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPracticeFormViewController: PDViewController {

    var detailsDict : NSDictionary!
    @IBOutlet weak var labelName1 : UILabel!
    @IBOutlet weak var labelName2 : UILabel!
    @IBOutlet weak var labelName3 : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!
    @IBOutlet weak var labelOther : UILabel!
    @IBOutlet weak var signature1 : UIImageView!
    @IBOutlet weak var signature2 : UIImageView!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var radioOfficeUse : RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.isPrivacySkipPressed{
            labelName1.text = patient.fullName
            labelName2.text = patient.fullName
            labelName3.text = patient.fullName
            labelDate1.text = patient.dateToday
            labelDate2.text = patient.dateToday
            labelDate3.text = patient.dateToday
            signature1.image = detailsDict["Signature1"] as? UIImage
            signature2.image = detailsDict["Signature2"] as? UIImage

        }else{
            signatureOffice.image = patient.privacySignatureOffice
            radioOfficeUse.setSelectedWithTag(patient.privacyPracticeTag)
            labelOther.text = patient.privacyOthers
            labelDate4.text = patient.dateToday


        }
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
