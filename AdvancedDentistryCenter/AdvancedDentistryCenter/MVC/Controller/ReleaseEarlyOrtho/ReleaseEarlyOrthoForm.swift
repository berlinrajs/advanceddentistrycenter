//
//  ReleaseEarlyOrthoForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 04/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//
//kReleaseEarlyOrthoForm

import UIKit

class ReleaseEarlyOrthoForm: PDViewController {
    
    
    @IBOutlet weak var patientName: UILabel!
    
    @IBOutlet weak var guardianName: UILabel!
    
    
    @IBOutlet weak var parentName: UILabel!
    
    @IBOutlet weak var patientSign: UIImageView!
    
    @IBOutlet weak var witnessSign: UIImageView!
    
    
    @IBOutlet weak var doctorSign: UIImageView!

    @IBOutlet weak var labelDate1: UILabel!
    
    @IBOutlet weak var labelDate2: UILabel!
    
    @IBOutlet weak var labelDate3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        
       patientSign.image = patient.ConsentSign1
         witnessSign.image = patient.ConsentSign2
         doctorSign.image = patient.ConsentSign3
        
        if patient.dateOfBirth.is18YearsOld == false {
            
            patientName.text = patient.fullName
            
            guardianName.text = patient.ReleaseOrthoGuardianName
            
            parentName.text = patient.ReleaseOrthoGuardianName
            
        } else {
        
            patientName.text = "N/A"
            
            guardianName.text = patient.fullName
            
            parentName.text = patient.fullName
        
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
