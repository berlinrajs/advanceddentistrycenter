//
//  ReleaseEarlyOrthoVC.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 04/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit
//kReleaseEarlyOrthoVC

class ReleaseEarlyOrthoVC: PDViewController {

    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelGaurdian : UILabel!
    
    @IBOutlet weak var textFieldGuardianName: PDTextField!
    
    @IBOutlet weak var patientSign: SignatureView!
    
    @IBOutlet weak var witnessSign: SignatureView!
    
    @IBOutlet weak var doctorSign: SignatureView!
    
    @IBOutlet weak var labelDate1: DateLabel!
    
    @IBOutlet weak var labelDate2: DateLabel!
    
    @IBOutlet weak var labelDate3: DateLabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if patient.dateOfBirth.is18YearsOld == false {
            
            textFieldGuardianName.isEnabled = true
            labelGaurdian.text = patient.fullName

            
        } else {
            
            textFieldGuardianName.isEnabled = false
            labelPatientName.text = patient.fullName

            
        }
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func doneButtonPressed(_ sender: AnyObject) {
        
        if !patientSign.isSigned() || !witnessSign.isSigned() || !doctorSign.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else if patient.dateOfBirth.is18YearsOld == false && textFieldGuardianName.isEmpty {
        
            let alert = Extention.alert("PLEASE ENTER GUARDIAN NAME")
            self.present(alert, animated: true, completion: nil)
     
        
        } else {
        
            patient.ConsentSign1 = patientSign.signatureImage()
            patient.ConsentSign2 = witnessSign.signatureImage()
            patient.ConsentSign3 = doctorSign.signatureImage()
        
            patient.ReleaseOrthoGuardianName = textFieldGuardianName.text
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseEarlyOrthoForm") as! ReleaseEarlyOrthoForm
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
            
            
        
        }
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReleaseEarlyOrthoVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        labelPatientName.text = textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
