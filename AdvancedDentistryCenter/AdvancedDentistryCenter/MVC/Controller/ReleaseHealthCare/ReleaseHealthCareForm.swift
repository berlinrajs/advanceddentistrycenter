//
//  ReleaseHealthCareForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 08/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ReleaseHealthCareForm: PDViewController {
    
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelPatientName: FormLabel!
    @IBOutlet var labelPhysicianName: UILabel!
    @IBOutlet var labelPhone: UILabel!
    @IBOutlet var labelAddress: UILabel!
    @IBOutlet var labelPhysicianName2: UILabel!
    @IBOutlet var labelPhone2: UILabel!
    @IBOutlet var labelAddress2: UILabel!
    @IBOutlet var labelPhysicianName3: UILabel!
    @IBOutlet var labelPhone3: UILabel!
    @IBOutlet var labelAddress3: UILabel!
    
    @IBOutlet var labelDoctorNmae: FormLabel!
    @IBOutlet weak var witnessSignatureView : UIImageView!
    @IBOutlet weak var doctorSignatureView : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    
    var witnessSign : UIImage!
    var doctorSign : UIImage!


    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDoctorNmae.text = "Dr. Mati"
        imageViewSignature.image = patient.signature
        labelDate.text = patient.dateToday
        labelPhysicianName.text = patient.physicianName1
        labelPhone.text = patient.phone1
        labelAddress.text = patient.address1
        labelPhysicianName2.text = patient.physicianName2
        labelPhone2.text = patient.phone2
        labelAddress2.text = patient.address2
        labelPhysicianName3.text = patient.physicianName3
        labelPhone3.text = patient.phone3
        labelAddress3.text = patient.address3
        labelDate.text = patient.dateToday
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday

        witnessSignatureView.image = witnessSign
        doctorSignatureView.image = doctorSign

        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
