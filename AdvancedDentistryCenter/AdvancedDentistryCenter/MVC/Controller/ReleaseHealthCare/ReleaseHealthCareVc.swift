//
//  ReleaseHealthCareVc.swift
//  AdvancedDentistryCenter
//
//  Created by SRS Web Solutions on 08/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ReleaseHealthCareVc: PDViewController {
    
    
    @IBOutlet var imageViewSignature: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    @IBOutlet var labelPatientDetails: UILabel!
    @IBOutlet var textFieldPhysicianName: PDTextField!
    @IBOutlet var textFieldPhone: PDTextField!
    @IBOutlet var textFieldAddress: PDTextField!
    @IBOutlet var textFieldPhysicianName2: PDTextField!
    @IBOutlet var textFieldPhone2: PDTextField!
    @IBOutlet var textFieldAddress2: PDTextField!
    @IBOutlet var textFieldPhysicianName3: PDTextField!
    @IBOutlet var textFieldPhone3: PDTextField!
    @IBOutlet var textFieldAddress3: PDTextField!
    @IBOutlet var doctorSignatureView : SignatureView!
    @IBOutlet var witnessSignatureView : SignatureView!
    @IBOutlet var labelDate1 : DateLabel!
    @IBOutlet var labelDate2 : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        
        labelPatientDetails.text = labelPatientDetails.text?.replacingOccurrences(of: "kpatientName", with: "\(patient.fullName)")
        let string : NSString = labelPatientDetails.text! as NSString
        let range = string.range(of: patient.fullName )
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelPatientDetails.attributedText = attributedString
        
    }
    
    
    @IBAction func buttonActionDone(_ sender: AnyObject) {
          self.view.endEditing(true)
        if stage1notfilled() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if stage2notfilled() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if stage3notfilled() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }
        else if  !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber || !textFieldPhone2.isEmpty && !textFieldPhone2.text!.isPhoneNumber || !textFieldPhone3.isEmpty && !textFieldPhone3.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.present(alert, animated: true, completion: nil)
        }
        else if !imageViewSignature.isSigned() || !doctorSignatureView.isSigned() || !witnessSignatureView.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped || !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            patient.signature = imageViewSignature.image
            patient.physicianName1 = textFieldPhysicianName.text
            patient.phone1 = textFieldPhone.text
            patient.address1 = textFieldAddress.text
            patient.physicianName2 = textFieldPhysicianName2.text
            patient.phone2 = textFieldPhone2.text
            patient.address2 = textFieldAddress2.text
            patient.physicianName3 = textFieldPhysicianName3.text
            patient.phone3 = textFieldPhone3.text
            patient.address3 = textFieldAddress3.text
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "ReleaseHealthCareForm") as!ReleaseHealthCareForm
            new1VC.witnessSign = witnessSignatureView.signatureImage()
            new1VC.doctorSign = doctorSignatureView.signatureImage()
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }
    
    func stage1notfilled() -> Bool{
        if textFieldPhysicianName.isEmpty && textFieldPhone.isEmpty && textFieldAddress.isEmpty
        {
            return false
        } else if textFieldPhysicianName.isEmpty || textFieldPhone.isEmpty || textFieldAddress.isEmpty {
            return true
        }
     else {
            return false
    
    }
    }
    
    func stage2notfilled() -> Bool{
        if textFieldPhysicianName2.isEmpty && textFieldPhone2.isEmpty && textFieldAddress2.isEmpty
        {
            return false
        } else if textFieldPhysicianName2.isEmpty || textFieldPhone2.isEmpty || textFieldAddress2.isEmpty {
            return true
        }
        else {
            return false
            
        }
    }
    
    func stage3notfilled() -> Bool{
        if textFieldPhysicianName3.isEmpty && textFieldPhone3.isEmpty && textFieldAddress3.isEmpty
        {
            return false
        } else if textFieldPhysicianName3.isEmpty || textFieldPhone3.isEmpty || textFieldAddress3.isEmpty {
            return true
        }
        else {
            return false
            
        }
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension  ReleaseHealthCareVc: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhone || textField == textFieldPhone2 || textField == textFieldPhone3 {
            return textField.formatPhoneNumber(range, string: string)
            
        }
        return true
    }
    
}




