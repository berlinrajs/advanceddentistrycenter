//
//  ReleaseOfLiabilityForm.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 05/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ReleaseOfLiabilityForm: PDViewController {
    
    @IBOutlet weak var formXray: FormLabel!
    
    @IBOutlet weak var formPatientName: FormLabel!
    
    @IBOutlet weak var formWitnessName: FormLabel!

    @IBOutlet weak var formRelationship: FormLabel!
    
    
    @IBOutlet weak var formRelationName: FormLabel!
    
    
    @IBOutlet weak var patientSign: UIImageView!
    
    @IBOutlet weak var witnessSign: UIImageView!
    
    
    @IBOutlet weak var dentistSign: UIImageView!
    
    
    @IBOutlet weak var labelDate1: UILabel!
    
    @IBOutlet weak var labelDate2: UILabel!
    
    
    @IBOutlet weak var labelDate3: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        patientSign.image = patient.ConsentSign1
         witnessSign.image = patient.ConsentSign2
         dentistSign.image = patient.ConsentSign3
        
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday

        
        self.formXray.text = patient.ReleaseXraydetail
        self.formWitnessName.text = patient.ReleaseXraywitessName == nil ? "N/A" : patient.ReleaseXraywitessName
        self.formRelationName.text = patient.ReleaseXrayRelationName == nil ? "N/A" : patient.ReleaseXrayRelationName
        self.formRelationship.text = patient.ReleaseXrayRelationShip == nil ? "N/A" : patient.ReleaseXrayRelationShip
       
        self.formPatientName.text = patient.fullName
        

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
