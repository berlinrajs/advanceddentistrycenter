//
//  ReleaseOfLiabilityVC.swift
//  AdvancedDentistryCenter
//
//  Created by SRS on 05/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//kReleaseOfLiabilityVC

import UIKit

class ReleaseOfLiabilityVC: PDViewController {
    
    
    @IBOutlet weak var relationView: UIView!
    
    @IBOutlet weak var textFieldXray: PDTextField!
    
    @IBOutlet weak var textFieldWitness: PDTextField!

    @IBOutlet weak var textFieldRelationName: PDTextField!
   
    @IBOutlet weak var textFieldRelationShip: PDTextField!
    
    @IBOutlet weak var patientOrguardianSign: SignatureView!
    
    @IBOutlet weak var witnessSign: SignatureView!
    
    @IBOutlet weak var dentistSign: SignatureView!
    
    
    @IBOutlet weak var labelDate1: DateLabel!
    
    @IBOutlet weak var labelDate2: DateLabel!
    
    @IBOutlet weak var labelDate3: DateLabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.relationView.isUserInteractionEnabled = false
        self.relationView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func SelfOrOtherAction(_ sender: AnyObject) {
        
        patient.relationViewTag = sender.tag
        
        if sender.tag == 2 {
            
            self.relationView.isUserInteractionEnabled = true
            self.relationView.backgroundColor = UIColor.clear
            
        } else {
            
            
            self.relationView.isUserInteractionEnabled = false
            self.relationView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            
        }

        
    }
    
    @IBAction func doneBtnPressed(_ sender: AnyObject) {
        
        if !patientOrguardianSign.isSigned() || !witnessSign.isSigned() || !dentistSign.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        }else if textFieldXray.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if patient.relationViewTag == 2 && textFieldRelationName.isEmpty && textFieldRelationName.isEmpty{
        
                let alert = Extention.alert("PLEASE ENTER RELATION NAME AND RELATIONSHIP")
                self.present(alert, animated: true, completion: nil)
            
        
        }
        
        else {
            
            patient.ConsentSign1 = patientOrguardianSign.signatureImage()
            patient.ConsentSign2 = witnessSign.signatureImage()
            patient.ConsentSign3 = dentistSign.signatureImage()
            
            
            
            patient.ReleaseXraydetail = textFieldXray.text
            patient.ReleaseXraywitessName = textFieldWitness.text
            patient.ReleaseXrayRelationName = textFieldRelationName.text
            patient.ReleaseXrayRelationShip = textFieldRelationShip.text
           
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseOfLiabilityForm") as! ReleaseOfLiabilityForm
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }

        
        
        
        
        
    }
    
    
   
    
}


extension ReleaseOfLiabilityVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldXray {
            
            let newLength = textField.text!.characters.count + string.characters.count - range.length
            return newLength <= 10 // Bool
           
        }
        

        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


