//
//  SinusSurgery1ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/3/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SinusSurgery1ViewController: PDViewController {

    @IBOutlet weak var patientSignature : SignatureView!
    @IBOutlet weak var witnessSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var doctorSignature : SignatureView!
    @IBOutlet weak var labelDate2 : DateLabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !patientSignature.isSigned() || !witnessSignature.isSigned() || !doctorSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped || !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "SinusFormVC") as! SinusSurgeryFormViewController
            new1VC.dictDetails = ["Patient" : patientSignature.signatureImage(), "Witness" : witnessSignature.signatureImage(), "Dentist" : doctorSignature.signatureImage()]
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }

}
