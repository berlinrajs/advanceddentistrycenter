//
//  SmileAnalysis1ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SmileAnalysis1ViewController: PDViewController {
    @IBOutlet weak var radioSmile : RadioButton!
    @IBOutlet weak var radioTeeth : RadioButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if radioSmile.selected == nil || radioTeeth.selected == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)

        }else{
            patient.radioSmileTag = radioSmile.selected == nil ? 0 : radioSmile.selected.tag
            patient.radioTeethTag = radioTeeth.selected == nil ? 0 : radioTeeth.selected.tag
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Smile2VC") as! SmileAnalysis2ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }

    }
    
    @IBAction func smileButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arraySmileTags.contains(sender.tag){
            patient.arraySmileTags.remove(sender.tag)
        }else{
            patient.arraySmileTags.add(sender.tag)
        }
        //POPUP
        if sender.isSelected && sender.tag == 7{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.smileOther = textField.text!
                }else{
                    self.patient.smileOther = "N/A"
                    self.patient.arraySmileTags.remove(sender.tag)
                    sender.isSelected = false
                }
            })
        }else if !sender.isSelected && sender.tag == 7{
            patient.smileOther = "N/A"
        }
    }
    
    @IBAction func teethButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arrayTeethTags.contains(sender.tag){
            patient.arrayTeethTags.remove(sender.tag)
        }else{
            patient.arrayTeethTags.add(sender.tag)
        }
        //POPUP
        if sender.isSelected && sender.tag == 6{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.teethOther = textField.text!
                }else{
                    self.patient.teethOther = "N/A"
                    self.patient.arrayTeethTags.remove(sender.tag)
                    sender.isSelected = false
                }
            })

        }else if !sender.isSelected && sender.tag == 6{
            patient.teethOther = "N/A"
        }


    }
    
    @IBAction func lifeStyleButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arrayLifestyleTag.contains(sender.tag){
            patient.arrayLifestyleTag.remove(sender.tag)
        }else{
            patient.arrayLifestyleTag.add(sender.tag)
        }

        //POPUP
        if sender.isSelected && sender.tag == 4{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.lifestyleOther = textField.text!
                }else{
                    self.patient.lifestyleOther = "N/A"
                    self.patient.arrayLifestyleTag.remove(sender.tag)
                    sender.isSelected = false
                }
            })

        }else if !sender.isSelected && sender.tag == 4{
            patient.lifestyleOther = "N/A"
        }

    }

    @IBAction func makeOverButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arrayMakeOverTag.contains(sender.tag){
            patient.arrayMakeOverTag.remove(sender.tag)
        }else{
            patient.arrayMakeOverTag.add(sender.tag)
        }
        
        //POPUP
        if sender.isSelected && sender.tag == 6{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.makeOverOther = textField.text!
                }else{
                    self.patient.makeOverOther = "N/A"
                    self.patient.arrayMakeOverTag.remove(sender.tag)
                    sender.isSelected = false
                }
            })

        }else if !sender.isSelected && sender.tag == 6{
            patient.makeOverOther = "N/A"
        }


    }
    
}
