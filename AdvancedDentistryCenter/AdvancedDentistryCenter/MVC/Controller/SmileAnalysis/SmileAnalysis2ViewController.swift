//
//  SmileAnalysis2ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SmileAnalysis2ViewController: PDViewController {

    @IBOutlet var radioEvents : RadioButton!
    @IBOutlet var radioChildren : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if radioEvents.selected == nil || radioChildren.selected == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)

        }else{
            let new1VC = consentStoryBoard.instantiateViewController(withIdentifier: "Smile3VC") as! SmileAnalysis3ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
        
    }


    @IBAction func familyButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arrayFamilyTag.contains(sender.tag){
            patient.arrayFamilyTag.remove(sender.tag)
        }else{
            patient.arrayFamilyTag.add(sender.tag)
        }
        //POPUP
        if sender.isSelected && sender.tag == 4{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.familyOther = textField.text!
                }else{
                    self.patient.familyOther = "N/A"
                    self.patient.arrayFamilyTag.remove(sender.tag)
                    sender.isSelected = false
                }
            })
        }else if !sender.isSelected && sender.tag == 4{
            patient.familyOther = "N/A"
        }

    }
    
    @IBAction func appointmentsButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arrayAppointmentTags.contains(sender.tag){
            patient.arrayAppointmentTags.remove(sender.tag)
        }else{
            patient.arrayAppointmentTags.add(sender.tag)
        }
        //POPUP
        if sender.isSelected && sender.tag == 6{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.appointmentOther = textField.text!
                }else{
                    self.patient.appointmentOther = "N/A"
                    self.patient.arrayAppointmentTags.remove(sender.tag)
                    sender.isSelected = false
                }
            })
        }else if !sender.isSelected && sender.tag == 6{
            patient.appointmentOther = "N/A"
        }

    }
    
    @IBAction func musicButtonPressed (_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.arrayMusicTags.contains(sender.tag){
            patient.arrayMusicTags.remove(sender.tag)
        }else{
            patient.arrayMusicTags.add(sender.tag)
        }
        //POPUP
        if sender.isSelected && sender.tag == 8{
            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.musicOther = textField.text!
                }else{
                    self.patient.musicOther = "N/A"
                    self.patient.arrayMusicTags.remove(sender.tag)
                    sender.isSelected = false
                }
            })
        }else if !sender.isSelected && sender.tag == 8{
            patient.musicOther = "N/A"
        }

    }
    
    @IBAction func upcomingEventsPressed (_ sender : RadioButton){
        if sender.tag == 1{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.upcomingEvents = textView.text!
                }else{
                    self.patient.upcomingEvents = "N/A"
                    sender.setSelectedWithTag(2)
                }
            })
        }else{
            self.patient.upcomingEvents = "N/A"
        }
        
    }
    
    @IBAction func childrensPressed (_ sender : RadioButton){
        if sender.tag == 1{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY THEIR NAMES AND AGES", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.childrens = textView.text!
                }else{
                    self.patient.childrens = "N/A"
                    sender.setSelectedWithTag(2)
                }
            })
        }else{
            self.patient.childrens = "N/A"
        }

    }
}
