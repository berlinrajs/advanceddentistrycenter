//
//  SmileAnalysis3ViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SmileAnalysis3ViewController: PDViewController {
    
    @IBOutlet weak var textviewHobbies : UITextView!
    @IBOutlet weak var textviewKnowmore : UITextView!
//    @IBOutlet weak var signaturePatient : SignatureView!
//    @IBOutlet weak var labelPatientName : UILabel!
//    @IBOutlet weak var labelDate : DateLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        labelDate.todayDate = patient.dateToday
//        labelPatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
//        if !signaturePatient.isSigned(){
//            let alert = Extention.alert("PLEASE SIGN THE FORM")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }else if !labelDate.dateTapped{
//            let alert = Extention.alert("PLEASE SELECT THE DATE")
//            self.presentViewController(alert, animated: true, completion: nil)
//        }else{
            patient.hobbies = textviewHobbies.text! == "PLEASE SPECIFY" ? "N/A" : textviewHobbies.text!
            patient.knowMore = textviewKnowmore.text! == "PLEASE SPECIFY" ? "N/A" : textviewKnowmore.text!
           // patient.smileAnalysisSignature = signaturePatient.signatureImage()
            //            let new1VC = consentStoryBoard.instantiateViewControllerWithIdentifier("SmileFormVC") as! SmileAnalysisFormViewController
            //            new1VC.patient = self.patient
            //            self.navigationController?.pushViewController(new1VC, animated: true)
            let new1VC = patientStoryBoard.instantiateViewController(withIdentifier: "PatientInformation1VC") as! PatientInformation1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
//    }
    
}


extension SmileAnalysis3ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE SPECIFY" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE SPECIFY"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
