//
//  ToothWhiteningStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothWhiteningStep1VC: PDViewController {

    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var dentistSignatureView: SignatureView!
    @IBOutlet weak var witnessSignatureView : SignatureView!
    @IBOutlet weak var labelDate3 : DateLabel!
    @IBOutlet weak var initialView: SignatureView!

    
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelPatientName.text = patient.fullName
        labelDentistName.text = patient.doctorName
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !patientSignatureView.isSigned() || !witnessSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !dentistSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM YOUR DENTIST")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else if !initialView.isSigned() {
            let alert = Extention.alert("PLEASE ADD YOUR INITIAL TO THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.Toothwhitningsignature1 = patientSignatureView.signatureImage()
            patient.Toothwhitningsignature2 = dentistSignatureView.signatureImage()
            patient.Toothwhitningsignature3 = !initialView.isSigned() ? nil : initialView.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kToothWhiteningFormVC") as! ToothWhiteningFormVC
            formVC.patient = patient
            formVC.witnessSign = witnessSignatureView.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
