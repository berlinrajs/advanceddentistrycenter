//
//  TreatmentFormViewController.swift
//  AdvancedDentistryCenter
//
//  Created by Bala Murugan on 8/3/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TreatmentFormViewController: PDViewController {

    var dictDetails : NSDictionary!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var signPatient : UIImageView!
    @IBOutlet weak var labeldate1 : UILabel!
    @IBOutlet weak var signWitness : UIImageView!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var signDentist : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        labeldate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        signPatient.image = dictDetails["Patient"] as? UIImage
        signWitness.image = dictDetails["Witness"] as? UIImage
        signDentist.image = dictDetails["Dentist"] as? UIImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
