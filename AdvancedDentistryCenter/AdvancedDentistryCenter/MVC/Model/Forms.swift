//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

//let kHealthHistory = "HEALTH HISTORY FORM ADA FORM"

let kConsentForms = "CONSENT FORMS"

let kBoneSurgery = "INFORMED CONSENT FOR BONE REGENERATIVE SURGERY"
let kTissueSurgery = "CONSENT FOR CONNECTIVE TISSUE GRAFT (GUM GRAFT) SURGERY"
let kTreatment = "CONSENT FOR TREATMENT"
let kAgreement = "INFORMED CONSENT AND AGREEMENT FOR THE INVISALIGN PATIENT"
let kSinusSurgery = "CONSENT FOR MAXILLARY SINUS ELEVATION SURGERY"
let kAnesthesia = "CONSENT FOR SURGERY AND ANESTHESIA"
let kCrownLengthening = "INFORMED CONSENT FOR CROWN LENGTHENING SURGERY"
let kDeclineTreatment = "DECLINE TREATMENT CONSENT"
let kPatientInformation = "IMPLANT PATIENT INFORMATION AND CONSENT FORM"
let kOcclusalEquilibration = "INFORMED CONSENT FOR OCCLUSAL EQUILIBRATION"
let kNitrousOxide = "NITROUS OXIDE INFORMED CONSENT FORM"
let kOralSedation = "ORAL SEDATION INFORMATION AND CONSENT FORM"
let KPhotography = "PHOTOGRAPHY RELEASE"
let kOrthodontic = "RELEASE OF LIABILITY FOR EARLY REMOVAL OF ORTHODONTIC APPLIANCES"
let KXray = "RELEASE OF LIABILITY FOR XRAY DENAIL"
let KEndodonticTherapy = "ENDODONTIC THERAPY CONSENT FORM"
let kToothWhitening = "INFORMED CONSENT FOR ZOOM! TOOTH WHITENING TREATMENT"
let kHealthCare = "AUTHORIZATION TO RELEASE HEALTH CARE INFORMATION"
let kFamilyMedicalHistory = "FAMILY MEDICAL HISTORY"
let kHealthHistoryUpdate = "HEALTH HISTORY UPDATE"
//let kPrivacyPractice = "ACKNOWLEDGEMENT OF RECEIPT OF NOTICE OF PRIVACY PRACTICES"
let toothNumberRequired = [KEndodonticTherapy,kHealthHistoryUpdate]

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
//let kSmileAnalysis = "SMILE ANALYSIS"
//let kNewPatientInformation = "NEW PATIENT INFORMATION"

let kNewPatientSignInForm = "NEW PATIENT SIGN IN FORM"

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var isToothNumberRequired : Bool!
    var toothNumbers : String!
    var toothNPO : String!
 

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (_ completion :(_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kNewPatientSignInForm,kFamilyMedicalHistory,kHealthHistoryUpdate,kInsuranceCard,kDrivingLicense, kConsentForms]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnected ? false : true, formObj)
    }
    

    fileprivate class func getFormObjects (_ forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 5 : idx
            formObj.formTitle = form
             formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kBoneSurgery,kTissueSurgery,kTreatment,kAgreement,kSinusSurgery,kAnesthesia,kCrownLengthening,kPatientInformation,kOcclusalEquilibration,kNitrousOxide,kOralSedation,KPhotography,kOrthodontic,KXray,KEndodonticTherapy,kToothWhitening,kDeclineTreatment,kHealthCare], isSubForm:  true)
            }
            formList.append(formObj)
        }
        return formList
    }
    
}
