//
//  PDPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    
    var initial: String!
    var nickName : String!
    var gender: Int!
    var workPhone : String!
    var homePhone : String!
     var phoneNumber : String!
     var fax : String!
    
//    var initial : String?
//    var prefferedName : String!
    var dateOfBirth : String!
    var dateToday : String!
//    var doctorName: String!
    var signature : UIImage!
    var signature2 : UIImage!
    var signature3 : UIImage!
    var signature4 : UIImage!
    var nitrousOxidemultiple : [Int]!
    var patientIdentification : String!
    var WitnessName : String!
    var relationshipName : String!
    var relationshipType : String!
    var popUpText : String!
    
    
    //NEW PATIENT
    var parentFirstName : String?
    var parentLastName : String?
    var parentDateOfBirth : String?
    
    var addressLine : String!
    var city : String!
    var state : String!
    var zipCode : String!
    var socialSecurityNumber : String?
    var email : String!
    var employerName : String?
    var employerPhoneNumber : String?
    var emergencyContactName : String!
    var emergencyContactPhoneNumber : String!
    var lastDentalVisit : String?
    var reasonForTodayVisit : String?
    var isHavingPain : Bool!
    var painLocation : String?
    var anyConcerns : String?
    var hearAboutUs : String?
    var relation : String!
    var initials : UIImage!
    var signature1 : UIImage!
//    var signature2 : UIImage!
//    var signature3 : UIImage!
//    var signature4 : UIImage!
    var signature5 : UIImage!
    var signature6 : UIImage!
    
    var physicianName : String!
    
    
    var fullName: String {
        return  initial.characters.count == 0 ? firstName + " " + lastName : firstName + " "  + initial + " " + lastName
    }
    
    // NitrousOxideConsent
    var isSelectedButton1 : Bool!
    var isSelectedButton2 : Bool!
    var isSelectedButton3 : Bool!
    var isSelectedButton4 : Bool!
    var isSelectedButton5 : Bool!
    var isSelectedButton6 : Bool!
    var isSelectedButton7 : Bool!
    var isSelectedButton8 : Bool!
    var isSelectedButton9 : Bool!
    var isSelectedButton10 : Bool!
    var isSelectedButton11 : Bool!
    var isSelectedButton12 : Bool!
    var isSelectedButton13 : Bool!
    // Release health care
    var physicianName1: String!
    var physicianName2: String!
    var physicianName3: String!
    var phone1: String!
    var phone2: String!
    var phone3: String!
    var address1: String!
    var address2: String!
    var address3: String!
    //FamilyMedicalHistory
    var buttontag1 : Int?
    var buttontag2 : Int?
    var buttontag3 : Int?
    var buttontag4 : Int?
    var buttontag5 : Int?
    var buttontag6 : Int?
    var buttontag7 : Int?
    var buttontag8 : Int?
    var buttontag9 : Int?
    var buttontag10 : Int?
    var buttontag11 : Int?
    var textViewOthers1 : String!
    var textViewOthers2 : String!
    var textViewOthers3 : String!
    var textViewCommments : String!
    //Health history update
    var healthDateChange : String!
    var healthComments1 : String!
    var healthPhysicianName : String!
    var healthPhysiciansPhone : String!
    var healthComments2 : String!
    var healthLastPhysicialExam : String!
    var healthAllergies : String!
    
    
    
    var healthDateChange2 : String!
    var healthComments3 : String = ""
    var healthPhysicianName2 : String!
    var healthPhysiciansPhone2 : String!
    var healthComments4 : String = ""
    var healthLastPhysicialExam2 : String!
    var healthAllergies2 : String!
    
    
    
    // EndodonticTheraphy
    var arrayEndodonticQuestions : NSArray!

    
    // COSNENT FOR INVISALIGNPATIENT
    var ConsentSign1 : UIImage!
    var ConsentSign2 : UIImage!
    var ConsentSign3 : UIImage!
    var ConsentSign4 : UIImage!
    var ConsentAddress : String!
    var ConsentState : String!
    var ConsentCity : String!
    var Consentzip : String!
    var ConsentWitnessname : String!
    
    
    //Release XRAY
    var ReleaseXraydetail : String!
    var ReleaseXraywitessName : String!
    var ReleaseXrayRelationName : String!
    var ReleaseXrayRelationShip : String!
    var relationViewTag :Int?
    
    // TOOTH WHITENING STEP1 VC
    
    var Toothwhitningsignature1 : UIImage!
    var Toothwhitningsignature2 : UIImage!
    var Toothwhitningsignature3 : UIImage!
    
    var doctorName: String = "Dr.Mati"
    
    
    //Adult Registration
    var adultRegistration: AdultRegistration!

    
    //SCAN CARDS
    var frontImage: UIImage?
    var backImage: UIImage?

    //PRIVACY PRACTICES
    var privacyPracticeTag : Int = 0
    var isPrivacySkipPressed : Bool = false
    var privacyOthers : String = ""
    var privacySignatureOffice : UIImage = UIImage()

    
    // ReleaseEarlyOrthoVC
    
    var ReleaseOrthoGuardianName : String!
    var age: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let birthDate = dateFormatter.date(from: dateOfBirth.capitalized)
        let ageComponents = (Calendar.current as NSCalendar).components(.year, from: birthDate!, to: Date(), options: NSCalendar.Options(rawValue: 0))
        return "\(ageComponents.year)"
    }
    

    var currentDateAndTime : String{
        var strTime = ""
        let dateFormatter : DateFormatter  = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
        strTime = dateFormatter.string(from: Date())
        return strTime
    }
    
    var patientInformation : PatientInformation = PatientInformation()

    
    
     required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
   
    var anesthesia : Anesthesia = Anesthesia()
    var equilibration : Equilibration = Equilibration()
    
    ///SMILE ANALYSIS
    var radioSmileTag : Int!
    var radioTeethTag : Int!
    var arraySmileTags : NSMutableArray = NSMutableArray()
    var smileOther : String = "N/A"
    var arrayTeethTags : NSMutableArray = NSMutableArray()
    var teethOther : String = "N/A"
    var arrayLifestyleTag : NSMutableArray = NSMutableArray()
    var lifestyleOther : String = "N/A"
    var arrayMakeOverTag : NSMutableArray = NSMutableArray()
    var makeOverOther : String = "N/A"
    var arrayFamilyTag : NSMutableArray = NSMutableArray()
    var familyOther : String = "N/A"
    var arrayAppointmentTags : NSMutableArray = NSMutableArray()
    var appointmentOther = "N/A"
    var arrayMusicTags : NSMutableArray = NSMutableArray()
    var musicOther : String = "N/A"
    var upcomingEvents : String = "N/A"
    var childrens : String = "N/A"
    var hobbies : String!
    var knowMore : String!
    var smileAnalysisSignature : UIImage!
    var patientNumber : String = ""

}

class Anesthesia : NSObject{
    
    var arraySelectedProcedures : NSArray!
    var patientSignature : UIImage!
    var printName : String = "N/A"
    var relationship : String = "N/A"
    var dentistSignature : UIImage!
    var witnessSignature : UIImage!
    
    
    required override init() {
        super.init()
    }

}


class Equilibration : NSObject{
    
    var patientSignature : UIImage!
    var printName : String = "N/A"
    var relationship : String = "N/A"
    var dentistSignature : UIImage!
    var witnessSignature : UIImage!
    
    
    required override init() {
        super.init()
    }
    
}


class AdultRegistration: NSObject {
    var patientDetails: PDPatient!
    var cellPhone: String!
    var emergencyCellPhone: String!
    var occupation: String!
    var height: String!
    var weight: String!
    var emergencyContactCellNumber : String!
    var responsiblePerson: String!
    var responsiblePersonRelation: String!
    var patientRegistrationStep3Questions: [PDQuestion]!
    var patientRegistrationStep4Questions: [PDQuestion]!
    var patientRegistrationMedicalQuestions: [PDQuestion]!
    
    var lastXrayDate: String!
    var lastDentalExam: String!
    var doneOnLastDentalExam: String!
    var reasonForTodayVisit: String!
    var feelAboutSmile: String!
    var lastPhysicalExam: String!
    var patientRegistrationStep6Questions: [PDQuestion]!
    var physician : Physician!
    var patientRegistrationStep8Questions: [[PDQuestion]]!
    var patientRegistrationStep9Questions: [PDQuestion]!
    var patientRegistrationStep10Questions: [PDQuestion]!
    
    var previousDentist: String!
    var previousDentistPhone: String!
    
    
}

class Physician : NSObject {
    var name: String!
    var phoneNumber: String!
    var address: String!
    var city: String!
    var state: String!
    var zip: String!
}


class PatientInformation : NSObject {
    
    var primaryCompanyName : String = "N/A"
    var primaryCompanyNumber : String = "N/A"
    var primaryGroupNumber : String = "N/A"
    var primaryInsuredId : String = "N/A"
    var primaryAddress : String = "N/A"
    var primaryCity : String = "N/A"
    var primaryState : String = "N/A"
    var primaryZipcode : String = "N/A"
    var primaryName : String = "N/A"
    var primaryRelationship : String = "N/A"
    var primaryDateOfBirth : String = "N/A"
    var primarySocialSecurityNumber : String = "N/A"
    var primaryEmployerName : String = "N/A"
    var primaryPracticeTag : Int = 0
    
    var secondaryCompanyName : String = "N/A"
    var secondaryCompanyNumber : String = "N/A"
    var secondaryGroupNumber : String = "N/A"
    var secondaryInsuredId : String = "N/A"
    var secondaryAddress : String = "N/A"
    var secondaryCity : String = "N/A"
    var secondaryState : String = "N/A"
    var secondaryZipcode : String = "N/A"
    var secondaryName : String = "N/A"
    var secondaryRelationship : String = "N/A"
    var secondaryDateOfBirth : String = "N/A"
    var secondarySocialSecurityNumber : String = "N/A"
    var secondaryEmployerName : String = "N/A"
    var secondaryPracticeTag : Int = 0
    
    var responsibleName : String!
    var responsibleRelationship : String!
    var responsiblePhoneNUmber : String!
    var responsibleDateOfBirth : String!
    var responsibleAddress : String!
    var responsibleCity : String!
    var responsibleState : String!
    var responsibleZipcode : String!
    var responsibleEmployerName : String!
    var responsibleWOrkPhone : String!
    var responsibleDriverLicense : String!
    var responsiblePaymentType : Int!
    var responsibleCardNumber : String!
    var responsibleExpDate : String!
    var responsibleSocialSecurityNumber : String!
    var legalGuardianName : String = "N/A"
    var legalGuardianRelationship : String = "N/A"
    var legalGuardianPractice : Int = 0
    var signature : UIImage!
    var signatureOfficial : UIImage!
    
    var emergencyName : String!
    var emergencyRelationship : String!
    var emergencyCity : String!
    var emergencyState : String!
    var emergencyCellNumber : String!
    var emergencyWorkNumber : String!
    var emergencyHomeNumber : String!
    
    required override init() {
        super.init()
    }

}


