//
//  PDQuestion.swift
//  ProDental
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDQuestion: NSObject {
    var question : String!
    var isAnswerRequired : Bool!
   // var isSelected : Bool = false
    var answer : String?
    var options: [PDOption]?
//    var selectedOption : Bool! {
//        didSet {
//            if selectedOption == false {
//                self.answer = nil
//            }
//        }
//    }
    
    var selectedOption : Bool? = false {
        didSet {
            if selectedOption == false {
                answer = nil
            }
        }
    }
    
    init(question: String) {
        super.init()
        self.question = question
        self.isAnswerRequired = false
        self.selectedOption = false
    }
    
    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.selectedOption = false
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [PDQuestion] {
        var questions  = [PDQuestion]()
        for dict in arrayResult {
            let obj = PDQuestion(dict: dict as! NSDictionary)
            obj.selectedOption = false
            questions.append(obj)
        }
        return questions
    }
    
    class func arrayOfQuestions(_ array: [String]) -> [PDQuestion]{
        var questions = [PDQuestion]()
        for string in array {
            let question = PDQuestion(question: string)
            question.selectedOption = false
            questions.append(question)
        }
        return questions
    }
    
    class func patientRegistrationStep3Questions(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Tuberculosis activa",
                                    "Tos persistente de más de 3 semanas de duración",
                                    "Tos que produce sangre",
                                    "Ha estado en contacto con alguien que tiene tuberculosis"] : ["Active Tuberculosis",
                                                                                                   "Persistent cough greater than a 3 week duration",
                                                                                                   "Cough that produces blood",
                                                                                                   "Been exposed to anyone with tuberculosis"]
        return self.arrayOfQuestions(question)
    }

    
    class func patientRegistrationStep4Questions(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Le sangran las encías cuando se cepilla o cuando usa seda dental?",
                                    "Le duelen los dientes con el frío, calor, con los dulces o al presionar?",
                                    "Sufre de boca seca?",
                                    "Ha tenido algún tratamiento periodontal (de la encía)?",
                                    "Ha tenido tratamientos de ortodoncia (con aparatos)?",
                                    "Ha tenido algún problema asociado con un tratamiento dental anterior?",
                                    "Está fluorada el agua que llega a su casa?",
                                    "Bebe usted agua embotellada o filtrada?",
                                    "Tiene dolor o molestias dentales en este momento?",
                                    "Sufre de dolor de oídos o del cuello?",
                                    "Tiene algún ruido, salto o molestia en la mandíbula?",
                                    "Tiene bruxismo o hace rechinar los dientes?",
                                    "Tiene lesiones o úlceras en su boca?",
                                    "Usa dentaduras (placas) completas o parciales?",
                                    "Participa en actividades enérgicas de recreación?",
                                    "Ha sufrido alguna lesión grave en la cabeza o en la boca?",
                                    "Le quedan alimentos o seda dental atrapados entre los dientes?"] : ["Do your gums bleed when you brush or floss?",
                                                                                                         "Are your teeth sensitive to cold, hot, sweets or pressure?",
                                                                                                         "Is your mouth dry?",
                                                                                                         "Have you had any periodontal (gum) treatments?",
                                                                                                         "Have you ever had orthodontic (braces) treatment?",
                                                                                                         "Have you had any problems associated with previous dental treatment?",
                                                                                                         "Is your home water supply fluoridated?",
                                                                                                         "Do you drink bottled or filtered water?",
                                                                                                         "Are you currently experiencing dental pain or discomfort?",
                                                                                                         "Do you have earaches or neck pains?",
                                                                                                         "Do you have any clicking, popping or discomfort in the jaw?",
                                                                                                         "Do you brux or grind your teeth?",
                                                                                                         "Do you have sores or ulcers in your mouth?",
                                                                                                         "Do you wear dentures or partials?",
                                                                                                         "Do you participate in active recreational activities?",
                                                                                                         "Have you ever had a serious injury to your head or mouth?"]
        let questions = self.arrayOfQuestions(question)
        questions[7].isAnswerRequired = true
        let options = isSpanish ? ["A diario", "Semanalmente", "Ocasionalmente"] : ["DAILY", "WEEKLY", "OCCASIONALLY"]
        questions[7].options = PDOption.arrayOfOptions(options)
        return questions
    }
    
    class func patientRegistrationStep6Questions(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Se encuentra ahora bajo el cuidado de un médico?",
                                    "Se encuentra usted sano/a?",
                                    "Ha habido algún cambio en su salud general durante el último año?",
                                    "Ha tenido alguna enfermedad grave, operación o ha sido hospitalizado/a en los últimos 5 años?",
                                    "Está tomando o ha tomado recientemente algún medicamento recetado o sin receta?"] : ["Are you now under the care of a physician?",
                                                                                                                            "Are you in good health?",
                                                                                                                            "Has there been any change in your general health within the past year?",
                                                                                                                            "Have you had a serious illness, operation or been hospitalized in the past 5 years?",
                                                                                                                            "Are you taking or have you recently taken any prescription or over the counter medicine(s)?"]
        let questions = self.arrayOfQuestions(question)
        for (idx, obj) in questions.enumerated() {
            if idx > 1 {
                obj.isAnswerRequired = true
            }
        }
        return questions
    }
    
    class func patientRegistrationStep8Questions1(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Usa lentes de contacto?",
                                    "Articulaciones Artificiales. Ha tenido algún reemplazo ortopédico total de una articulación (cadera, rodilla, codo, dedo)?",
                                    "Está tomando o tiene que empezar a tomar un agente antirresortivo (como Fosamax®, Actonel®, Atelvia, Boniva®, Reclast, Prolia) debido a osteoporosis o a enfermedad de Paget?",
                                    "Desde el año 2001, ha sido tratado/a o está actualmente en lista para comenzar tratamiento con un agente antirresortivo (como Aredia®, Zometa®, XGEVA) para dolor óseo, hipercalcemia o complicaciones esqueléticas derivadas de la enfermedad de Paget, mieloma múltiple o cáncer metastásico?",
                                    "Usa sustancias reguladas (drogas)?",
                                    "Usa tabaco (fumado, aspirado/rapé, masticado, en bidis)?",
                                    "Bebe bebidas alcohólicas?"] : ["Do you wear contact lenses?",
                                                                     "Joint Replacement. Have you had an orthopedic total joint (hip, knee, elbow, finger) replacement?",
                                                                     "Are you taking or scheduled to begin taking an antiresorptive agent (like Fosamax® , Actonel®, Atelvia, Boniva®, Reclast, Prolia) for osteoporosis or Paget’s disease?",
                                                                     "Since 2001, were you treated or are you presently scheduled to begin treatment with an antiresorptive agent (like Aredia®, Zometa®, XGEVA) for bone pain, hypercalcemia or skeletal complications resulting from Paget’s disease, multiple myeloma or metastatic cancer?",
                                                                     "Do you use controlled substances (drugs)?",
                                                                     "Do you use tobacco (smoking, snuff, chew, bidis)?",
                                                                     "Do you drink alcoholic beverages?"]
        let questions = self.arrayOfQuestions(question)
        let options = isSpanish ? ["Mucho", "Algo", "No me interesa"] : ["VERY", "SOMEWHAT", "NOT INTERESTED"]
        questions[1].isAnswerRequired = true
        questions[3].isAnswerRequired = true
        questions[5].isAnswerRequired = true
        questions[5].options = PDOption.arrayOfOptions(options)
        questions[6].isAnswerRequired = true
        return questions
    }

    class func patientRegistrationStep8Questions2(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Embarazada?",
                                    "Tomando píldoras anticonceptivas o de sustitución hormonal?",
                                    "Amamantando?"] : ["Are you pregnant?",
                                                       "Are you taking birth control pills or hormonal replacement?",
                                                       "Are you nursing?"]
        let questions = self.arrayOfQuestions(question)
        questions[0].isAnswerRequired = true
        return questions
    }
    
    class func patientRegistrationStep9Questions(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Anestésicos locales",
                                    "Aspirina",
                                    "Penicilina u otros antibióticos",
                                    "Barbituratos, sedativos o pastillas para dormir",
                                    "Sulfas",
                                    "Codeína u otros narcóticos",
                                    "Metales",
                                    "Látex (goma)",
                                    "Yodo",
                                    "Polen (fiebre del heno)/estacional",
                                    "Animales",
                                    "Alimentos",
                                    "Otros"] : ["Local anesthetics",
                                                "Aspirin",
                                                "Penicillin or other antibiotics",
                                                "Barbiturates, sedatives, or sleeping pills",
                                                "Sulfa drugs",
                                                "Codeine or other narcotics",
                                                "Metals",
                                                "Latex (rubber)",
                                                "Iodine",
                                                "Hay fever/seasonal",
                                                "Animals",
                                                "Food",
                                                "Other"]
        let questions = self.arrayOfQuestions(question)
        for (_, obj) in questions.enumerated() {
            obj.isAnswerRequired = true
        }
        return questions
    }

    
    class func patientRegistrationMedicalQuestions(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Enfermedad cardiovascular",
                                    "Angina",
                                    "Arterioesclerosis",
                                    "Insuficiencia cardíaca congestiva",
                                    "Daño en las válvulas cardíacas",
                                    "Infarto del miocardio",
                                    "Soplo en el corazón",
                                    "Presión arterial baja",
                                    "Presión arterial alta",
                                    "Otros defectos congénitos del corazón",
                                    "Prolapso de la válvula mitral",
                                    "Marcapasos",
                                    "Fiebre reumática",
                                    "Enfermedad cardíaca reumática",
                                    "Sangramiento anormal",
                                    "Anemia",
                                    "Transfusión sanguínea",
                                    "Hemofilia",
                                    "SIDA o infección por VIH",
                                    "Artritis",
                                    "Enfermedad autoinmune",
                                    "Artritis reumatoidea",
                                    "Lupus eritematoso sistémico",
                                    "Asma",
                                    "Bronquitis",
                                    "Enfisema",
                                    "Sinusitis",
                                    "Tuberculosis",
                                    "Cáncer/Quimoterapia/ Radioterapia",
                                    "Dolores de pecho por esfuerzo",
                                    "Dolor crónico",
                                    "Diabetes Tipo I o II",
                                    "Trastornos de alimentación",
                                    "Malnutrición",
                                    "Enfermedad gastrointestinal",
                                    "Reflujo G.E./ardor persistente",
                                    "Úlceras",
                                    "Alteraciones de la tiroides",
                                    "Derrame cerebral",
                                    "Glaucoma",
                                    "Hepatitis, ictericia o enfermedad hepática",
                                    "Epilepsia",
                                    "Desmayos o ataques epilépticos",
                                    "Alteraciones neurológicas",
                                    "Alteraciones del sueño",
                                    "Usted ronca?",
                                    "Alteraciones mentales",
                                    "Infecciones recurrentes",
                                    "Alteraciones renales",
                                    "Sudor nocturno",
                                    "Osteoporosis",
                                    "Inflamación persistente de los ganglios del cuello",
                                    "Cefaleas graves/jaquecas",
                                    "Pérdida de peso severa o rápida",
                                    "Enfermedades venéreas",
                                    "Orina en forma excesiva"] : ["Cardiovascular disease",
                                                                  "Angina",
                                                                  "Arteriosclerosis",
                                                                  "Congestive heart failure",
                                                                  "Damaged heart valves",
                                                                  "Heart attack",
                                                                  "Heart murmur",
                                                                  "Low blood pressure",
                                                                  "High blood pressure",
                                                                  "Other congenital heart defects",
                                                                  "Mitral valve prolapse",
                                                                  "Pacemaker",
                                                                  "Rheumatic fever",
                                                                  "Rheumatic heart disease",
                                                                  "Abnormal bleeding",
                                                                  "Anemia",
                                                                  "Blood transfusion",
                                                                  "Hemophilia",
                                                                  "AIDS or HIV infection",
                                                                  "Arthritis",
                                                                  "Autoimmune disease",
                                                                  "Rheumatoid arthritis",
                                                                  "Systemic lupus erythematosus",
                                                                  "Asthma",
                                                                  "Bronchitis",
                                                                  "Emphysema",
                                                                  "Sinus trouble",
                                                                  "Tuberculosis",
                                                                  "Cancer/Chemotherapy/Radiation Treatment",
                                                                  "Chest pain upon exertion",
                                                                  "Chronic pain",
                                                                  "Diabetes Type I or II",
                                                                  "Eating disorder",
                                                                  "Malnutrition",
                                                                  "Gastrointestinal disease",
                                                                  "G.E. Reflux/persistent heartburn",
                                                                  "Ulcers",
                                                                  "Thyroid problems",
                                                                  "Stroke",
                                                                  "Glaucoma",
                                                                  "Hepatitis, jaundice or liver disease",
                                                                  "Epilepsy",
                                                                  "Fainting spells or seizures",
                                                                  "Neurological disorders",
                                                                  "Sleep disorder",
                                                                  "Do you snore?",
                                                                  "Mental health disorders",
                                                                  "Recurrent Infections",
                                                                  "Kidney problems",
                                                                  "Night sweats",
                                                                  "Osteoporosis",
                                                                  "Persistent swollen glands in neck",
                                                                  "Severe headaches/migraines",
                                                                  "Severe or rapid weight loss",
                                                                  "Sexually transmitted disease",
                                                                  "Excessive urination"]
        let questions = self.arrayOfQuestions(question)
        let answerRequired = isSpanish ? ["Transfusión sanguínea", "Alteraciones neurológicas", "Alteraciones mentales", "Infecciones recurrentes"] : ["Blood transfusion", "Neurological disorders", "Mental health disorders", "Recurrent Infections"]
        let questionsAnswerRequired = questions.filter { (obj) -> Bool in
            return answerRequired.contains(obj.question)
        }
        for obj in questionsAnswerRequired {
            obj.isAnswerRequired = true
        }
        return questions
    }

    class func patientRegistrationStep10Questions(_ isSpanish : Bool) -> [PDQuestion] {
        let question = isSpanish ? ["Válvula cardíaca artificial (prótesis)",
                                    "Previa endocarditis infecciosa",
                                    "Válvulas dañadas en corazón transplantado",
                                    "ECC cianótica, sin reparar",
                                    "Reparada en los últimos 6 meses (completamente)",
                                    "ECC reparada con defectos residuales",
                                    "Tiene alguna enfermedad, condición o problema que no figure más arriba y que cree que yo debería saber?",
                                    "Le ha recomendado algún médico o su dentista anterior que tome antibióticos antes de su tratamiento dental?"] : ["Artificial (prosthetic) heart valve",
                                                                                                                                                         "Previous infective endocarditis",
                                                                                                                                                         "Damaged valves in transplanted heart",
                                                                                                                                                         "Unrepaired, cyanotic CHD",
                                                                                                                                                         "Repaired (completely) in last 6 months",
                                                                                                                                                         "Repaired CHD with residual defects",
                                                                                                                                                         "Do you have any disease, condition, or problem not listed above that you think I should know about?",
                                                                                                                                                         "Has a physician or previous dentist recommended that you take antibiotics prior to your dental treatment?"]
        let questions = self.arrayOfQuestions(question)
        questions[6].isAnswerRequired = true
        questions[7].isAnswerRequired = true
        return questions
    }


}

class PDOption : NSObject {
    var question : String!
    var isSelected : Bool?
    var index : Int!
    
    init(value : String) {
        super.init()
        self.question = value
    }
    
    class func arrayOfOptions(_ array: [String]) -> [PDOption]{
        var questions  = [PDOption]()
        for value in array {
            let obj = PDOption(value: value)
            obj.isSelected = false
            questions.append(obj)
        }
        return questions
    }
    
    
    class func getObjects (_ arrayResult : NSArray) -> [PDOption] {
        var questions  = [PDOption]()
        for (idx, value) in arrayResult.enumerated() {
            let obj = PDOption(value: value as! String)
            obj.index = idx
            questions.append(obj)
        }
        return questions
    }
    
    class func hippaForm1(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let questions = ["I have read the material available at the front desk.",
                         "I would like to request a printed copy to take with me."]
        completion(self.getObjects(questions as NSArray), true)
    }
    
    
    class func hippaForm2(_ isChild : Bool, completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let questions = ["You may use my \(isChild == true ? "child’s" : "") first name & photo on your Web site gallery and social media pages. This may include Facebook, Google+, Twitter, Instagram, and Pinterest.",
                         "You may use my \(isChild == true ? "child’s" : "") photo as referenced above, but no name.",
                         "I would prefer that my \(isChild == true ? "child’s" : "") smile is not featured online."]
        completion(self.getObjects(questions as NSArray), true)
    }
}


