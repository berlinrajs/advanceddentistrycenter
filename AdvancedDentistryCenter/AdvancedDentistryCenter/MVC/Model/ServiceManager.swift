//
//  ServiceManager.swift
//  ProDental
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 ProDental. All rights reserved.
//

import UIKit
let kAppKey = "MC-ADVANCEDDENTISTRYCENTER-MI-08"

class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: "http://mncell.com/mclogin/"))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result as AnyObject)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error)
        }
    }
    class func loginWithUsername(_ userName: String, password: String, completion:
        @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("apploginapi.php?", parameters: ["appkey": kAppKey, "username": userName, "password": password], success: { (result) in
            if (result["posts"]! as! NSDictionary)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]! as! NSDictionary)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    class func uploadFile(fileName: String, clientName: String, patientName: String, formName: String, pdfData: Data, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
            // http://mconsent.net/admin/api/activity_api.php?client_name=pp&formname=MedicalHistoryform&task_data=image&email=srs@gmail.com
            
            let manager = AFHTTPSessionManager(baseURL: URL(string: "http://mconsent.net/admin/api/")!)
            
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            manager.post("activity_api.php?", parameters: ["client_name": clientName, "patient_name": patientName, "formname": formName, "appkey": kAppKey], constructingBodyWith: { (data) in
                data.appendPart(withFileData: pdfData, name: "task_data", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                completion(true, nil)
                print("RESULT: \(String(describing: result))")
            }, failure: { (task, error) in
                completion(false, error.localizedDescription)
                print("ERROR: \(error.localizedDescription)")
            })
    }
}
