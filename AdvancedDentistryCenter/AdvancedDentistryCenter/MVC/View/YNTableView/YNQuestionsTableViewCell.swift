//
//  YNQuestionsTableViewCell.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

@objc protocol YNQuestionsTableViewCellDelegate {
    @objc optional func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell)
    @objc optional func questionNoClicked(forCell cell: YNQuestionsTableViewCell)

}

class YNQuestionsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioButtonYes: RadioButton!
    @IBOutlet weak var buttonCheckBox: UIButton!
    var type: YNCellType!
    
    var question: PDQuestion!
    var delegate: YNQuestionsTableViewCellDelegate?
    
    static var questionFont: UIFont {
        get {
            return UIFont(name: "WorkSans-Regular", size: 16.0)!
        }
    }
    
    class func initWith(_ cellType: YNCellType) -> YNQuestionsTableViewCell? {
        var cell: YNQuestionsTableViewCell?
        switch  cellType {
        case .yesOrNo:
            cell = Bundle.main.loadNibNamed("YNTableView", owner: nil, options: nil)![0] as? YNQuestionsTableViewCell
        case .yesOrNoOrDK:
            cell = Bundle.main.loadNibNamed("YNTableView", owner: nil, options: nil)![1] as? YNQuestionsTableViewCell
        case .checkBox:
            cell = Bundle.main.loadNibNamed("YNTableView", owner: nil, options: nil)![2] as? YNQuestionsTableViewCell
        default:
            cell = Bundle.main.loadNibNamed("YNTableView", owner: nil, options: nil)![2] as? YNQuestionsTableViewCell
        }
        cell!.type = cellType
        return cell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ question: PDQuestion) {
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.question = question
        if self.type == YNCellType.yesOrNo {
            labelTitle.text = question.question
            if question.selectedOption == nil {
                radioButtonYes.deselectAllButtons()
            } else {
                radioButtonYes.setSelected(question.selectedOption!)
            }
        } else if self.type == YNCellType.yesOrNoOrDK {
            labelTitle.text = question.question
            radioButtonYes.setSelectedWithTag(question.selectedOption == nil ? 2 : question.selectedOption == false ? 1 : 0)
        } else if self.type == YNCellType.checkBox {
//            buttonCheckBox.setTitle(question.question, forState: UIControlState.Normal)
            buttonCheckBox.isSelected = question.selectedOption!
        } else {
            radioButtonYes.setSelected(question.selectedOption!)
        }

        self.question = question
        labelTitle?.text = question.question
    }
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        if self.type == YNCellType.checkBox {
            sender.isSelected = !sender.isSelected
            question.selectedOption = sender.isSelected
            if question.isAnswerRequired == true && sender.isSelected {
                self.delegate?.questionAnswerRequired?(forCell: self)
            }
        } else if self.type == YNCellType.yesOrNoOrDK {
            if question.isAnswerRequired == true && sender.tag == 0 {
                self.delegate?.questionAnswerRequired?(forCell: self)
            } else {
                
                if sender.tag != 0 && self.question.options != nil {
                    for option in self.question.options! {
                        option.isSelected = false
                    }
                }
                question.selectedOption = sender.tag == 0 ? true : sender.tag == 1 ? false : nil
                self.delegate?.questionNoClicked?(forCell: self)
            }
        } else {
            self.question.selectedOption = radioButtonYes.isSelected
            if question.isAnswerRequired == true && radioButtonYes.isSelected {
                self.delegate?.questionAnswerRequired?(forCell: self)
            }
        }
    }

}
