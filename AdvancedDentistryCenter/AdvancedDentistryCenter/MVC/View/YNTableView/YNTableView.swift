//
//  YNTableView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

public enum YNCellType: Int {
    case yesOrNo = 0, yesOrNoOrDK, checkBox, custom
}

protocol YNTableViewDataSource {
    func sectionTitlesInTableView(_ tableView: YNTableView) -> [String]?
    func arrayOfQuestionsArrayInTableView(_ tableView: YNTableView) -> [[PDQuestion]]?
}

class YNTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
    var delegateYNTableView : YNQuestionsTableViewCellDelegate?
    var cellType: YNCellType! = .yesOrNoOrDK {
        didSet {
//            self.reloadData()
        }
    }
    
    var ynDataSource: YNTableViewDataSource? {
        didSet {
            self.clipsToBounds = false
            self.arraySectionTitle = ynDataSource?.sectionTitlesInTableView(self)
            self.arrayOfQuestionsArray = ynDataSource?.arrayOfQuestionsArrayInTableView(self)
        }
    }
    
    var arrayOfQuestionsArray : [[PDQuestion]]? {
        didSet {
            self.reloadData()
        }
    }
    var arrayOfQuestions: [PDQuestion]? {
        didSet {
            self.arrayOfQuestionsArray = [arrayOfQuestions!]
        }
    }
    
    var arraySectionTitle: [String]?
    
    override func awakeFromNib() {
        
        self.delegate = self
        self.dataSource = self
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOfQuestionsArray != nil ? arrayOfQuestionsArray!.count : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfQuestionsArray?[section] != nil ? arrayOfQuestionsArray![section].count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as? YNQuestionsTableViewCell
        if cell == nil {
            cell = YNQuestionsTableViewCell.initWith(self.cellType)
        }
        let obj = self.arrayOfQuestionsArray![indexPath.section][indexPath.row]
        cell!.configureCell(obj)
        cell!.tag = indexPath.row
        
        cell!.delegate = delegateYNTableView
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let obj = self.arrayOfQuestionsArray![indexPath.section][indexPath.row]
        let height = obj.question.heightWithConstrainedWidth(self.frame.width - (cellType.rawValue == 0 ? 133 : cellType.rawValue == 1 ? 164 : 16), font: YNQuestionsTableViewCell.questionFont)
        return height + (self.cellType == YNCellType.checkBox ? 10 : 16)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return arraySectionTitle == nil ? 0 : 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if arraySectionTitle == nil {
            return nil
        }
        
        var headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Header") as? YNHeaderView
        
        if headerView == nil {
            headerView = YNHeaderView(reuseIdentifier: "Header")
        }
        headerView!.configWithTitle(arraySectionTitle![section])
        
        return headerView
    }
}
class YNHeaderView: UITableViewHeaderFooterView {
    
    var labelHeader: UILabel!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        let bgView = UIView()
        bgView.backgroundColor = UIColor.clear
        self.backgroundView = bgView
        contentView.backgroundColor = UIColor.clear
        
        labelHeader = UILabel(frame: CGRect.zero)
        labelHeader.font = UIFont(name: "WorkSans-Regular", size: 18.0)
        labelHeader.backgroundColor = UIColor.clear
        labelHeader.textColor = UIColor.white
        labelHeader.textAlignment = NSTextAlignment.left
        
        //        labelHeader.numberOfLines = 0
        self.addSubview(labelHeader)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        labelHeader.frame = CGRect(x: -20, y: 10, width: self.frame.width, height: self.frame.height - 10)
    }
    func configWithTitle(_ title: String) {
        labelHeader.text = title
    }
}
